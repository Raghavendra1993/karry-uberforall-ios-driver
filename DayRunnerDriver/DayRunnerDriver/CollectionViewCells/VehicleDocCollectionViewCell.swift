//
//  VehicleDocCollectionViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 17/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class VehicleDocCollectionViewCell: UICollectionViewCell {
    @IBOutlet var insuranceImage: UIImageView!
    
    @IBOutlet var removeDocImage: UIButton!
}
