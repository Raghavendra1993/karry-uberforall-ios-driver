//
//  ProfileCollectionViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 17/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {
    @IBOutlet var vehicleDetailImage: UIImageView!
    
    @IBOutlet var removeDetailsImage: UIButton!
}
