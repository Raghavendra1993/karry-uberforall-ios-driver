//
//  HelpViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {
    
    
    
    @IBOutlet var topView: UIView!
    @IBOutlet var buttomContraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {

      Helper.shadowView(sender: topView, width:UIScreen.main.bounds.size.width-20, height:topView.frame.size.height)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //  endTheAnimation()
    }
    
    //MARK:- navigate to register screen
    @IBAction func signupButton(_ sender: Any) {
        performSegue(withIdentifier: "toSignupVC", sender: nil)
    }
    
    //MARK:- navigate to login screen
    @IBAction func loginButton(_ sender: Any) {
        performSegue(withIdentifier: "toSigninVC", sender: nil)
    }
    
    // MARK:- start moving the bottonview to up
    func startTheAnimation(){
        UIView.animate(withDuration: 0.4,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {
                        self.buttomContraint.constant = 80;
                        
        },
                       completion: { finished in
                        print("startAnimation")
        })
        self.view.layoutIfNeeded()
    }
    
    // MARK:- start moving the bottonview to Down
    func endTheAnimation(){
        UIView.animate(withDuration: 0.4,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {
                        self.buttomContraint.constant = -165;
                        
        },
                       completion: { finished in
                        print("EndAnimation")
        })
        self.view.layoutIfNeeded()
    }
    
}
