//
//  SignUpViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,UINavigationControllerDelegate,CountryPickerDelegate  {
    
    @IBOutlet var selectOperator: UIButton!
    @IBOutlet var contentScrollView: UIView!
    @IBOutlet var profileImage: UIImageView!
    
    @IBOutlet var countryImage: UIImageView!
    
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var countryCode: UILabel!
    
    @IBOutlet var collectionView: UICollectionView!
    
    @IBOutlet var SignupButton: UIButton!
    
    @IBOutlet var operatorTextField: UITextField!
    
    @IBOutlet var heightOfOperatorField: NSLayoutConstraint!
    @IBOutlet var lastName: HoshiTextField!
    @IBOutlet var firstNAme: HoshiTextField!
    @IBOutlet var password: HoshiTextField!
    @IBOutlet var emailAddress: HoshiTextField!
    @IBOutlet var phoneNumber: HoshiTextField!
    
    @IBOutlet var referralcodeTF: HoshiTextField!
    @IBOutlet var operatorButton: UIButton!
    
    @IBOutlet var zonesTF: HoshiTextField!
    @IBOutlet var operatorImage: UIImageView!
    @IBOutlet var topSpaceForOperatorField: NSLayoutConstraint!
    @IBOutlet var freeLancerButton: UIButton!
    var typeModelMake:Int = 0
    var activeTextField = UITextField()
    var selectImageTyep:Int = 0
    var myImages = [UIImage]()
    var signuParameters = [String: Any]()
    var licenceImages = String()
    var profileUrl  = String()
    var pickedImage = UIImageView()
    var latit = String()
    var longit = String()
    
    
    var mobileService = false
    var emailService = false
    var referralService = false
    
    let amazonWrapper:AmazonWrapper = AmazonWrapper.sharedInstance()
    let location = LocationManager.sharedInstance
    
    var zonesID = [String]()
    var indexesSelected   = [Int]()
    
    override func viewDidLoad() {
        pickedImage.image = nil
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setCountryCode()
      
        amazonWrapper.delegate = self
        location.startUpdatingLocation()
        location.delegate = self
        
        
        
        ///This been commented for removing the operator
        //        selectOperator.layer.borderWidth = 1
        //        selectOperator.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0x23C590).cgColor
        //
        //        let ud = UserDefaults.standard
        //
        //        if ud.bool(forKey: "freeLancerSelected"){
        //            operatorButton.isSelected = false
        //            freeLancerButton.isSelected = true
        //            self.freeLaccing(self)
        //
        //        }else{
        //            operatorButton.isSelected = true
        //            freeLancerButton.isSelected = false
        //            self.operatorAction(self)
        //        }
        
        
        SignupButton.setTitle("NEXT", for: UIControlState.normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            self .moveViewUp(activeView: activeTextField, keyboardHeight: keyboardSize.height)
        }
    }
    
    // MARK: - Tapgesture
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    /// hide keyboard
    func dismisskeyBord() {
        if (referralcodeTF.text?.isEmpty)! {
            
        }else{
            if !referralService {
                verifyingTheReferral()
            }
        }

        view.endEditing(true)
    }
    
    @IBAction func selectVehicleDoc(_ sender: Any) {
        selectImageTyep = 1 // For licence images tag
        selectImage()
    }
    
    @IBAction func selectProfilePic(_ sender: Any) {
        selectImageTyep = 0 //ForProfilePic tag
        selectImage()
    }
    
    @IBAction func backToController(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func freeLaccing(_ sender: Any) {
        let ud = UserDefaults.standard
        ud.set(true, forKey: "freeLancerSelected")
        ud.synchronize()
        freeLancerButton.isSelected = true
        operatorButton.isSelected = false
        self.heightOfOperatorField.constant = 0
        topSpaceForOperatorField.constant = 0
        SignupButton.setTitle("NEXT", for: UIControlState.normal)
        operatorImage.isHidden = true
    }
    
    @IBAction func operatorAction(_ sender: Any) {
        let ud = UserDefaults.standard
        ud.set(false, forKey: "freeLancerSelected")
        ud.synchronize()
        operatorButton.isSelected = true
        freeLancerButton.isSelected = false
        self.heightOfOperatorField.constant = 40
        topSpaceForOperatorField.constant = 15
        SignupButton.setTitle("REGISTER", for: UIControlState.normal)
        operatorImage.isHidden = false
    }
    
    @IBAction func selectOperator(_ sender: Any) {
        typeModelMake = 5 // for operator selection tag
        self.view.endEditing(true)
        performSegue(withIdentifier:"toSelectOperator" , sender: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        // Get Max Y of Active View with respect their Super View
        var viewMAX_Y = activeView.frame.maxY
        
        // Check if SuperView of ActiveView lies in Nexted View context
        // Get Max for every Super of ActiveTextField with respect to Views SuperView
        if var view: UIView = activeView.superview {
            
            // Check if Super view of View in Nested Context is View of ViewController
            while (view != self.view.superview) {
                viewMAX_Y += view.frame.minY
                view = view.superview!
            }
        }
        
        // Calculate the Remainder
        let remainder = self.view.frame.height - (viewMAX_Y + keyboardHeight)
        
        // If Remainder is Greater than 0 does mean, Active view is above the and enough to see
        if (remainder >= 0) {
            // Do nothing as Active View is above Keyboard
        }
        else {
            // As Active view is behind keybard
            // ScrollUp View calculated Upset
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            self.mainScrollView.contentOffset = CGPoint(x: 0, y: -remainder)
            })
        }
        
        // Set ContentSize of ScrollView
        var contentSizeOfContent: CGSize = self.contentScrollView.frame.size
        contentSizeOfContent.height += keyboardHeight
        self.mainScrollView.contentSize = contentSizeOfContent
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        self.mainScrollView.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(0))
        let contentSizeOfContent: CGSize = self.contentScrollView.frame.size
        self.mainScrollView.contentSize = contentSizeOfContent
    }
    
    @IBAction func goToVehicleDetails(_ sender: Any) {
        
        signupMethod()
    }
    
    // MARK: - SignupMethod
    func signupMethod(){
        if (firstNAme.text?.isEmpty)!{
            self.present(Helper.alertVC(title:signup.Message! as NSString , message:signup.enterFirstName! as NSString), animated: true, completion: nil)
        }
            
        else if (phoneNumber.text?.isEmpty)!{
            self.present(Helper.alertVC(title: signup.Message! as NSString, message:signup.enterPhoneNumber! as NSString), animated: true, completion: nil)
        }
            
        else if (emailAddress.text?.isEmpty)!{
            self.present(Helper.alertVC(title: signup.Message! as NSString, message:signup.enterEmailId! as NSString), animated: true, completion: nil)
        }
        else if (password.text?.isEmpty)!{
            self.present(Helper.alertVC(title: signup.Message! as NSString, message:signup.enterPassword! as NSString), animated: true, completion: nil)
        }
        else if !(emailAddress.text?.isValidEmail)!{
            emailAddress.becomeFirstResponder()
            self.present(Helper.alertVC(title: signup.Message! as NSString, message:signup.InValidEmail! as NSString), animated: true, completion: nil)
        }else if myImages.count == 0{
            self.present(Helper.alertVC(title: signup.Message! as NSString, message:"Please Select licence images"), animated: true, completion: nil)
        }else if pickedImage.image == nil{
           self.present(Helper.alertVC(title: signup.Message! as NSString, message:"Please Select Profile images"), animated: true, completion: nil)
        }
        else{
            self.uploadProfileimgToAmazon()
            self.uploadLicenceToAmazon()
            saveTheParameters()
            performSegue(withIdentifier:segues.vehicleDetails! , sender: nil)
            
        }
    }
    
    func uploadProfileimgToAmazon(){
        var url = String()
        url = AMAZONUPLOAD.PROFILEIMAGE + self.phoneNumber.text! + ".png"
        amazonWrapper.uploadImageToAmazon(withImage: profileImage.image!, imgPath: url)
    }
    
    
    func uploadLicenceToAmazon(){
        
//        var imagesCount:Int = 0
//        for images in myImages {
//            imagesCount =  imagesCount + 1
            var strUrl = String()
            strUrl = AMAZONUPLOAD.DRIVERLICENCE + self.phoneNumber.text! + "1" + ".png"
            amazonWrapper.uploadImageToAmazon(withImage: myImages[0], imgPath: strUrl)
 //       }
    }
    
    func saveTheParameters(){
        
        var accountType = String()
        //        if operatorButton.isSelected {
        //            accountType = "2"
        //        }else{
        accountType = "1"
        //     }
        
        if (referralcodeTF.text!.isEmpty){
           referralcodeTF.text! = ""
        }
        self.licenceImages = Utility.amazonUrl + AMAZONUPLOAD.DRIVERLICENCE + self.phoneNumber.text! + "_1" + ".png"
         let profileIMG = Utility.amazonUrl + AMAZONUPLOAD.PROFILEIMAGE + self.phoneNumber.text! + ".png"
        
        signuParameters = ["ent_first_name"            : firstNAme.text!,
                           "ent_last_name"             : lastName.text!,
                           "ent_email"                 : emailAddress.text!,
                           "ent_password"              : password.text!,
                           "ent_mobile"                : phoneNumber.text!,
                           "ent_contry_code"           : countryCode.text!,
                           "ent_zipcode"               : "560024",
                           "ent_latitude"              : latit,
                           "ent_longitude"             : longit,
                           "ent_profile_pic"           : profileIMG,
                           "deviceId"                  : Utility.deviceId,
                           "ent_plat_no"               : "",
                           "ent_type"                  : "",
                           "ent_specialities"          : "",
                           "ent_make"                  : "",
                           "ent_model"                 : "",
                           "ent_vehicleImage"          : "",
                           "ent_operator"              : "" ,
                           "driverLicense"             : self.licenceImages,
                           "accountType"               : accountType,
                           "deviceType"                : "1",
                           "zones"                     : zonesID,
                           "referral"                  : referralcodeTF.text!,
                           "ent_insurance_photo"       : "",
                           "ent_carriage_permit"       : "",
                           "ent_reg_cert"              : ""]
    }
    
    func sendRequestForSignup() {
        
        let params : [String : Any] =       ["mobile"   : countryCode.text! + phoneNumber.text!,
                                             "userType" : "2"]  //1:Slave 2:Master
        
        print("getOtp parameters :",params)
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.SignupGetOTP,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("signup response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        self.performSegue(withIdentifier:"toVerifyFromSignup" , sender: nil)
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    
    /// MARK: - actionSheet
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: signup.selectImage as String?,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: signup.cancel as String?,
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: signup.gallery as String?,
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: signup.camera as String?,
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        //        if pickedImage.image != nil {
        //            let removePhoto: UIAlertAction = UIAlertAction(title: "Remove Photo" as String?,
        //                                                           style: .cancel) { action -> Void in
        //                                                            self.removePhoto()
        //            }
        //            actionSheetController.addAction(removePhoto)
        //        }
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    // MARK: - photo gallery
    func removePhoto()
    {
        profileUrl = ""
        pickedImage.image = nil
        profileImage.image=UIImage(named: "userdefaulticon")
        
    }
    
    
    // MARK: - photo gallery
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    // MARK: - Camera selection
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerControllerSourceType.camera;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
        }
    }
    
    // MARK: - setCountrycode
    func setCountryCode(){
        let picker = CountryPicker.dialCode(code: "IN")
        countryCode.text = picker.dialCode
        countryImage.image = picker.flag
    }
    
    ///*******Select country mobile code***************//
    @IBAction func selectTheCountryCode(_ sender: Any) {
        
        performSegue(withIdentifier:segues.countryPicker!, sender: nil)
    }
    
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == segues.countryPicker!
        {
            let nav = segue.destination as! UINavigationController
            if let picker: CountryPicker = nav.viewControllers.first as! CountryPicker?
            {
                picker.delegate = self
            }
        }
        else if segue.identifier == "toSelectOperator" {
            let nextScene = segue.destination as? VehicleTypeModelMakeVC
            nextScene?.delegate = self
            typeModelMake = 5
            nextScene?.type = typeModelMake
            nextScene?.alreadySelectedIndexes = indexesSelected
        }else if segue.identifier == "toVerifyFromSignup"{
            let nextScene = segue.destination as? VerifyOTPVC
            nextScene?.defineTheOtp = 1
            nextScene?.mobileNumber = countryCode.text! + phoneNumber.text!
            nextScene?.signupParams = signuParameters
        }else if segue.identifier == segues.vehicleDetails!{
            let nav =  segue.destination as! UINavigationController
            let nextScene = nav.viewControllers.first as! VehicleDetailsVC?
            nextScene?.signupParams = signuParameters 
        }
    }
    
    // MARK: - country delegate method
    internal func didPickedCountry(country: Country)
    {
        countryCode.text = country.dialCode
        countryImage.image = country.flag
    }
    
    func removeProPhotos(_ sender: UIButton) {
        let selectedIndex: Int = sender.tag % 1000
        myImages.remove(at: selectedIndex)
        collectionView.reloadData()
        
    }
    
    func validateEmailPhone(typeValidate:String) {
        
        let params : [String : Any]
        if typeValidate == "2"{
            Helper.showPI(message:"Verifying the MobileNumber")
            params  = [
                "mobile"         : countryCode.text! + phoneNumber.text!,
                "velidationType" : typeValidate]
        }else{
            Helper.showPI(message:"Verifying the EmailAddress")
            params = [
                "velidationType" : typeValidate,
                "ent_email":emailAddress.text!]
        }
        
        print("getOtp parameters :",params)
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.EMAILPHONEVALIDATE,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("Validate Response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        let flag:Int = response["errFlag"] as! Int
                                        if flag == 1{
                                            let data:Int = response["data"] as! Int
                                            if data == 1{
                                                self.emailService = false
                                                self.emailAddress.becomeFirstResponder()
                                                self.present(Helper.alertVC(title: signup.Message! as NSString, message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                                
                                            }else{
                                                self.mobileService = false
                                                self.phoneNumber.becomeFirstResponder()
                                                self.present(Helper.alertVC(title: signup.Message! as NSString, message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                            }
                                        }else{
                                            if typeValidate == "2"{
                                                self.mobileService = true
                                            }else{
                                                self.emailService = true
                                            }
                                        }
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            self.view.endEditing(true)
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    func verifyingTheReferral(){
        
        Helper.showPI(message:"Loading..")
        let params : [String : Any] =  ["": ""] //Get APi
        
        print("params :",params)
        
        let serviceName = API.METHOD.REFERRALCODE + referralcodeTF.text!
        
        
        NetworkHelper.requestGETURL(method: serviceName,
                                    success: { (response) in
                                        Helper.hidePI()
                                        if response.isEmpty == false {
                                            
                                            self.referralService = true
                                            
                                        } else {
                                            self.referralService = false
                                        }
                                        
        })
        { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    


}

// MARK: - CollectionView datasource method
extension SignUpViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: signupIdentifierds.collectionImage!, for: indexPath as IndexPath) as! ProfileCollectionViewCell
            
            cell.vehicleDetailImage.image = myImages[indexPath.row]
            
            cell.removeDetailsImage.tag = indexPath.row + 1000
            cell.removeDetailsImage.addTarget(self, action: #selector(self.removeProPhotos), for: .touchUpInside)
            
            return cell
        }else{
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: signupIdentifierds.collectionaddImg! , for: indexPath as IndexPath) as! ProfileAddCollectionViewCell
            
            return cell1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return myImages.count
        }else{
            if myImages.count == 0{
                return 1
            }else{
                return 0
            }
        }
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
}


// MARK: - CollectionView delegate method
extension SignUpViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
}

// MARK: - Textfield delegate method
extension SignUpViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        if textField == password && (emailAddress.text?.characters.count)!>2{
            
            if !(emailAddress.text?.isValidEmail)!{
                emailAddress.becomeFirstResponder()
                self.present(Helper.alertVC(title: signup.Message! as NSString, message:signup.InValidEmail! as NSString), animated: true, completion: nil)
            }else{
                if !emailService {
                self.validateEmailPhone(typeValidate: "1")
                }
            }
            
        }else if textField == emailAddress && (phoneNumber.text?.characters.count)!>2{
            if !mobileService {
                   self.validateEmailPhone(typeValidate: "2")
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumber {
            mobileService = false
        }
        
        if textField == emailAddress {
            emailService = false
        }
        
        if textField ==  referralcodeTF{
            referralService = false
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {  //delegate method
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        switch textField {
        case firstNAme:
            lastName.becomeFirstResponder()
            break
        case lastName:
            phoneNumber.becomeFirstResponder()
            break
        case phoneNumber:
            emailAddress.becomeFirstResponder()
            break
            
        case emailAddress:
            password.becomeFirstResponder()
            break
            
        default:
            if (referralcodeTF.text?.isEmpty)! {
               
            }else{
                if !referralService {
                      verifyingTheReferral()
                }
            }
            
            dismisskeyBord()
            break
        }
        
        return true
    }
}

// MARK: - ImagePicker delegate method
extension SignUpViewController : UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        if selectImageTyep == 0{
            pickedImage.image = image
            profileImage.image = image
        }else{
            myImages.append(image)
            collectionView.reloadData()
        }
        //.resizeImage(size: CGSize(width: 100, height: 100))
        self.dismiss(animated: true, completion: nil)
    }
}


extension SignUpViewController:vehicleDetailsDelegate{
    internal func didSelectMakeModelType(vehicleData: VehicleType)
    {
        zonesTF.text = vehicleData.vehicleTypeMakeModel
        zonesID = vehicleData.seletedIDs
        indexesSelected   = vehicleData.selectedIndexes
    }

}

extension SignUpViewController: AmazonWrapperDelegate{
    func didImageUploadedSuccessfully(withDetails imageURL: String) {
        profileUrl = imageURL
    }
    
    func didImageFailtoUpload(_ error: Error?) {
        
    }
}

extension SignUpViewController:LocationManagerDelegate{
    func locationFound(_ latitude: Double, longitude: Double) {
    }
    func locationFoundGetAsString(_ latitude: NSString, longitude: NSString) {
        latit = latitude as String
        longit = longitude as String
    }
}
