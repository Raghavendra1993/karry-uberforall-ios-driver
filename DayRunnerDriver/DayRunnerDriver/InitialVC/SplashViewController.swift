//
//  SplashViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    var window: UIWindow?
    
    @IBOutlet var splashImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.window = UIApplication.shared.keyWindow
        
        let screenSize: CGRect = UIScreen.main.bounds
        if(screenSize.height == 568)
        {
            splashImage.image = UIImage(named:"ip5.png")
        }else if(screenSize.height == 480)
        {
            splashImage.image = UIImage(named:"ip5.png")
        }else if(screenSize.height == 667)
        {
            splashImage.image = UIImage(named:"ip6.png")
        }else if(screenSize.height == 736)
        {
            splashImage.image = UIImage(named:"ip6+.png")
        }else{
            splashImage.image = UIImage(named:"ip6+.png")
        }

        
        //        let lm = LocationManager.sharedInstance
        //        lm.startUpdatingLocation()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let defaults = UserDefaults.standard
        defaults.setValue(deviceID, forKey:USER_INFO.DEVICE_ID)
        defaults.synchronize()
        
        Timer.scheduledTimer(timeInterval: 2.0,
                             target: self,
                             selector: #selector(self.methodToBeCalled),
                             userInfo: nil,
                             repeats: false); //
    }
    
    /// Moves to helpVC
    func methodToBeCalled(){
        
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            performSegue(withIdentifier: "toSignin", sender: nil)
            //            let controller = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.signIN)
            //            navigationController?.viewControllers = [controller!]
        }
//        else if Utility.vehicleTypeID == USER_INFO.VEHTYPEID{
//            performSegue(withIdentifier: "toSelectVehicle", sender: nil)
//        }
        else{
            self.moveTohomeVC()
        }
    }
    
    func moveTohomeVC() {
        dismiss(animated: true, completion: nil)
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: homeVC)
        
        //  UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
        leftViewController.homeVC = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = homeVC
        // self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }
    
}
