//
//  SetNewpasswordViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 19/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SetNewpasswordViewController: UIViewController {
    
    @IBOutlet var contentScrollView: UIView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var bottomContraint: NSLayoutConstraint!
    @IBOutlet var confirmPassword: HoshiTextField!
    @IBOutlet var newPassword: HoshiTextField!
    var activeTextField = UITextField()
    var secureOtp = String()
    var mobileNumber = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    
    //MARK: - Keyboard Methods
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            self.moveViewUp(activeView: confirmPassword, keyboardHeight: keyboardSize.height)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveScrollViewDown()
        }
    }
    
    func setNewpassword() {
        Helper.showPI(message:"Verifying OTP")
        
        
        let params : [String : Any] =  ["securenumber"   : secureOtp,
                                        "password"       : newPassword.text!,
                                        "userType"       :2,
                                        "ent_mobile"     :mobileNumber]
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.UPDATEPASSWORD,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("Verification Response : ",response)
                                    if response.isEmpty == false {
                                        _ = self.navigationController?.popToRootViewController(animated: true)
                                        
                                        Helper.hidePI()
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    
    //********* Hide the keyboard********//
    @IBAction func hideKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    //*********back to root vc*************//
    @IBAction func backToVC(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveThePAssword(_ sender: Any) {
        if (newPassword.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message", message:"Please enter password"), animated: true, completion: nil)
        }else if (confirmPassword.text?.isEmpty)!{
            self.present(Helper.alertVC(title: "Message", message:"Please enter confirm Password"), animated: true, completion: nil)
        }else if newPassword.text! != confirmPassword.text!{
            self.present(Helper.alertVC(title: "Message", message:"password Mismatched"), animated: true, completion: nil)
        }else{
            self.setNewpassword()
        }
    }
    
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        // Get Max Y of Active View with respect their Super View
        var viewMAX_Y = activeView.frame.maxY
        
        // Check if SuperView of ActiveView lies in Nexted View context
        // Get Max for every Super of ActiveTextField with respect to Views SuperView
        if var view: UIView = activeView.superview {
            
            // Check if Super view of View in Nested Context is View of ViewController
            while (view != self.view.superview) {
                viewMAX_Y += view.frame.minY
                view = view.superview!
            }
        }
        
        // Calculate the Remainder
        let remainder = self.view.frame.height - (viewMAX_Y + keyboardHeight)
        
        // If Remainder is Greater than 0 does mean, Active view is above the and enough to see
        if (remainder >= 0) {
            // Do nothing as Active View is above Keyboard
        }
        else {
            // As Active view is behind keybard
            // ScrollUp View calculated Upset
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            self.mainScrollView.contentOffset = CGPoint(x: 0, y: -remainder)
                            //                            self.bottomContraint.constant = keyboardHeight
                            //                            self.view.layoutIfNeeded()
            })
        }
        
        // Set ContentSize of ScrollView
        var contentSizeOfContent: CGSize = self.contentScrollView.frame.size
        contentSizeOfContent.height += keyboardHeight
        self.mainScrollView.contentSize = contentSizeOfContent
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveScrollViewDown() {
        self.mainScrollView.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(0))
        let contentSizeOfContent: CGSize = self.contentScrollView.frame.size
        self.mainScrollView.contentSize = contentSizeOfContent
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        //                        self.bottomContraint.constant = 0
                        //                        self.view.layoutIfNeeded()
        })
    }
    
   func dismisskeyBord(){
    self.view.endEditing(true)
    }
    
}

// MARK: - Textfield delegate method
extension SetNewpasswordViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        switch textField {
        case newPassword:
            confirmPassword.becomeFirstResponder()
            break
        case confirmPassword:
            dismisskeyBord()
            break
        default:
            dismisskeyBord()
            break
        }
        
        return true
    }
}

