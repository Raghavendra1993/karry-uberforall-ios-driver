//
//  SelectVehicleVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SelectVehicleVC: UIViewController {
    
    var window: UIWindow?
    var selectedIndex:Int = -1
    @IBOutlet var ConfirmAction: UIButton!
    var vehicles = [[String : Any]]()
    
    var vehicleDetails = [SelectVehicle]()
    
    var token = String()
    
    @IBOutlet var vehicleTypeTableView: UITableView!
    
    override func viewDidLoad() {
        
        for dict in vehicles {
            
            let number    = dict["platNo"] as? String
            let type      = dict["vehicleType"] as? String
            let model     = dict["vehicleModel"] as? String
            let iD        = dict["id"] as? String
            let vehId     = dict["typeId"] as? String
            let goodType  = dict["goodTypes"] as? [String]
            
            vehicleDetails.append(SelectVehicle.init(plateNo: number!, vehType: type!, vehModel: model!,vehID:iD!,vehTypeID:vehId!, goodTypes:goodType!))
        }
        
        self.window = UIApplication.shared.keyWindow
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func logoutFromApp() {
        Helper.showPI(message:"Logging Out..")
        
        let params : [String : Any] =  ["token"   : self.token,
                                        "userType"     : "2"]
        
        NetworkHelper.requestPUT(serviceName:API.METHOD.LOGOUT,
                                 params: params,
                                 success: { (response : [String : Any]) in
                                    print("Verification Response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        self.dismiss(animated: true, completion: nil)
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    @IBAction func LogoutAction(_ sender: Any) {
        logoutFromApp()
        
    }
    
    @IBAction func ConfirmVehicle(_ sender: Any) {
        createMenuView()
        
    }
    
    fileprivate func createMenuView() {
        if selectedIndex == -1 {
            self.present(Helper.alertVC(title: "Message", message:"Please Select VehicleID"), animated: true, completion: nil)
            
        }else{
            
            makeVehicleDefault()
        }
    }
    
    func makeVehicleDefault() {
        Helper.showPI(message:"Selecting Vehicle..")
        let params : [String : Any] =  [
            "token"          :self.token,
            "workplace_id"   :vehicleDetails[selectedIndex].VehicleID,
            "type_id"        :vehicleDetails[selectedIndex].typeId,
            "goodTypes"      :vehicleDetails[selectedIndex].goodType]
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.VEHICLEDEFAULT,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("signup response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        UserDefaults.standard.set(self.vehicleDetails[self.selectedIndex].typeId, forKey: USER_INFO.VEHTYPEID)
                                        self.moveTohomeVC()
                                        let defaults = UserDefaults.standard
                                        defaults.set(self.token, forKey: USER_INFO.SESSION_TOKEN)
                                        defaults.synchronize()
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    func moveTohomeVC() {
        dismiss(animated: true, completion: nil)
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: homeVC)
        
        //  UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
        leftViewController.homeVC = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = homeVC
        // self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }

  }



extension SelectVehicleVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicleDetails.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"vehicleids") as! VehicleIDsTableViewCell!
        
        if selectedIndex == indexPath.row {
            cell?.selectedButton.isSelected = true
            cell?.backGroundView.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0x23c590)
        }else{
            cell?.selectedButton.isSelected = false
            cell?.backGroundView.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0xc2c2c2)
        }
        
        cell?.vehicleModel.text = vehicleDetails[indexPath.row].vehicleModel
        cell?.vehicleType.text = vehicleDetails[indexPath.row].vehicleType
        cell?.plateNumber.text = vehicleDetails[indexPath.row].plateNumber
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 85 //UITableViewAutomaticDimension
    }
}

extension SelectVehicleVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedIndex == indexPath.row{
            selectedIndex = -1
            ConfirmAction.isSelected = false
        }else{
            selectedIndex = indexPath.row
            ConfirmAction.isSelected = true
        }
        
        tableView.reloadData()
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

