//
//  ForgotPasswordWithEmailVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ForgotPasswordWithEmailVC: UIViewController {
    
    @IBOutlet var countryCode: UILabel!
    @IBOutlet var countryImage: UIImageView!
 
    
    @IBOutlet var contentScrollView: UIView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var phoneTF: UITextField!
    @IBOutlet var buttomConstrain: NSLayoutConstraint!
    var typeOfRecovery:Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // setCountryCode()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0x23C590)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func pickTheCountryCode(_ sender: Any) {
        performSegue(withIdentifier:"pickCountryCodeFromFP", sender: nil)
    }
    
    ///********dismissing the vc*******//
    @IBAction func backToController(_ sender: Any) {
        
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyBoard))
        view.addGestureRecognizer(tap)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    
    //MARK: - Keyboard Methods
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            self.moveViewUp(activeView: phoneTF, keyboardHeight: keyboardSize.height)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveScrollViewDown()
        }
    }
    
    @IBAction func generateOTP(_ sender: Any) {
        if (phoneTF.text?.isEmpty)! {
              self.present(Helper.alertVC(title: "Message", message:"Please enter Mobile or Email"), animated: true, completion: nil)
        }else{
            self.forgotPasswordRecovery()
        }
        
       
    }
    
    //*******hide keyboard***********//
    @IBAction func hideKeyBoard(_ sender: Any) {
        self.view.endEditing(true)
    }

    // MARK: - setCountrycode
    func setCountryCode(){
        let picker = CountryPicker.dialCode(code: "IN")
        countryCode.text = picker.dialCode
        countryImage.image = picker.flag
    }
    
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "pickCountryCodeFromFP"
        {
            let nav = segue.destination as! UINavigationController
            if let picker: CountryPicker = nav.viewControllers.first as! CountryPicker?
            {
                picker.delegate = self
            }
        }else if segue.identifier == "toVerifyOTP" {
            let nextScene = segue.destination as? VerifyOTPVC
            nextScene?.mobileNumber = phoneTF.text!
            nextScene?.defineTheOtp = 2
        }
    }
    
    
    func forgotPasswordRecovery(){
        
        let testString = phoneTF.text!
        
        
        let phone = testString.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        if phone == testString {
            typeOfRecovery = 1
        }else{
            typeOfRecovery = 2
        }
        
        print(phone)
        
        Helper.showPI(message:"Loading..")
        let params : [String : Any] =  [
            "ent_email_mobile":phoneTF.text!,
            "userType":2,
            "ent_type":typeOfRecovery
        ]
        
        print("params :",params)
        NetworkHelper.requestPOST(serviceName:API.METHOD.FORGOTPASSWORD,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        let flag:Int = response["errFlag"] as! Int
                                        if flag == 1{
                                            self.present(Helper.alertVC(title: signup.Message! as NSString, message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                        }else{
                                            if  self.typeOfRecovery == 1{
                                                self.performSegue(withIdentifier: "toVerifyOTP", sender: nil)
                                            }else{
//                                                self.navigationController?.present(Helper.alertVC(title: "Message", message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                                _ = self.navigationController?.popToRootViewController(animated: true)
                                            }
                                        }
                                    } else {
                                        
                                    }
                                    
        })
        { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    

    
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        // Get Max Y of Active View with respect their Super View
        var viewMAX_Y = activeView.frame.maxY
        
        // Check if SuperView of ActiveView lies in Nexted View context
        // Get Max for every Super of ActiveTextField with respect to Views SuperView
        if var view: UIView = activeView.superview {
            
            // Check if Super view of View in Nested Context is View of ViewController
            while (view != self.view.superview) {
                viewMAX_Y += view.frame.minY
                view = view.superview!
            }
        }
        
        // Calculate the Remainder
        let remainder = self.view.frame.height - (viewMAX_Y + keyboardHeight)
        
        // If Remainder is Greater than 0 does mean, Active view is above the and enough to see
        if (remainder >= 0) {
            // Do nothing as Active View is above Keyboard
        }
        else {
            // As Active view is behind keybard
            // ScrollUp View calculated Upset
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            self.mainScrollView.contentOffset = CGPoint(x: 0, y: -remainder)
                           //  self.buttomConstrain.constant = keyboardHeight
                           // self.view.layoutIfNeeded()
            })
        }
        
        // Set ContentSize of ScrollView
        var contentSizeOfContent: CGSize = self.contentScrollView.frame.size
        contentSizeOfContent.height += keyboardHeight
        self.mainScrollView.contentSize = contentSizeOfContent
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveScrollViewDown() {
        self.mainScrollView.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(0))
        let contentSizeOfContent: CGSize = self.contentScrollView.frame.size
        self.mainScrollView.contentSize = contentSizeOfContent
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                       // self.buttomConstrain.constant = 0
                       // self.view.layoutIfNeeded()
        })
    }

}

extension ForgotPasswordWithEmailVC:CountryPickerDelegate{
    // MARK: - country delegate method
    internal func didPickedCountry(country: Country)
    {
        countryCode.text = country.dialCode
        countryImage.image = country.flag
    }
}

extension ForgotPasswordWithEmailVC:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
