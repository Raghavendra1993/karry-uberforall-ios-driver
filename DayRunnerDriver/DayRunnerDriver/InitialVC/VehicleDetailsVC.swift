//
//  VehicleDetailsVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class VehicleDetailsVC: UIViewController,UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    
    @IBOutlet var contentScrollView: UIView!
    var activeTextField = UITextField()
    var selectImageTyep:Int = 0
    var typeModelMake:Int = 0
    
    
    @IBOutlet var insuranceImage: UIImageView!
    @IBOutlet var registrationImage: UIImageView!
    @IBOutlet var permitImage: UIImageView!
    @IBOutlet var mainSrollView: UIScrollView!
    @IBOutlet var plateNumber: HoshiTextField!
    @IBOutlet var vehicleModel: HoshiTextField!
    @IBOutlet var vehicleMake: HoshiTextField!
    @IBOutlet var vehicleType: HoshiTextField!
    @IBOutlet var vehicleSpecialities: HoshiTextField!
    
    let amazonWrapper:AmazonWrapper = AmazonWrapper.sharedInstance()
    
    var signupParams = [String: Any]()
    
    var vehicleDocs = String()
    @IBOutlet var vehicleImage: UIImageView!
    
    var vehImage = UIImageView()
    var insImage = UIImageView()
    var regImage = UIImageView()
    var perImage = UIImageView()
    
    var vehicleTypeID     = String()
    var vehicleMakeID     = String()
    var vehicleModelID    = String()
    var specialitiesIds   = [String]()
    
    var indexesSelected   = [Int]()
    var vehicleSelected:Int = -1
    var makeSelected:Int    = -1
    var modelSelected:Int   = -1
    
    @IBAction func backToController(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func doneSignUp(_ sender: Any) {
        signupMethod()
    }
    
  
  
    @IBAction func selectVehiclePic(_ sender: Any) {
        selectImageTyep = 0
        selectImage()
    }
    
    @IBAction func selectvehicleDoc(_ sender: Any) {
        switch (sender as AnyObject).tag {
        case 1:
            selectImageTyep = 1
        case 2:
            selectImageTyep = 2
        case 3:
            selectImageTyep = 3
        default:
            selectImageTyep = 1
        }
        selectImage()
    }
    
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vehImage.image = nil
        insImage.image = nil
        regImage.image = nil
        perImage.image = nil
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        amazonWrapper.delegate = self
    }
    
    @IBAction func selectType(_ sender: Any) {
        

        self.view.endEditing(true)
        typeModelMake = 1
        performSegue(withIdentifier: "selectVehicleDetails", sender: nil)
    }
    
    
    @IBAction func selectMake(_ sender: Any) {
        self.view.endEditing(true)
        typeModelMake = 2
        performSegue(withIdentifier: "selectVehicleDetails", sender: nil)
        
    }
    
    @IBAction func selectModel(_ sender: Any) { //Need to send Make ID
        self.view.endEditing(true)
        if vehicleTypeID.isEmpty{
            self.present(Helper.alertVC(title: "Message", message:"Please Select the Vehicle Make"), animated: true, completion: nil)
        }else{
        typeModelMake = 3
        performSegue(withIdentifier: "selectVehicleDetails", sender: nil)
        }
        
    }
    
    @IBAction func selectSpecialities(_ sender: Any) {//Need to send vehicle ID
        if vehicleTypeID.isEmpty{
            self.present(Helper.alertVC(title: "Message", message:"Please Select the Vehicle Type"), animated: true, completion: nil)
        }else{
            typeModelMake = 4
            performSegue(withIdentifier: "selectVehicleDetails", sender: nil)
        }
        self.view.endEditing(true)
    }
    
    
    func uploadVehicleimgToAmazon(){
        var url = String()
        url = AMAZONUPLOAD.VEHICLEIMAGE + plateNumber.text! + ".png"
        amazonWrapper.uploadImageToAmazon(withImage: vehicleImage.image!, imgPath: url)
    }
    
    
    func uploadInsuranceToAmazon(){
        var insUrl = String()
        insUrl = AMAZONUPLOAD.INSURANCE + vehicleType.text! + "ins" + ".png"
        amazonWrapper.uploadImageToAmazon(withImage: insuranceImage.image!, imgPath: insUrl)
        
        var perUrl = String()
        perUrl = AMAZONUPLOAD.INSURANCE + vehicleType.text! + "permit" + ".png"
        amazonWrapper.uploadImageToAmazon(withImage: insuranceImage.image!, imgPath: perUrl)
        
        var regUrl = String()
        regUrl = AMAZONUPLOAD.INSURANCE + vehicleType.text! + "reg" + ".png"
        amazonWrapper.uploadImageToAmazon(withImage: insuranceImage.image!, imgPath: regUrl)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectVehicleDetails" {
            let nextScene = segue.destination as? VehicleTypeModelMakeVC
            nextScene?.delegate = self
            if typeModelMake == 1{
                nextScene?.type = typeModelMake
                nextScene?.alreadySelectedIndex = vehicleSelected
            }else if typeModelMake == 2{
                nextScene?.type = typeModelMake
                nextScene?.alreadySelectedIndex = makeSelected
            }else if typeModelMake == 3{
                nextScene?.type = typeModelMake
                nextScene?.idMakeSeleted = vehicleMakeID
                nextScene?.alreadySelectedIndex = modelSelected
            }else if typeModelMake == 4{
                nextScene?.type = typeModelMake
                nextScene?.idMakeSeleted = vehicleTypeID
                nextScene?.alreadySelectedIndexes = indexesSelected
                
            }
        }else if segue.identifier == "signupVerification"{
            let nextScene = segue.destination as? VerifyOTPVC
            nextScene?.defineTheOtp = 1   // for signup 1
            nextScene?.mobileNumber = String(describing: signupParams["ent_contry_code"]!) + String(describing: signupParams["ent_mobile"]!)
            nextScene?.signupParams = signupParams
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            self .moveViewUp(activeTextField, andKeyboardHeight: Float(keyboardSize.height))
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //**hide the keyboard**//
    func dismisskeyBord() {
        view.endEditing(true)
           moveViewDown()
    }
    
    // move view to current position
    func moveViewDown() {
        self.mainSrollView.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(0))
    }
    
    //*******move view according to the textfield to show ********//
    func moveViewUp(_ textfield: UIView, andKeyboardHeight height: Float) {
        
        var  textFieldMaxY:Float = Float(textfield.frame.maxY)+10
        
        var view: UIView? = textfield.superview
        while view != self.view.superview {
            textFieldMaxY += Float((view?.frame.minY)!)
            view = view?.superview
        }
        let remainder: Float? = Float((self.view.window?.frame.height)!) - (textFieldMaxY + height)
        if remainder! >= 0 {
            
        }
        else {
            UIView.animate(withDuration: 0.4, animations: {() -> Void in
                self.mainSrollView.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(-remainder!))
            })
        }
    }
    
    //**************** Textfield delegate methods *******************//
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        dismisskeyBord()
        return true
    }
    
    /// cheking the vehicle data*********//
    func signupMethod(){
        if (plateNumber.text?.isEmpty)!{
            self.present(Helper.alertVC(title: vehicleDetails.Message! as NSString, message:vehicleDetails.vehicleNumber! as NSString), animated: true, completion: nil)
        }
        else if (vehicleSpecialities.text?.isEmpty)!{
            self.present(Helper.alertVC(title: vehicleDetails.Message! as NSString, message:vehicleDetails.vehicleColor! as NSString), animated: true, completion: nil)
        }
        else if (vehicleType.text?.isEmpty)!{
            self.present(Helper.alertVC(title: vehicleDetails.Message! as NSString, message:vehicleDetails.vehicleType! as NSString), animated: true, completion: nil)
        }
            
        else if (vehicleMake.text?.isEmpty)!{
            self.present(Helper.alertVC(title: vehicleDetails.Message! as NSString, message:vehicleDetails.vehicleMake! as NSString), animated: true, completion: nil)
        }
        else if (vehicleModel.text?.isEmpty)!{
            self.present(Helper.alertVC(title: vehicleDetails.Message! as NSString, message:vehicleDetails.vehicleModel! as NSString), animated: true, completion: nil)
        }
        else if vehImage.image == nil{
            self.present(Helper.alertVC(title: "Message", message:"Please Select the vehicle Image"), animated: true, completion: nil)
        }
        else if insImage.image == nil{
            self.present(Helper.alertVC(title: "Message", message:"Please Provider Vehicle Insurance"), animated: true, completion: nil)
        }
        else if  regImage.image == nil{
            self.present(Helper.alertVC(title: "Message", message:"Please Provider Vehicle Registation"), animated: true, completion: nil)
        }
        else if perImage.image == nil{
            self.present(Helper.alertVC(title: "Message", message:"Please Provider Vehicle Permit"), animated: true, completion: nil)
        }
        else{
            
            let vehicleIMG      = Utility.amazonUrl + AMAZONUPLOAD.VEHICLEIMAGE + plateNumber.text! + ".png"
            let reg_cer         = Utility.amazonUrl + AMAZONUPLOAD.INSURANCE + vehicleType.text! + "reg" + ".png"
            let motor_insurance = Utility.amazonUrl + AMAZONUPLOAD.INSURANCE + vehicleType.text! + "ins" + ".png"
            let carrierPermin   = Utility.amazonUrl + AMAZONUPLOAD.INSURANCE + vehicleType.text! + "permit" + ".png"
            
            
            signupParams.updateValue(motor_insurance, forKey: "ent_insurance_photo")
            signupParams.updateValue(reg_cer, forKey: "ent_reg_cert")
            signupParams.updateValue(carrierPermin, forKey: "ent_carriage_permit")
            
            signupParams.updateValue(plateNumber.text!, forKey: "ent_plat_no")
            signupParams.updateValue(vehicleTypeID, forKey: "ent_type")
            signupParams.updateValue(specialitiesIds, forKey: "ent_specialities")
            signupParams.updateValue(vehicleMakeID, forKey: "ent_make")
            signupParams.updateValue(vehicleModelID, forKey: "ent_model")
            signupParams.updateValue(vehicleIMG, forKey: "ent_vehicleImage")
            
            print(vehicleIMG)
            sendRequestForSignup()
            uploadInsuranceToAmazon()
            uploadVehicleimgToAmazon()
        }
    }
    
    
    func sendRequestForSignup() {
        
        Helper.showPI(message:"Signing up..")
        let params : [String : Any] =       ["mobile"   : String(describing: signupParams["ent_contry_code"]!) + String(describing: signupParams["ent_mobile"]!),// need to add code
            "userType" : "2"]  //1:Slave 2:Master
        
        print("getOtp parameters :",params)
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.SignupGetOTP,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("signup response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        // performSegue(withIdentifier:segues.vehicleDetails! , sender: nil)
                                        let flag:Int = response["errFlag"] as! Int
                                        if flag == 0{
                                            self.performSegue(withIdentifier:"signupVerification" , sender: nil)
                                        }else{
                                            Helper.hidePI()
                                            self.present(Helper.alertVC(title: "Message", message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                        }
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }

    
    
    func signUpDone(){
        let controller = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.signIN)
        navigationController?.viewControllers = [controller!]
    }

    
    /*****************************************************************/
    //MARK: - UIImage Picker
    /*****************************************************************/
    
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: vehicleDetails.selectImage as String?,
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: vehicleDetails.cancel as String?,
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: vehicleDetails.gallery as String?,
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: vehicleDetails.camera as String?,
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //********** navigate to gallery*********//
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    //********** navigate to camera*********//
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerControllerSourceType.camera;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    /*****************************************************************/
    //MARK: - UIImage Picker Delegate Method
    /*****************************************************************/
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
      
        switch selectImageTyep {
        case 0:
            vehicleImage.image = image
            vehImage.image = image
            break
        case 1:
            insuranceImage.image = image
            insImage.image = image
            break
        case 2:
            registrationImage.image = image
            regImage.image = image
            break
        case 3:
            permitImage.image = image
            perImage.image = image
            break
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }
}


extension VehicleDetailsVC:vehicleDetailsDelegate{
    internal func didSelectMakeModelType(vehicleData: VehicleType)
    {
        if vehicleData.type == 1{
            vehicleType.text = vehicleData.vehicleTypeMakeModel
            vehicleTypeID = vehicleData.idSelected
            vehicleSpecialities.text = ""
            indexesSelected = [Int]()
            vehicleSelected = vehicleData.selIndex
            
        }else if vehicleData.type == 2{
            vehicleMake.text = vehicleData.vehicleTypeMakeModel
            vehicleMakeID = vehicleData.idSelected
            vehicleModel.text = ""
            modelSelected = -1
            makeSelected = vehicleData.selIndex
            
        }else if vehicleData.type == 3{
            vehicleModel.text = vehicleData.vehicleTypeMakeModel
            vehicleModelID = vehicleData.idSelected
            modelSelected = vehicleData.selIndex
        }
        else{
            vehicleSpecialities.text = vehicleData.vehicleTypeMakeModel
            specialitiesIds = vehicleData.seletedIDs
            indexesSelected = vehicleData.selectedIndexes
        }
    }
}

extension VehicleDetailsVC: AmazonWrapperDelegate{
    func didImageUploadedSuccessfully(withDetails imageURL: String) {
        
    }
    
    func didImageFailtoUpload(_ error: Error?) {
        
    }
}

