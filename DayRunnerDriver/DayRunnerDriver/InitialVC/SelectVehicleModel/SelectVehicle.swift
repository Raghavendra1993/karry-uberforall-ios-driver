//
//  SelectVehicle.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 10/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SelectVehicle {
    
    let plateNumber :String!
    let vehicleType:String!
    let vehicleModel:String!
    let VehicleID:String!
    let typeId:String!
    var goodType = [String]()
    
    init(plateNo:String,vehType:String,vehModel:String,vehID:String,vehTypeID:String,goodTypes:[String]) {
        self.plateNumber = plateNo
        self.vehicleType = vehType
        self.vehicleModel = vehModel
        self.VehicleID = vehID
        self.typeId = vehTypeID
        self.goodType = goodTypes
    }
}

