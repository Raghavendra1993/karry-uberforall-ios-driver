//
//  SignInViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    
    var activeTextField = UITextField()
    
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var passwrodTextField: HoshiTextField!
    @IBOutlet var emailTextField: HoshiTextField!
    
    @IBOutlet var buttomSignupView: UIView!
    
    var sessionToken = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        emailTextField.text = Utility.savedID
        passwrodTextField.text = Utility.savedPassword
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        Helper.shadowView(sender: buttomSignupView, width:UIScreen.main.bounds.size.width, height:buttomSignupView.frame.size.height)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            self .moveViewUp(activeView: activeTextField, keyboardHeight: keyboardSize.height)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            // self.moveViewDown()
        }
    }
    
    //*****hide keyboard*****//
    func dismisskeyBord() {
        view.endEditing(true)
    }
    
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        // Get Max Y of Active View with respect their Super View
        var viewMAX_Y = activeView.frame.maxY
        
        // Check if SuperView of ActiveView lies in Nexted View context
        // Get Max for every Super of ActiveTextField with respect to Views SuperView
        if var view: UIView = activeView.superview {
            
            // Check if Super view of View in Nested Context is View of ViewController
            while (view != self.view.superview) {
                viewMAX_Y += view.frame.minY
                view = view.superview!
            }
        }
        
        // Calculate the Remainder
        let remainder = self.view.frame.height - (viewMAX_Y + keyboardHeight)
        
        // If Remainder is Greater than 0 does mean, Active view is above the and enough to see
        if (remainder >= 0) {
            // Do nothing as Active View is above Keyboard
        }
        else {
            // As Active view is behind keybard
            // ScrollUp View calculated Upset
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            self.mainScrollView.contentOffset = CGPoint(x: 0, y: -remainder)
            })
        }
        
        // Set ContentSize of ScrollView
        //        var contentSizeOfContent: CGSize = self..frame.size
        //        contentSizeOfContent.height += keyboardHeight
        //        self.mainScrollView.contentSize = contentSizeOfContent
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        // Resign back to Normal Position having set Content Size as Initial
        self.mainScrollView.contentSize = self.mainScrollView.frame.size
    }
    
    
    //******** Moves to retrive password*********//
    @IBAction func forgotPasswordAction(_ sender: Any) {
        performSegue(withIdentifier: "toForgotPassword", sender: nil)
    }
    
    @IBAction func signInAction(_ sender: Any) {
        signinMethod()
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        performSegue(withIdentifier: "toSignup", sender: nil)
    }
    
    ///*************checking the textfield data*******//
    func signinMethod(){
        if (emailTextField.text?.isEmpty)!{
            self.present(Helper.alertVC(title: "Message", message:"Please enter Email Address"), animated: true, completion: nil)
        }
        else if (passwrodTextField.text?.isEmpty)!{
            self.present(Helper.alertVC(title: "Message", message:"Please enter Password"), animated: true, completion: nil)
        }
        else{
            sendRequestForSignup()
        }
    }
    
    func sendRequestForSignup() {
        
        Helper.showPI(message:"Signing in..")
        let params : [String : Any] =  ["mobile"         : emailTextField.text!,
                                        "password"       : passwrodTextField.text!,
                                        "ent_deviceId"   : Utility.deviceId,
                                        "ent_push_token" : Utility.pushToken ,
                                        "ent_appversion" : Utility.appVersion,
                                        "ent_devMake"    : "Apple",
                                        "ent_devModel"   : Utility.modelName,
                                        "ent_devtype"    : "1",
                                        "ent_device_time": Helper.currentGMTDateTime]
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.LOGIN,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("signup response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        let flag:Int = response["errFlag"] as! Int
                                        
                                        if flag == 0{
                                            
                                            if let dict = response["data"] as? NSDictionary {
                                                let defaults = UserDefaults.standard
                                                defaults.set(self.passwrodTextField.text! ,   forKey: USER_INFO.OLDPASSWORD)
                                                defaults.set(dict["publishChn"]   as! String, forKey: USER_INFO.DRIVERPUBCHANNEL)
                                               // defaults.set(dict["token"]        as! String, forKey: USER_INFO.SESSION_TOKEN)
                                                defaults.set(dict["code"]         as! String, forKey: USER_INFO.REFERRAL_CODE)
                                                defaults.set(dict["mid"]          as! String, forKey: USER_INFO.USER_ID)
                                                defaults.set(dict["chn"]          as! String, forKey: USER_INFO.DRIVERCHANNEL)
                                                defaults.set(dict["presence_chn"] as! String, forKey: USER_INFO.PRESENCECHANNEL)
                                                defaults.set(dict["pub_key"]      as! String, forKey: USER_INFO.PUBLISHKEY)
                                                defaults.set(dict["sub_key"]      as! String, forKey: USER_INFO.SUBSCRIBEKEY)
                                                defaults.set(dict["server_chn"]   as! String, forKey: USER_INFO.SERVERCHANNEL)
                                                
                                                defaults.set(self.emailTextField.text!, forKey: USER_INFO.SAVEDID)
                                                
                                                defaults.set(self.passwrodTextField.text!, forKey: USER_INFO.SAVEDPASSWORD)
                                                
                                                defaults.synchronize()
                                                self.sessionToken = dict["token"] as! String
                                                self.performSegue(withIdentifier: "toSelectVehicle", sender: dict["vehicles"])
                                            }
                                        }else{
                                            self.present(Helper.alertVC(title: "Message", message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                        }
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toSelectVehicle" {
            let nav =  segue.destination as! UINavigationController
            let nextScene = nav.viewControllers.first as! SelectVehicleVC?
            nextScene?.vehicles = (sender as? [[String : Any]])!
             nextScene?.token = sessionToken
        }
    }
}

extension SignInViewController : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        if textField .isEqual(emailTextField){
            passwrodTextField.becomeFirstResponder()
        }
        else{
            dismisskeyBord()
        }
        return true
    }
}
