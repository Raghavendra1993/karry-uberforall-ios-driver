//
//  ChangePasswordVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 19/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet var reEnterPassword: HoshiTextField!
    @IBOutlet var newPassword: HoshiTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    //************* hide the keyboard************//
    @IBAction func hideKeyboard(_ sender: Any) {
         self.view.endEditing(true)
        
    }
    
    ///Save the new password*******//
    @IBAction func saveTheNewpassword(_ sender: Any) {
        if (newPassword.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message", message:"Please enter password"), animated: true, completion: nil)
        }else if (reEnterPassword.text?.isEmpty)! {
            self.present(Helper.alertVC(title: "Message", message:"Please Re-enter  password"), animated: true, completion: nil)
        }else if newPassword.text == reEnterPassword.text{
            self.present(Helper.alertVC(title: "Message", message:"password mismatched"), animated: true, completion: nil)
        }else{
            self.updateName()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backToVC(_ sender: Any) {
       dismiss(animated: true, completion: nil)
    }
    
    func updateName(){
        Helper.showPI(message:"Loading..")
        let params : [String : Any] =  ["token"       : Utility.sessionToken,
                                        "ent_password": newPassword.text!,
                                        ]
        
        NetworkHelper.requestPUT(serviceName:API.METHOD.UPDATEPROFILE,
                                 params: params,
                                 success: { (response : [String : Any]) in
                                    print("UpdateDriverStatus response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        let defaults = UserDefaults.standard
                                        defaults.set(self.newPassword.text! , forKey: USER_INFO.OLDPASSWORD)
                                        defaults.synchronize()
                                        self.dismiss(animated: true, completion: nil)
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    func dismissView(){
        self.view.endEditing(true)
    }
}



// MARK: - Textfield delegate method
extension ChangePasswordVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        
        switch textField {
        case newPassword:
            reEnterPassword.becomeFirstResponder()
            break

        default:
     
            dismissView()
            break
        }
        
        return true
    }
}




