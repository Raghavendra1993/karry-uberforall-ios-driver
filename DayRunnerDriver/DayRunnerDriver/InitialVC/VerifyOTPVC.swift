//
//  VerifyOTPVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class VerifyOTPVC: UIViewController {
    var activeTextField = UITextField()
    @IBOutlet var textField1: UITextField!
    @IBOutlet var textField2: UITextField!
    @IBOutlet var textField3: UITextField!
    @IBOutlet var textField4: UITextField!
    
    @IBOutlet var contentScrollView: UIView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var buttomConstraint: NSLayoutConstraint!
    @IBOutlet var resendOTP: UIButton!
    var defineTheOtp:Int = 0   // 1: signup, 2:forgot passwrod, 3:change number
     var processType:Int = 0
    
    var mobileNumber = String()
    var signupParams = [String: Any]()
    
    @IBAction func backToController(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    //***********Move to vc to create new password*******//
    @IBAction func verifyOTP(_ sender: Any) {
        if self.defineTheOtp == 1{  // signup
            processType = 2
            verifyOTP()
        }else if self.defineTheOtp == 2{ //forgot password
            processType = 1
            verifyOTP()
        }
        else{
            processType = 2
            verifyOTP()
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    
    //MARK: - Keyboard Methods
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            self.moveViewUp(activeView: textField1, keyboardHeight: keyboardSize.height)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveScrollViewDown()
        }
    }
    

    
    
    //******hide the keyboard******//
    @IBAction func hideKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    @IBAction func resendOtp(_ sender: Any) {
        if self.defineTheOtp == 2{
            self.resendOtpForChangePassword()
        }else{
            self.resendOtpForSignup()
        }
    }
    
    func resendOtpForChangePassword(){
        
        
        Helper.showPI(message:"Loading..")
        let params : [String : Any] =  [
            "ent_email_mobile":mobileNumber,
            "userType":2,
            "ent_type":1
        ]
        
        print("params :",params)
        NetworkHelper.requestPOST(serviceName:API.METHOD.FORGOTPASSWORD,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        let flag:Int = response["errFlag"] as! Int
                                        if flag == 1{
                                                Helper.hidePI()
                                            self.present(Helper.alertVC(title: signup.Message! as NSString, message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                        }else{
                                            self.present(Helper.alertVC(title: signup.Message! as NSString, message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                        }
                                    }
        })
        { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    func resendOtpForSignup() {
        
        Helper.showPI(message:"Signing up..")
        let params : [String : Any] = [ "mobile"   : mobileNumber,// need to add code
            "userType" : "2"]  //1:Slave 2:Master
        
        print("getOtp parameters :",params)
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.SignupGetOTP,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("signup response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        // performSegue(withIdentifier:segues.vehicleDetails! , sender: nil)
                                        let flag:Int = response["errFlag"] as! Int
                                        if flag == 0{
                                            Helper.hidePI()
                                            self.present(Helper.alertVC(title: "Message", message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                        }else{
                                            Helper.hidePI()
                                            self.present(Helper.alertVC(title: "Message", message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                        }
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
              Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    

    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func signUpDone(){
        let controller = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.signIN)
        navigationController?.viewControllers = [controller!]
        
    }
    
    func verifyOTP() {
        
        if (textField1.text?.isEmpty)! || (textField1.text?.isEmpty)! || (textField1.text?.isEmpty)! || (textField1.text?.isEmpty)!{
            self.present(Helper.alertVC(title: "Message", message:"Please fill the OTP"), animated: true, completion: nil)
        }else{
            Helper.showPI(message:"Verifying OTP")
            let params : [String : Any] =  ["mobile"   : mobileNumber,
                                            "code"     : textField1.text!+textField2.text!+textField3.text!+textField4.text!,
                                            "processType":processType,
                                            "userType":2]
            
            NetworkHelper.requestPOST(serviceName:API.METHOD.verifyOTP,
                                      params: params,
                                      success: { (response : [String : Any]) in
                                        print("Verification Response : ",response)
                                        if response.isEmpty == false {
                                            let flag:Int = response["errFlag"] as! Int
                                            
                                            if flag == 0{
                                                if self.defineTheOtp == 1{
                                                    self.sendRequestForSignup()
                                                }else if self.defineTheOtp == 2{
                                                    Helper.hidePI()
                                                    self.performSegue(withIdentifier: "toCreateNewpassword", sender: nil)
                                                }else if self.defineTheOtp == 3{
                                                    self.updateNumber()
                                                    Helper.hidePI()
                                                }
                                            }else{
                                                Helper.hidePI()
                                                self.present(Helper.alertVC(title: "Message", message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                            }
                                        } else {
                                            
                                        }
                                        
            }) { (Error) in
                Helper.hidePI()
                self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
            }
        }
    }
    
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toCreateNewpassword" {
            let nextScene = segue.destination as? SetNewpasswordViewController
            nextScene?.secureOtp = textField1.text!+textField2.text!+textField3.text!+textField4.text!
            nextScene?.mobileNumber = mobileNumber
        }
    }
    
    func sendRequestForSignup() {
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.SIGNUP,
                                  params: signupParams,
                                  success: { (response : [String : Any]) in
                                    print("signup response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        let flag:Int = response["errFlag"] as! Int
                                        
                                        if flag == 0{
                                            self.handlingTheSignupResponse(responseData: [response])
                                        }else{
                                              Helper.hidePI()
                                            self.present(Helper.alertVC(title: "Message", message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                        }
                                        
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            Helper.hidePI()

            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    
    func updateNumber(){
        Helper.showPI(message:"Loading..")
        let params : [String : Any] =  ["token"     : Utility.sessionToken,
                                        "ent_mobile":mobileNumber,
                                        ]
        
        NetworkHelper.requestPUT(serviceName:API.METHOD.UPDATEPROFILE,
                                 params: params,
                                 success: { (response : [String : Any]) in
                                    print("UpdateDriverStatus response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        let flag:Int = response["errFlag"] as! Int
                                        
                                        if flag == 0{
                                            _ = self.navigationController?.popToRootViewController(animated: true)
                                        }else{
                                             Helper.hidePI()
                                            self.present(Helper.alertVC(title: "Message", message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                        }
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }

    
    
    func handlingTheSignupResponse(responseData:[[String: Any]]){
        
        self.signUpDone()
        self.present(Helper.alertVC(title: "Message", message:"Thanks for signing up. one of our representative will get in touch with you in the next 24 hours to setup your profile and get all the necessary documents"), animated: true, completion: nil)
    }
    
    
    /// Move View Up When keyboard Appears
    ///
    /// - Parameters:
    ///   - activeView: View that has Became First Responder
    ///   - keyboardHeight: Keyboard Height
    func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
        // Get Max Y of Active View with respect their Super View
        var viewMAX_Y = activeView.frame.maxY
        
        // Check if SuperView of ActiveView lies in Nexted View context
        // Get Max for every Super of ActiveTextField with respect to Views SuperView
        if var view: UIView = activeView.superview {
            
            // Check if Super view of View in Nested Context is View of ViewController
            while (view != self.view.superview) {
                viewMAX_Y += view.frame.minY
                view = view.superview!
            }
        }
        
        // Calculate the Remainder
        let remainder = self.view.frame.height - (viewMAX_Y + keyboardHeight)
        
        // If Remainder is Greater than 0 does mean, Active view is above the and enough to see
        if (remainder >= 0) {
            // Do nothing as Active View is above Keyboard
        }
        else {
            // As Active view is behind keybard
            // ScrollUp View calculated Upset
            UIView.animate(withDuration: 0.4,
                           animations: { () -> Void in
                            self.mainScrollView.contentOffset = CGPoint(x: 0, y: -remainder)
                            self.buttomConstraint.constant = keyboardHeight
                            self.view.layoutIfNeeded()
            })
        }
        
        // Set ContentSize of ScrollView
        var contentSizeOfContent: CGSize = self.contentScrollView.frame.size
        contentSizeOfContent.height += keyboardHeight
        self.mainScrollView.contentSize = contentSizeOfContent
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveScrollViewDown() {
        self.mainScrollView.contentOffset = CGPoint(x: CGFloat(0), y: CGFloat(0))
        let contentSizeOfContent: CGSize = self.contentScrollView.frame.size
        self.mainScrollView.contentSize = contentSizeOfContent
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttomConstraint.constant = 0
                        self.view.layoutIfNeeded()
        })
    }


}

/*****************************************************************/
//MARK: - UITextField Delegates
/*****************************************************************/
extension VerifyOTPVC: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        //print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        //print("TextField did end editing method called")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //print("TextField should begin editing method called")
        
        activeTextField = textField
        //        moveViewUp(textField, keyboardHeight: 220)
        
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        //print("TextField should clear method called")
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        //print("TextField should snd editing method called")
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
        //print("While entering the characters this method gets called")
        
        // This allows numeric text only, but also backspace for deletes
        if string.length > 0 && !string.isNumber {
            return false
        }
        
        // Hide Keyboard when you enter last digit
        if textField.isEqual(textField4) && !string.isEmpty {
            textField.text = string
            textField.resignFirstResponder()
        }
        
        let oldLength = textField.text!.length
        let replacementLength = string.length
        let rangeLength = range.length
        let newLength = oldLength - rangeLength + replacementLength
        
        
        // This 'tabs' to next field when entering digits
        if (newLength == 1) {
            
            if textField.isEqual(textField1) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: textField2, afterDelay: 0.05)
            }
            else if textField.isEqual(textField2) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: textField3, afterDelay: 0.05)
            }
            else if textField.isEqual(textField3) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: textField4, afterDelay: 0.05)
            }
        }
            //this goes to previous field as you backspace through them, so you don't have to tap into them individually
        else if (oldLength > 0 && newLength == 0) {
            
            if textField.isEqual(textField4) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: textField3, afterDelay: 0.05)
            }
            else if textField.isEqual(textField3) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: textField2, afterDelay: 0.05)
            }
            else if textField.isEqual(textField2) {
                self.perform(#selector(self.setNextResponder(nextResponder:)), with: textField1, afterDelay: 0.05)
            }
        }
        return newLength <= 1
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //print("TextField should return method called")
        
        return true
    }
    
    func setNextResponder(nextResponder: UITextField) {
        nextResponder.becomeFirstResponder()
    }
}



