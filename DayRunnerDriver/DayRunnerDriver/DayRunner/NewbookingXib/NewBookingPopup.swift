//
//  NewBookingPopup.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import AVFoundation

class NewBookingPopup: UIView {
    
    
    @IBOutlet var bid: UILabel!
    var player: AVAudioPlayer?
    @IBOutlet var progressView: CircleProgressView!
    @IBOutlet var pickAddress: UILabel!
    
    @IBOutlet var progressText: UILabel!
    @IBOutlet var dropTime: UILabel!
    @IBOutlet var pickTime: UILabel!
    
    @IBOutlet var deliveryFee: UILabel!
    @IBOutlet var dropAddress: UILabel!
    
    var timerProgress = Timer()
    
    let nf = NumberFormatter()
    var countDown:Int = 0
    var progressVal:Double = 0.0
    var countVal:Float = 0.1
    var proVal:Float = 1.0
    var expiryTime:Double = 0.0
    
    @IBOutlet var distanceLabel: UILabel!
    
    var viewController = UIApplication.shared.keyWindow?.rootViewController
    
    
    var bookingDict = [String:Any]()
    
        private static var share: NewBookingPopup? = nil
        var isShown:Bool = false
        
        static var instance: NewBookingPopup {
            
            if (share == nil) {
                share = Bundle(for: self).loadNibNamed("Newbooking",
                                                       owner: nil,
                                                       options: nil)?.first as? NewBookingPopup
            }
            return share!
    }
    
    func newbookingDic(dict:[String:Any]){
        self.playSound()
        bookingDict =  dict
        var expTime:Int = 0
        self.progressText.text = String(describing: dict["ExpiryTimer"])
        expTime = Int(dict["ExpiryTimer"] as! String)!
        countDown =   expTime - 4
        expiryTime = Double(expTime)
        pickAddress.text  = dict["adr1"] as? String
        dropAddress.text  = dict["drop1"] as? String
        deliveryFee.text  = Utility.currencySymbol + String(describing:bookingDict["amount"]!)
        dropTime.text  = dict["dropDt"] as? String
        pickTime.text  = dict["dt"] as? String
        nf.numberStyle = NumberFormatter.Style.none
        nf.maximumFractionDigits = 2
         bid.text = "BID:" + String(describing:bookingDict["bid"] as! NSNumber)
        timerProgress =  Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
        
        let ud = UserDefaults.standard
        ud.set(false, forKey: "isSound")
        ud.synchronize()
    }
    
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "taxina-1", withExtension: "mp3") else {
            print("error")
            return
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
            player.numberOfLoops = 1
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    func timerTick(){
        progressVal = progressVal + 1.0
        
        let val:Double = 1/expiryTime
        
        print(val)
        
        self.progressView.progress = ((1.0) - (val * progressVal))
        
        //   if (Float(Double(1/300)) * Float(progressVal)) == countVal  {
        //       if countVal * 10 == proVal  {
        //           proVal = proVal + 1
        countDown = countDown - 1;
        self.progressText.text = "" + nf.string(from: NSNumber(value: self.countDown))!
        //         }
        //          countVal = countVal + 0.1;
        //      }
        
        if countDown <= 0 {
            timerProgress.invalidate()
            hide()
        }
    }
    
    @IBAction func makeTheServiceCall(_ sender: Any) {
        respondingToAppointment(status:"6" )
           timerProgress.invalidate()
    }
    @IBAction func rejectButtonAction(_ sender: Any) {
        respondingToAppointment(status:"3" )
        timerProgress.invalidate()
    }
    
    func respondingToAppointment(status:String) {
        Helper.showPI(message:"Loading..")
        
        let params : [String : Any] =  ["token"     : Utility.sessionToken,
                                        "ent_status": status,
                                        "ent_booking_id" :bookingDict["bid"]! as Any ]
        
        print(params)
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.RESPONDAPPT,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("Newbooking Accepted response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        let flag:Int = response["errFlag"] as! Int
                                        
                                        if flag == 0{
                                            self.hide()
                                            if status == "6"{
                                                // Define identifier
                                                let notificationName = Notification.Name("gotNewBooking")
                                                
                                                // Post notification
                                                NotificationCenter.default.post(name: notificationName, object: nil)
                                            }
                                        }else{
                                            self.hide()
                                            self.viewController?.present(Helper.alertVC(title: "Message", message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                            
                                            //
                                            //                                            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                                            //                                                while let presentedViewController = topController.presentedViewController {
                                            //                                                    topController = presentedViewController
//                                                     topController.present(Helper.alertVC(title: "Message", message:response["errMsg"] as! NSString), animated: true, completion: nil)
//                                                }
//                                            }
                                        }
                
                                    } else {
                                    }
                                    
        }) { (Error) in
            
                  self.hide()
        }
    }
    

        /// Show Network
        func show() {
            if isShown == false {
                isShown = true
                let window = UIApplication.shared.keyWindow
                self.frame = (window?.frame)!
                window?.addSubview(self)
                
                self.alpha = 0.0
                UIView.animate(withDuration: 0.5,
                               animations: {
                                self.alpha = 1.0
                })
            }
        }
        
        /// Hide
        func hide() {
            player?.stop()
            timerProgress.invalidate()
            if isShown == true {
                isShown = false
                
                UIView.animate(withDuration: 0.5,
                               animations: {
                                NewBookingPopup.share = nil
                                self.alpha = 0.0
                },
                               completion: { (completion) in
                                self.removeFromSuperview()
                })
            }
        }
}
