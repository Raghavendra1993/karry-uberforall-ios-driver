//
//  HistModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 23/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import Foundation

struct Sale {
    var month: String
    var value: Double
}

class DataGenerator {
    
    static var randomizedSale: Double {
        return Double(arc4random_uniform(10000) + 1) / 10
    }
    
    static func data() -> [Sale] {
        let months =  ["MON","TUE","WED","THU","FRI","SAT","SUN"]
        var sales = [Sale]()
        
        for month in months {
            let sale = Sale(month: month, value: randomizedSale)
            sales.append(sale)
        }
        
        return sales
    }
}
