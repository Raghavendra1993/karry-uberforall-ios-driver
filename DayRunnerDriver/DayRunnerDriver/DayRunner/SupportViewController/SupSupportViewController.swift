//
//  SupSupportViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 10/06/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SupSupportViewController: UIViewController {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var supportVC: UITableView!
    var selectedIndex:Int = 0
    var supportDict = [Support]()
    var subSupportDict = [SubSupport]()
    
    override func viewDidLoad() {
        self.titleLabel.text = supportDict[selectedIndex].supportName
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.parseTheSubCategories(subDict:supportDict[selectedIndex].supportArray)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func parseTheSubCategories(subDict:[[String:Any]]){
        for dict in subDict {
            let notes    = dict["Name"] as? String
            let custChn    = dict["desc"] as? String
            subSupportDict = [SubSupport.init(name: notes!, description: custChn!)]
        }
        self.supportVC.reloadData()
    }
    
    @IBAction func backToVC(_ sender: Any) {
          _ = navigationController?.popToRootViewController(animated: true)
    }

}

///*********** tableview datasource************///
extension SupSupportViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return subSupportDict.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SubSupportTableViewCell = tableView.dequeueReusableCell(withIdentifier: "subSupport") as! SubSupportTableViewCell!
        cell.subSupportLabel.text = subSupportDict[indexPath.row].supName
        
        return cell
    }
}

///*********** tableview Delegate************///
extension SupSupportViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
