//
//  SupportModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 10/06/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit

class Support {
    
    let supportName :String!
    var supportArray:[[String:Any]]
    
    init(supName:String,supArray:[[String:Any]]) {
        self.supportName       = supName
        self.supportArray = supArray
    }
}

class SubSupport {
    
    let supName :String!
    var desc:String!
    
    init(name:String,description:String) {
        self.supName       = name
        self.desc    = description
    }
}

