//
//  InviteViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 15/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Social
import MessageUI

class InviteViewController: UIViewController,MFMailComposeViewControllerDelegate {
    @IBOutlet var referralCode: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        referralCode.text = Utility.referralCode
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0x23C590)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func shareCodeWithMessage(_ sender: Any) {
        let messageVC = MFMessageComposeViewController()
        
         let referralText = "ReferralCode:" + referralCode.text!
        
        messageVC.body = referralText
        messageVC.recipients = ["Enter tel-nr"]
        messageVC.messageComposeDelegate = self;
        
        self.present(messageVC, animated: false, completion: nil)
      }
    
    @IBAction func shareCodeWithEmail(_ sender: Any) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    @IBAction func shareCodeWithTwitter(_ sender: Any) {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter) {
            
            let tweetShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            
            let referralText = "ReferralCode:" + referralCode.text!
            
            tweetShare.setInitialText(referralText)
            
        
            self.present(tweetShare, animated: true, completion: nil)
            
        } else {
            
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account to tweet.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func shareCodeWithFB(_ sender: Any) {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
            let fbShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
              let referralText = "ReferralCode:" + referralCode.text!
            fbShare.setInitialText(referralText)
            
            self.present(fbShare, animated: true, completion: nil)
            
        } else {
            let alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func menuButton(_ sender: Any) {
          slideMenuController()?.toggleLeft() 
    }
    func showSendMailErrorAlert() {
        
        let sendMailErrorAlert = UIAlertController(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", preferredStyle: .alert)
        sendMailErrorAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(sendMailErrorAlert, animated: true, completion:nil)
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
     //   mailComposerVC.setToRecipients(["teja@mobifyi.com"])
            let referralText = "ReferralCode:" + referralCode.text!
        mailComposerVC.setSubject("DayRunnerDriver ReferralCode" + referralText)
        mailComposerVC.setMessageBody("Hi, your are using DayRunner app....", isHTML: false)
        
        return mailComposerVC
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
 }
extension InviteViewController : MFMessageComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
