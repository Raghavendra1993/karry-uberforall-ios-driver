
//
//  CheckOldPassword.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 17/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit

protocol CheckPasswordDelegate:class
{
    func AuthorisedToChangeThePassword()
}

class CheckOldPassword: UIView {
    open weak var delegate: CheckPasswordDelegate?
    private static var share: CheckOldPassword? = nil
    var isShown:Bool = false
    
    @IBOutlet var instructionLabel: UILabel!
    @IBOutlet var textField: HoshiTextField!
    static var instance: CheckOldPassword {
        
        if (share == nil) {
            share = Bundle(for: self).loadNibNamed("ChangePassword",
                                                   owner: nil,
                                                   options: nil)?.first as? CheckOldPassword
        }
        return share!
    }
    
    @IBAction func tagestureActionOnContentView(_ sender: Any) {
          self.endEditing(true)
    }
    @IBAction func tapGestureAction(_ sender: Any) {
        self.hide()
        self.endEditing(true)
    }
    @IBAction func checkTheOldPassword(_ sender: Any) {
        
        if textField.text == Utility.checkOldPassword {
            self.delegate?.AuthorisedToChangeThePassword()
            self.hide()
        }else{
            
            self.instructionLabel.isHidden = false
            self.endEditing(true)
        }
    }
    
    
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            self .moveViewUp(activeView: textField, keyboardHeight: keyboardSize.height)
        }
    }
    

    
    /// Show Network
    func show() {
          self.instructionLabel.isHidden = true
        textField.text = ""
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }
    
    /// Hide
    func hide() {
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
     func moveViewUp(activeView: UIView, keyboardHeight: CGFloat) {
        
       
    }
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        
        })
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
          self.instructionLabel.isHidden = true
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        self.endEditing(true)
        return true
    }
    
}


