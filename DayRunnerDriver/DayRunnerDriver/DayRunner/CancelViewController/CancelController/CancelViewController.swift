//
//  CancelViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 25/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
protocol cancelBookingDelegate:class
{
    func cancelReason()
}

class CancelViewController: UIViewController {
        open weak var delegate: cancelBookingDelegate?
    @IBOutlet var commentsTextView: UITextView!
    @IBOutlet var cancelTableView: UITableView!
       @IBOutlet var ConfirmAction: UIButton!
    
    var selectedIndex:Int = -1
    var cancelReasons = [String]()
    
    var bookingID = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        getTypeMakeNModel()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
           // self.moveViewDown()
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
           // self .moveViewUp(activeView: activeTextField, keyboardHeight: keyboardSize.height)
        }
    }

    
    @IBAction func confirmCancellation(_ sender: Any) {
        if selectedIndex == -1 && commentsTextView.text == "Add the comments here..."{
            self.present(Helper.alertVC(title: "Message", message:"Please Select Cancellation Reason (or) Provide your reason"), animated: true, completion: nil)
        }else{
            cancelBooking()
        }
    }
    
    @IBAction func tapgestureAction(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func backToVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    func selectReason(_ sender: UIButton) {
        if selectedIndex == sender.tag % 1000{
            selectedIndex = -1
            ConfirmAction.isSelected = false
        }else{
            selectedIndex = sender.tag % 1000
            ConfirmAction.isSelected = true
        }
        cancelTableView.reloadData()
    }
    
    
    func getTypeMakeNModel(){
        Helper.showPI(message: "loading..")
        var serviceName = String()
        
        serviceName = "0"
        
        NetworkHelper.requestGETURL(method: API.METHOD.CANCELREASONS + serviceName,
                                    success: { (response) in
                                        Helper.hidePI()
                                        if response.isEmpty == false {
                                            self.cancelReasons = response["data"] as! [String]
                                            self.cancelTableView.reloadData()
                                        } else {
                                            
                                        }
                                        
        })
        { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    func cancelBooking(){
        var reason = String()
        
        if selectedIndex == -1 {
            reason = commentsTextView.text
        }else{
          reason = cancelReasons[selectedIndex]
        }
        
        Helper.showPI(message:"Loading..")
        let params : [String : Any] =  ["token"       : Utility.sessionToken,
                                        "ent_booking_id": bookingID,
                                        "ent_status":4,
                                        "ent_reason":reason
        ]
        
        NetworkHelper.requestPUT(serviceName:API.METHOD.CANCELBOOKING,
                                 params: params,
                                 success: { (response : [String : Any]) in
                                    print("UpdateDriverStatus response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        self.delegate?.cancelReason()
                                        self.dismiss(animated: true, completion: nil)
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
   
}

///****** tableview datasource*************//
extension CancelViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.cancelReasons.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"cancelBooking") as! CancelTableViewCell!
        if selectedIndex == indexPath.row {
            cell?.cancelReason.isSelected = true
        }else{
          cell?.cancelReason.isSelected = false
        }
        cell?.cancelReason.setTitle(self.cancelReasons[indexPath.row], for: .normal)
         cell?.cancelReason.setTitle(self.cancelReasons[indexPath.row], for: .highlighted)
        cell?.cancelReason.tag = indexPath.row + 1000
        cell?.cancelReason.addTarget(self, action: #selector(self.selectReason), for: .touchUpInside)

        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50;
    }
}

//MARK:- Tableview delegate methods**************//
extension CancelViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.reloadData()
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        selectedIndex = -1
    }
}

extension CancelViewController:UITextViewDelegate{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        textView.text = ""
        return true
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            self.view.endEditing(true)
             return false
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.characters.count < 1{
            textView.text = "Add the comments here..."
            ConfirmAction.isSelected = false
        }else{
           ConfirmAction.isSelected = true
        }
    }
}

