//
//  CancelTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 25/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class CancelTableViewCell: UITableViewCell {

    @IBOutlet var cancelReason: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
