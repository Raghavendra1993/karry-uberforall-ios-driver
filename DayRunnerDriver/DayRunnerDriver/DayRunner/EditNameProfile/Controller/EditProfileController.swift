//
//  EditProfileController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 20/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class EditProfileController: UIViewController {

    @IBOutlet weak var buttonBottomConst: NSLayoutConstraint!
    var name : NSString = ""
    var text : NSString = ""
    var activeTextField = UITextField()
    @IBOutlet weak var textfield: HoshiTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saveName(_ sender: Any) {
      self.moveViewDown()
        if (textfield.text?.isEmpty)! {
             self.present(Helper.alertVC(title: "Message", message:"Field shouldn't be empty"), animated: true, completion: nil)
           
        }else{
            self.updateName()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    func dismissKeyboard() {
        
        view.endEditing(true)
    }
    
    func updateName(){
        Helper.showPI(message:"Loading..")
        let params : [String : Any] =  ["token"     : Utility.sessionToken,
                                        
                                        "ent_name" :textfield.text as Any]
        
        NetworkHelper.requestPUT(serviceName:API.METHOD.UPDATEPROFILE,
                                 params: params,
                                 success: { (response : [String : Any]) in
                                    print("UpdateDriverStatus response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        self.dismiss(animated: true, completion: nil)
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
        
            dismiss(animated: true, completion: nil)
    }

    //MARK: - Keyboard Methods
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            self .moveViewUp(keyboardHeight: keyboardSize.height)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }
    
    //MARK:-Move View Up
    func moveViewUp(keyboardHeight: CGFloat) {
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttonBottomConst.constant = keyboardHeight
                        self.view.layoutIfNeeded()
        })
    }
    
    
    //MARK:- Move View Down
    func moveViewDown() {
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttonBottomConst.constant = 0
                        self.view.layoutIfNeeded()
        })
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
    }
}

