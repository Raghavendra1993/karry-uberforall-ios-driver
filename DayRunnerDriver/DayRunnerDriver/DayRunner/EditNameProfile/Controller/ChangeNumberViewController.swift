//
//  ChangeNumberViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 17/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ChangeNumberViewController: UIViewController {
    
    @IBOutlet var countryImage: UIImageView!
    @IBOutlet var countryCode: UILabel!
    @IBOutlet var buttonContraint: NSLayoutConstraint!
    @IBOutlet var phoneTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0x23C590)
        self.setCountryCode()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    
    //MARK: - Keyboard Methods
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        {
            self.moveViewUp(keyboardHeight: keyboardSize.height)
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            self.moveViewDown()
        }
    }

    
    @IBAction func pickTheCountryCode(_ sender: Any) {
        performSegue(withIdentifier:"pickCountryCodeFromFP", sender: nil)
    }
    
    
    @IBAction func verifyOTP(_ sender: Any) {
          self.moveViewDown()
        self.view.endEditing(true)
        if (phoneTF.text?.isEmpty)! {
                 self.present(Helper.alertVC(title: "Message", message:"Please enter the number"), animated: true, completion: nil)
        }else
        {
           self.changeWithVerification()
        }
    }
    
    // MARK: - setCountrycode
    func setCountryCode(){
        let picker = CountryPicker.dialCode(code: "IN")
        countryCode.text = picker.dialCode
        countryImage.image = picker.flag
    }
    

  
    @IBAction func backToVC(_ sender: Any) {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func hideKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }

    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "pickCountryCodeFromFP"
        {
            let nav = segue.destination as! UINavigationController
            if let picker: CountryPicker = nav.viewControllers.first as! CountryPicker?
            {
                picker.delegate = self
            }
        }else if segue.identifier == "toVerifyTheNumber" {
            let nextScene = segue.destination as? VerifyOTPVC
            nextScene?.mobileNumber = countryCode.text! + phoneTF.text!
            nextScene?.defineTheOtp = 3
        }
    }
    
    func changeWithVerification() {
        
        Helper.showPI(message:"Signing up..")
        let params : [String : Any] =       ["mobile"   : countryCode.text! + phoneTF.text!,
                                             "userType" : "2"]  //1:Slave 2:Master
        
        print("getOtp parameters :",params)
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.SignupGetOTP,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("signup response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        self.performSegue(withIdentifier:"toVerifyTheNumber", sender: nil)
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    
    func moveViewUp(keyboardHeight: CGFloat) {
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttonContraint.constant = keyboardHeight
                        self.view.layoutIfNeeded()
        })
    }
    
    
    
    
    /// Move View Down / Normal Position When keyboard disappears
    func moveViewDown() {
        
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in
                        self.buttonContraint.constant = 0
                        self.view.layoutIfNeeded()
        })
    }
    
    

}

extension ChangeNumberViewController:CountryPickerDelegate{
    // MARK: - country delegate method
    internal func didPickedCountry(country: Country)
    {
        countryCode.text = country.dialCode
        countryImage.image = country.flag
    }
}

