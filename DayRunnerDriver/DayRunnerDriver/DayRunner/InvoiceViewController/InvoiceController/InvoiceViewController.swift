
//
//  InvoiceViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import FloatRatingView

enum ICSectionType : Int {
    case ICBillDetails = 0
 //   case ICsubCategories = 1
    case ICAddServices = 1
    case ICGrandTot = 2
    case ICrecepientData = 3
    case ICDocPhotos = 4
    case ICRatingCust = 5
}

enum ICRowType : Int {
    case ICRowDefault = 0
    case ICRowSeperator = 1
}



class InvoiceViewController: UIViewController ,UINavigationControllerDelegate {
    
    @IBOutlet var invoiceTableView: UITableView!
    var noOfServices: Int = 0
    static var myImages = [UIImage]()
    var SelectedIndex:Int = 0
    let amazonWrapper:AmazonWrapper = AmazonWrapper.sharedInstance()
    var documentImages = String()
    var ratingBycust:Float = 0
    
    var disc = String()
    var distFare = String()
    var timeFare = String()
    var grandTotal = String()
    var waitFee = String()
    
    var receiverNumber = String()
    var nameReceiver = String()
     var tollFee = String()
    let location = LocationManager.sharedInstance
    
    @IBOutlet var bookingID: UILabel!
    var bookingDict = [Home]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        invoiceTableView.estimatedRowHeight = 10
        invoiceTableView.rowHeight = UITableViewAutomaticDimension
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0x23C590)
        ratingBycust = 4.0
        bookingID.text = "BID:" + bookingDict[SelectedIndex].bookingId
        amazonWrapper.delegate = self
        let ud = UserDefaults.standard
        ud.set(false, forKey:"signatureMade")
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitTheInvoice(_ sender: Any) {
        self.updateTheJobCompletionStatus()
 
    }
    
    @IBAction func gestureAction(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func addServices(_ sender: Any) {
        noOfServices = noOfServices + 1;
        invoiceTableView.reloadData()
    }
    
    @IBAction func needHelp(_ sender: Any) {
        
    }
    
    @IBAction func backToRootVC(_ sender: Any) {
         _ = navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func takeDocPhotos(_ sender: Any) {
        selectImage()
    }
    
    
    func updateTheJobCompletionStatus(){
        let ud = UserDefaults.standard
        if ud.bool(forKey: "signatureMade") {
            self.uploadSignatureimgToAmazon()
            self.uploadDocPhotosimgToAmazon()
            self.updateBookingStatus(status: 10)
        }else{
            self.present(Helper.alertVC(title: signup.Message! as NSString, message:"Please take the signature from customer"), animated: true, completion: nil)
        }
    }
    
    
    func uploadSignatureimgToAmazon(){
        
        let indexPath = IndexPath(row: 0, section: ICSectionType.ICrecepientData.rawValue)
        if let cell: ICReceiverTableViewCell = invoiceTableView.cellForRow(at: indexPath) as? ICReceiverTableViewCell {
            var url = String()
            url = AMAZONUPLOAD.SIGNATURE + bookingDict[SelectedIndex].bookingId + ".png"
            let image = self.image(from: cell.signatureView)
            amazonWrapper.uploadImageToAmazon(withImage: image, imgPath: url)
        }
    }
    
    func uploadDocPhotosimgToAmazon(){
        var imagesCount:Int = 0
        for images in InvoiceViewController.myImages {
            imagesCount =  imagesCount + 1
            var url = String()
            url = AMAZONUPLOAD.DOCUMENTS + bookingDict[SelectedIndex].bookingId + "_" + String(imagesCount) + ".png"
            amazonWrapper.uploadImageToAmazon(withImage: images, imgPath: url)
        }
    }

    
    
    /*****************************************************************/
    //MARK: - UIImage Picker
    /*****************************************************************/
    
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: "Please Select Image",
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel",
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery",
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera",
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerControllerSourceType.camera;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    func removeService(_ sender: UIButton) {
    //    let selectedIndex: Int = sender.tag % 1000
        noOfServices = noOfServices - 1;
        invoiceTableView.reloadData()
    }
    
    func updateBookingStatus(status:Any) {
        var strUrl = String()
        
        var imagesCount:Int = 0
        
        for _ in InvoiceViewController.myImages {
            imagesCount =  imagesCount + 1
            strUrl = Utility.amazonUrl + AMAZONUPLOAD.DOCUMENTS + bookingDict[SelectedIndex].bookingId + "_" + String(imagesCount) + ".png"
            if self.documentImages.isEmpty{
                self.documentImages = strUrl
            }else{
                self.documentImages = self.documentImages + "," + strUrl
            }
        }
        let signatureURL = Utility.amazonUrl + AMAZONUPLOAD.SIGNATURE + bookingDict[SelectedIndex].bookingId + ".png"
        let ud = UserDefaults.standard
        
        var _: Error?
        
        var  jsonData1   = Data()
        var  jsonData2   = Data()
        var  arrivedData = String()
        var  pathData    = String()
        
        
        if ((ud.object(forKey: "beforeReach")) != nil) {
            jsonData1 = try! JSONSerialization.data(withJSONObject:(ud.object(forKey: "beforeReach")) as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
            arrivedData = String(data: jsonData1, encoding: String.Encoding.utf8)!
            
        }else{
            arrivedData = ""
        }
        
        if ((ud.object(forKey: "pathArrived")) != nil) {
            jsonData2 = try! JSONSerialization.data(withJSONObject:(ud.object(forKey: "pathArrived")) as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
            pathData = String(data: jsonData2, encoding: String.Encoding.utf8)!
        }else{
            pathData = ""
        }
        
        let indexPath = IndexPath(row: 0, section: ICSectionType.ICrecepientData.rawValue)
        let cell: ICReceiverTableViewCell = invoiceTableView.cellForRow(at: indexPath) as! ICReceiverTableViewCell

        
        if tollFee.isEmpty {
             tollFee = "0"
        }
        
        Helper.showPI(message:"Loading..")
        let params : [String : Any] =  ["token"              : Utility.sessionToken,
                                        "ent_status"         : status,
                                        "ent_booking_id"     :bookingDict[SelectedIndex].bookingId as Any,
                                        "ent_signatureUrl"   : signatureURL,
                                        "ent_documents"      :documentImages,
                                        "ent_tollFee"        :tollFee,
                                        "ent_receiverName"   :cell.receiverName.text!,
                                        "ent_receiverPhone"  :cell.phoneNumber.text!,
                                        "ent_rating"         :ratingBycust,
                                        "travel_path_arrived":arrivedData,
                                        "travel_path_Drop"   :pathData]
        
        
        NetworkHelper.requestPUT(serviceName:API.METHOD.UPDATEBOOKINGSTATUS,
                                 params: params,
                                 success: { (response : [String : Any]) in
                                    print("UpdateDriverStatus response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        let flag:Int = response["errFlag"] as! Int
                                        
                                        if flag == 0{
                                            self.publishCompleteStatus()
                                            ud.removeObject(forKey: "pathArrived")
                                            ud.removeObject(forKey: "beforeReach")
                                            ud.removeObject(forKey:"DriverArrived")
                                            ud.synchronize()
                                            _ = self.navigationController?.popToRootViewController(animated: true)
                                        }else{
                                            Helper.hidePI()
                                            self.present(Helper.alertVC(title: "Message", message:response["errMsg"] as! NSString), animated: true, completion: nil)
                                        }
                                        
                                    } else {
                                        
                                    }
        }) { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    func image(from view: SignatureView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    func publishCompleteStatus(){
        let ud = UserDefaults.standard
        ud.set(String(describing:bookingDict[SelectedIndex].bookingId!) + "|" + bookingDict[SelectedIndex].custChn + "|" + String(describing:10), forKey: USER_INFO.SELBID)
        ud.synchronize()
        self.location.locationUpdatesToCustomerChannel(val: "")
    }


//    func image(from view: UIView) -> UIImage {
//        UIGraphicsBeginImageContext(view.frame.size)
//        view.layer.render(in: UIGraphicsGetCurrentContext()!)
//        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        return image!
//    }

}

extension InvoiceViewController : UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        InvoiceViewController.myImages.append(image)
        invoiceTableView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension InvoiceViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType : ICSectionType = ICSectionType(rawValue: section)!
        
        switch sectionType {
            
        case .ICBillDetails:
            return 2
//            
//        case .ICAddServices :
//            return noOfServices
//            
        case .ICAddServices :
            return 1
            
        case .ICGrandTot :
            return 1
            
        case .ICrecepientData :
            return 2
            
        case .ICDocPhotos :
            return 2
            
        case .ICRatingCust :
            return 1
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType : ICSectionType = ICSectionType(rawValue: indexPath.section)!
        let sectionCell : ICDividerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "divider") as! ICDividerTableViewCell
        
        switch sectionType {
        case .ICBillDetails:
            let rowType : ICRowType = ICRowType(rawValue: indexPath.row)!
            switch rowType {
            case .ICRowDefault:
                sectionCell.selectionStyle = .none
                return sectionCell
            default:
                let cell: ICBillDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "billDetails") as! ICBillDetailsTableViewCell
                
                cell.discount.text = Utility.currencySymbol + self.disc
                cell.distanceFare.text = Utility.currencySymbol + self.distFare
                cell.timeFare.text = Utility.currencySymbol + self.timeFare
                cell.waitingCharge.text = Utility.currencySymbol + self.waitFee
                
                let a:Float = Float(self.disc)!
                let b:Float = Float(self.distFare)!
                let c:Float = Float(self.timeFare)!
                let d:Float = Float(self.waitFee)!
                let subtot:Float = a + b + c + d
                cell.subTotal.text = Utility.currencySymbol + String(describing:subtot)
                cell.selectionStyle = .none
                return cell
            }
            
        case .ICAddServices:
            
            let cell :ICServicesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "services") as! ICServicesTableViewCell
            if tollFee.isEmpty ||  tollFee == "$0.00" {
                cell.serviceAmount.placeholder = Utility.currencySymbol + "0.00"
            }else{
                cell.serviceAmount.text = Utility.currencySymbol + tollFee
            }
            
            cell.selectionStyle = .none
            return cell

        case .ICGrandTot:
            
            let cell :ICGrandTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "grandTotal") as! ICGrandTotalTableViewCell
            if tollFee.isEmpty ||  tollFee == "$0.00"  {
                cell.grandTotal.text = Utility.currencySymbol + grandTotal
            }else{
                let a:Float = Float(grandTotal)!
                let b:Float = Float(tollFee)!
                let tot:Float = a + b
                cell.grandTotal.text = Utility.currencySymbol + String(describing:tot)
            }
            
            cell.selectionStyle = .none
            return cell
            
        case .ICrecepientData:
            let rowType : ICRowType = ICRowType(rawValue: indexPath.row)!
            switch rowType {
            case .ICRowSeperator:
                sectionCell.selectionStyle = .none
                return sectionCell
                
            default:
                let cell: ICReceiverTableViewCell = tableView.dequeueReusableCell(withIdentifier: "receiverDetails") as! ICReceiverTableViewCell
                if receiverNumber.isEmpty {
                     cell.phoneNumber.text = bookingDict[SelectedIndex].dropPhone
                }else
                {
                     cell.phoneNumber.text = receiverNumber
                }
                if nameReceiver.isEmpty {
                    cell.receiverName.text = bookingDict[SelectedIndex].dropCustName
                }else
                {
                    cell.receiverName.text = nameReceiver
                }
               
                cell.selectionStyle = .none
                return cell
            }
            
        case .ICDocPhotos:
            let rowType : ICRowType = ICRowType(rawValue: indexPath.row)!
            switch rowType {
            case .ICRowSeperator:
                sectionCell.selectionStyle = .none
                return sectionCell
            default:
                let cell: ICAddPhotosTableViewCell = tableView.dequeueReusableCell(withIdentifier: "addPhotos") as! ICAddPhotosTableViewCell
                cell.reloadCollectionViewData(arrayOFImages:InvoiceViewController.myImages)
                cell.selectionStyle = .none
                return cell
            }
            
        case .ICRatingCust:
            
            let cell :ICRatingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "rating") as! ICRatingTableViewCell
            cell.selectionStyle = .none
            cell.ratingView.delegate = self
            cell.ratingView.fullImage = UIImage.init(named: "invoice_green_star_icon")
            cell.ratingView.emptyImage = UIImage.init(named: "invoice_grey_star_icon")
            cell.ratingView.maxRating = 5
            cell.ratingView.minRating = 1
            cell.ratingView.rating = 4
            cell.ratingView.editable = true
            cell.ratingView.halfRatings = true
            cell.ratingView.floatRatings = true
            return cell
        }
    }
}


extension InvoiceViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case 11:
            textField.text = ""
            break
        default: break
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
    }
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {  //delegate method
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {  //delegate method
        switch textField.tag {
        case 11:
           // textField.text = ""
            if !(textField.text?.isEmpty)!{
                tollFee = textField.text!
            }
            break
        case 22:
            nameReceiver = textField.text!
            break
        case 33:
            receiverNumber =  textField.text!
            break
        default:
            self.view.endEditing(true)
        }
        self.invoiceTableView.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        switch textField.tag {
        case 11:
            textField.text = ""
            if !(textField.text?.isEmpty)!{
                tollFee =  textField.text!
            }
            break
        case 22:
            nameReceiver = textField.text!
            break
        case 33:
            receiverNumber = textField.text!
            break
        default:
            self.view.endEditing(true)
        }
        self.view.endEditing(true)
        self.invoiceTableView.reloadData()
        return true
    }
}



extension InvoiceViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension InvoiceViewController: FloatRatingViewDelegate{
    /**
     Returns the rating value when touch events end
     */
    public func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        ratingBycust = rating
    }
}

extension InvoiceViewController: AmazonWrapperDelegate{
    func didImageUploadedSuccessfully(withDetails imageURL: String) {
        
    }
    
    func didImageFailtoUpload(_ error: Error?) {
        
    }
}


extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
    }
}
