//
//  ICDocPhotosCollectionViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 25/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ICDocPhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var removeDocImage: UIButton!
    @IBOutlet var docImages: UIImageView!
}
