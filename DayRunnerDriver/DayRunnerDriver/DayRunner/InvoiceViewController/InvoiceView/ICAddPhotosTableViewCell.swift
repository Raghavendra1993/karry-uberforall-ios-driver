//
//  ICAddPhotosTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ICAddPhotosTableViewCell: UITableViewCell {

    @IBOutlet var addPhotosLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    var imagesOfArray = [UIImage]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func reloadCollectionViewData(arrayOFImages: Array<Any>) {
        imagesOfArray = arrayOFImages as! [UIImage]
        collectionView.reloadData()
    }
    
    func removeDocPhotos(_ sender: UIButton) {
        let selectedIndex: Int = sender.tag % 1000
        imagesOfArray.remove(at: selectedIndex)
         collectionView.reloadData()
        InvoiceViewController.myImages = imagesOfArray
    }
}

extension ICAddPhotosTableViewCell:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "docPhotos", for: indexPath as IndexPath) as! ICDocPhotosCollectionViewCell
        cell.docImages.image = imagesOfArray[indexPath.row]
        cell.removeDocImage.tag = indexPath.row + 1000
        cell.removeDocImage.addTarget(self, action: #selector(self.removeDocPhotos), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if imagesOfArray.count > 0
        {
            addPhotosLabel.isHidden = true
            return imagesOfArray.count
        }else{
            addPhotosLabel.isHidden = false
            return 0
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}




