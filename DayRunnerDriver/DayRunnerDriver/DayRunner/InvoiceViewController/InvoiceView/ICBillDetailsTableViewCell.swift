//
//  ICBillDetailsTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ICBillDetailsTableViewCell: UITableViewCell {
    @IBOutlet var distanceFare: UILabel!
    @IBOutlet var timeFare: UILabel!
    @IBOutlet var subTotal: UILabel!

    @IBOutlet var waitingCharge: UILabel!
    @IBOutlet var discount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
