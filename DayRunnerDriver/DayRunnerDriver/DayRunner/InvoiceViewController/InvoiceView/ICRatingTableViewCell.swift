//
//  ICRatingTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import FloatRatingView

class ICRatingTableViewCell: UITableViewCell {

    @IBOutlet var ratingView: FloatRatingView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
