//
//  ShipmentCollectionViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 02/06/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class ShipmentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var acitivityIndicator: UIActivityIndicatorView!
    @IBOutlet var image: UIImageView!
}
