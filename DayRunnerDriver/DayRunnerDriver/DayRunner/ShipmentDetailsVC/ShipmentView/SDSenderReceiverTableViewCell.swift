//
//  SDSenderReceiverTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SDSenderReceiverTableViewCell: UITableViewCell {

    @IBOutlet var custPhone: UILabel!
    @IBOutlet var custName: UILabel!
    
    @IBOutlet var receiverName: UILabel!
    @IBOutlet var receiverPhone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
