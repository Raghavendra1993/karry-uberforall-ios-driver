//
//  SDTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class SDTableViewCell: UITableViewCell {

    @IBOutlet var noPhotosLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var notes: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var goodType: UILabel!
    var jobPhoto = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updatedTheJobImages(photo:[String]){
        if photo.count == 0 {
            noPhotosLabel.isHidden = false
        }else{
           noPhotosLabel.isHidden = true
        }
        jobPhoto = photo
        collectionView.reloadData()
    }
}


// MARK: - CollectionView datasource method
extension SDTableViewCell:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "shipments", for: indexPath as IndexPath) as! ShipmentCollectionViewCell
        cell.acitivityIndicator.startAnimating()
        let stringUrl = jobPhoto[indexPath.row]
        
        cell.image.kf.setImage(with: URL(string: stringUrl),
                               placeholder:#imageLiteral(resourceName: "history_gallery_icon"),
                               options: [.transition(ImageTransition.fade(1))],
                               progressBlock: { receivedSize, totalSize in
        },
                               completionHandler: { image, error, cacheType, imageURL in
                                cell.acitivityIndicator.stopAnimating()
        })
        
            cell.acitivityIndicator.stopAnimating()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if jobPhoto.count == 0{
            return 0
        }else{
            return jobPhoto.count
        }
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}


// MARK: - CollectionView delegate method
extension SDTableViewCell:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
    }
}

