//
//  SDAddressTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 24/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SDAddressTableViewCell: UITableViewCell {

    @IBOutlet var pickTime: UILabel!
    @IBOutlet var dropTime: UILabel!
    @IBOutlet var dropAddress: UILabel!
    @IBOutlet var pickAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
