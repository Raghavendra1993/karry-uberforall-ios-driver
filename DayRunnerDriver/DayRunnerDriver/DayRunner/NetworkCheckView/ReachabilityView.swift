//
//  ReachabilityShowView.swift

//
//  Created by Rahul Sharma on 06/10/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class ReachabilityView: UIView {
    
    private static var share: ReachabilityView? = nil
    var isShown:Bool = false
    
    static var instance: ReachabilityView {

        if (share == nil) {
            share = Bundle(for: self).loadNibNamed("ReachabilityView",
                                                   owner: nil,
                                                   options: nil)?.first as? ReachabilityView
        }
        return share!
    }
    

    /// Show Network
    func show() {
        if isShown == false {
            isShown = true
            let window = UIApplication.shared.keyWindow
            self.frame = (window?.frame)!
            window?.addSubview(self)
            
            self.alpha = 0.0
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.alpha = 1.0
            })
        }
    }
    
    /// Hide
    func hide() {
        if isShown == true {
            isShown = false
            
            UIView.animate(withDuration: 0.5,
                           animations: {
                            ReachabilityView.share = nil
                            self.alpha = 0.0
            },
                           completion: { (completion) in
                            self.removeFromSuperview()
            })
        }
    }
}
