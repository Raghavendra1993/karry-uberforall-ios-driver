//
//  VehicleTypeModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 08/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit

class VehType {
    
    let VehID :String!
    let VehName:String!
    
    init(vehicleID:String,vehicleName:String) {
        self.VehID = vehicleID
        self.VehName = vehicleName
    }
}

class VehSpecial {
    
    let VehSpecialID :String!
    let VehSpecialName:String!
    
    init(vehicleSPID:String,vehicleSPName:String) {
        self.VehSpecialID = vehicleSPID
        self.VehSpecialName = vehicleSPName
    }
}

