//
//  VehicleMakeModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 08/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//


import UIKit

class Make {
    
    let makeID :String!
    let makeName:String!
    
    init(vehicleMakeID:String,vehicleMakeName:String) {
        self.makeID = vehicleMakeID
        self.makeName = vehicleMakeName
    }
}


class Model {
    
    let modelID :String!
    let modelName:String!
    
    init(vehicleModelID:String,vehicleModelName:String) {
        self.modelID = vehicleModelID
        self.modelName = vehicleModelName
    }
}


