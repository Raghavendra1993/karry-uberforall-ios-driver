//
//  VehicleTypeModelMakeVC.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 26/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit


protocol vehicleDetailsDelegate:class
{
    func didSelectMakeModelType(vehicleData: VehicleType)
}

class VehicleType {
    var vehicleTypeMakeModel = String()
    var idSelected = String()
    var type:Int = 0
    
    var seletedIDs = [String]()
    var selIndex:Int = 0
    var selectedIndexes = [Int]()
    
    
    init(vehicletyp: String, typeof:Int, selectedID:String,selectedIdsArray:[String],index:Int,selectedIndex:[Int]) {
        self.vehicleTypeMakeModel = vehicletyp
        self.type = typeof
        self.idSelected = selectedID
        self.seletedIDs =  selectedIdsArray
        self.selIndex = index
        self.selectedIndexes = selectedIndex
    }
}


class VehicleTypeModelMakeVC: UIViewController {
    
    open weak var delegate: vehicleDetailsDelegate?
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var vehicleModelTV: UITableView!
    var vechArray: [[String: AnyObject]] = []
    @IBOutlet var doneButton: UIButton!
    var vehicle: VehicleType? = nil
    
    var idMakeSeleted =  String()
    
    var operatorList    = [Operator]()
    var make            = [Make]()
    var model           = [Model]()
    var vehType         = [VehType]()
    var vehSpecialities = [VehSpecial]()
    
    var alreadySelectedIndex:Int = -1
    var alreadySelectedIndexes = [Int]()
    
    var type:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vehicleModelTV.allowsMultipleSelectionDuringEditing = true
        vehicleModelTV.tintColor = Helper .UIColorFromRGB(rgbValue: 0x17CA9C)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        switch type {
        case 1:
            titleLabel.text = "Vehicle Types"
            break;
        case 2:
            titleLabel.text = "Vehicle Make"
            break;
        case 3:
            titleLabel.text = "Vehicle Model"
            break;
        case 4:
            titleLabel.text = "Specialities"
            break;
        default:
            titleLabel.text = "Zones"
            break;
        }
        vehicleModelTV.setEditing(!vehicleModelTV.isEditing, animated: true)
        getTypeMakeNModel()
    }
    
    func getTypeMakeNModel(){
        var serviceName = String()
        
        switch type {
        case 1:
            serviceName = API.METHOD.vehicleTypes
            break;
        case 4:
            serviceName = API.METHOD.vehicleTypes
            break;
        case 2:
            serviceName = API.METHOD.makeModel
            break;
        case 3:
            serviceName = API.METHOD.makeModel
            break;
        default:
            serviceName = API.METHOD.getZones
            break;
        }
        
        Helper.showPI(message:"Loading..")
        let params : [String : Any] =  ["": ""] //Get APi
        
        print("params :",params)
        
        NetworkHelper.requestGETURL(method: serviceName,
                                    success: { (response) in
                                        Helper.hidePI()
                                        if response.isEmpty == false {
                                            
                                            self.parsingTheServiceResponse(responseData: (response["data"] as? [[String: Any]])!)
                                            
                                        } else {
                                            
                                        }
                                        
        })
        { (Error) in
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    func parsingTheServiceResponse(responseData:[[String: Any]]){
        
        switch type {
        case 1:
            for items in responseData{
                
                let id = items["id"] as? String
                let company = items["Name"] as? String
                
                vehType.append(VehType.init(vehicleID: String(describing: id!), vehicleName: company!))
            }
            
            break;
        case 2:
            for items in responseData{
                
                let id = items["id"] as? String
                let MakeName = items["Name"] as? String
                
                make.append(Make.init(vehicleMakeID:id!,vehicleMakeName:MakeName!))
            }
            
            break;
        case 3:
            
            for items in responseData{
                let id = items["id"] as? String
                
                let models = items["models"] as? [[String: Any]]
                
                if id! == idMakeSeleted{
                    for data in models! {
                        
                        let modID = data["id"] as? String
                        let modName = data["Name"] as? String
                        
                        model.append(Model.init(vehicleModelID:modID!,vehicleModelName:modName!))
                    }
                }
            }
            
            break;
        case 4:
            
            for items in responseData{
                
                let id = items["id"] as? String
                
                let specialites = items["sepecialities"] as? [[String: Any]]
                
                if id! == idMakeSeleted {
                    for data in specialites! {
                        
                        let spID = data["id"] as? String
                        let spName = data["Name"] as? String
                        vehSpecialities.append(VehSpecial.init(vehicleSPID:spID!,vehicleSPName:spName!))
                    }
                }
            }
            
            break;
        default:
            
            for items in responseData{
                
                let id = items["id"] as? String
                let company = items["Name"] as? String
                
                operatorList.append(Operator.init(operatorID:id! , operatorCompany: company!))
            }
            break;
        }
        vehicleModelTV.reloadData()
        self.updateTheTableView()
        
    }
    
    func updateTheTableView(){
        switch type {
        case 1:
            if alreadySelectedIndex != -1 {
                let indexPath = IndexPath(row: alreadySelectedIndex, section: 0)
                self.vehicleModelTV.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.middle)
                   doneButton.isSelected = true
            }
            break;
        case 4:
            for index in alreadySelectedIndexes {
                let indexPath = IndexPath(row: index, section: 0)
                self.vehicleModelTV.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.middle)
                   doneButton.isSelected = true
            }
            break;
        case 2:
            if alreadySelectedIndex != -1 {
                let indexPath = IndexPath(row: alreadySelectedIndex, section: 0)
                self.vehicleModelTV.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.middle)
                   doneButton.isSelected = true
            }
            break;
        case 3:
            if alreadySelectedIndex != -1 {
                let indexPath = IndexPath(row: alreadySelectedIndex, section: 0)
                self.vehicleModelTV.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.middle)
                   doneButton.isSelected = true
            }
            break;
        default:
            for index in alreadySelectedIndexes {
                let indexPath = IndexPath(row: index, section: 0)
                self.vehicleModelTV.selectRow(at: indexPath, animated: true, scrollPosition: UITableViewScrollPosition.middle)
                   doneButton.isSelected = true
            }
            break;
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectedMakeModelType(_ sender: Any) {
        if doneButton.isSelected {
            var specialitiesIDs = [String]()
            var specialitiesSelected  = String()
            var collectionOfIndex = [Int]()
            switch type {
            case 4:
                let selectedRows: [Any]? = vehicleModelTV.indexPathsForSelectedRows
                
                for index in selectedRows! {
                    var indexpath:IndexPath = index as! IndexPath
                    
                    collectionOfIndex.append(indexpath.row)//selected cells
                    let spcIds = vehSpecialities[indexpath.row].VehSpecialID
                    specialitiesIDs.append(spcIds!)
                    
                    if specialitiesSelected.isEmpty {
                        specialitiesSelected = vehSpecialities[indexpath.row].VehSpecialName
                    }else{
                        specialitiesSelected = specialitiesSelected + "," + vehSpecialities[indexpath.row].VehSpecialName
                    }
                }
                
                vehicle = VehicleType.init(vehicletyp: specialitiesSelected, typeof:type, selectedID: "specialities", selectedIdsArray: specialitiesIDs,index:0,selectedIndex:collectionOfIndex)
                
                delegate?.didSelectMakeModelType(vehicleData: vehicle!)
                
                _ = navigationController?.popViewController(animated: true)
                break
                
            case 5:
                
                let selectedRows: [Any]? = self.vehicleModelTV.indexPathsForSelectedRows
                
                for index in selectedRows! {
                    var indexpath:IndexPath = index as! IndexPath
                    collectionOfIndex.append(indexpath.row) //selected cells
                    let spcIds = self.operatorList[indexpath.row].iD
                    specialitiesIDs.append(spcIds!)
                    if specialitiesSelected.isEmpty {
                        specialitiesSelected = self.operatorList[indexpath.row].company
                    }else{
                        specialitiesSelected = specialitiesSelected + "," + self.operatorList[indexpath.row].company
                    }
                }
                self.vehicle = VehicleType.init(vehicletyp: specialitiesSelected, typeof:self.type, selectedID: "zones", selectedIdsArray: specialitiesIDs,index:0,selectedIndex:collectionOfIndex)
                
                self.delegate?.didSelectMakeModelType(vehicleData: self.vehicle!)
                
                _ = self.navigationController?.popViewController(animated: true)
                break
            default:
                
                delegate?.didSelectMakeModelType(vehicleData: vehicle!)
                _ = navigationController?.popViewController(animated: true)
            }
            
        }else{
            switch type {
            case 1:
                self.present(Helper.alertVC(title: "Message", message:"Please Select the Type"), animated: true, completion: nil)
                break;
                
            case 2:
                self.present(Helper.alertVC(title: "Message", message:"Please select the Make"), animated: true, completion: nil)
                break;
                
            case 3:
                self.present(Helper.alertVC(title: "Message", message:"Please select the Model"), animated: true, completion: nil)
                break;
            case 4:
                self.present(Helper.alertVC(title: "Message", message:"Please select the Specialities"), animated: true, completion: nil)
                break;
            default:
                self.present(Helper.alertVC(title: "Message", message:"Please select the Zones"), animated: true, completion: nil)
                break;
            }
        }
    }
    
    @IBAction func backToVC(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}

//MARK:- tableview datasource
extension VehicleTypeModelMakeVC: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch type {
        case 1:
            return vehType.count;
        case 2:
            return make.count;
        case 3:
            return model.count;
        case 4:
            return vehSpecialities.count;
        default:
            return operatorList.count;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"typeModelMake") as! VTMMTableViewCell!
        
        switch type {
        case 1:
            cell?.textLabelForVehicle.text = vehType[indexPath.row].VehName!
            break
        case 2:
            cell?.textLabelForVehicle.text = make[indexPath.row].makeName!
            break
        case 3:
            cell?.textLabelForVehicle.text = model[indexPath.row].modelName!
            break
        case 4:
            cell?.textLabelForVehicle.text = vehSpecialities[indexPath.row].VehSpecialName!
            break
        default:
            cell?.textLabelForVehicle.text = operatorList[indexPath.row].company!
            break
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 50;
    }
}

//MARK:- Tableview delegate methods
extension VehicleTypeModelMakeVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        vehicle = nil
        if type != 4 && type != 5{
            for selectedIndexPath: IndexPath in tableView.indexPathsForSelectedRows! {
                if selectedIndexPath.row != indexPath.row {
                    tableView.deselectRow(at: selectedIndexPath, animated: false)
                }
            }
        }
        
        switch type {
        case 1:
            vehicle = VehicleType.init(vehicletyp: vehType[indexPath.row].VehName, typeof:type, selectedID: vehType[indexPath.row].VehID, selectedIdsArray: [],index:indexPath.row,selectedIndex:[])
            
            
            break
        case 2:
            vehicle = VehicleType.init(vehicletyp: make[indexPath.row].makeName, typeof:type, selectedID: make[indexPath.row].makeID, selectedIdsArray: [],index:indexPath.row,selectedIndex:[])
            
            
            break
        case 3:
            vehicle = VehicleType.init(vehicletyp: model[indexPath.row].modelName, typeof:type, selectedID: model[indexPath.row].modelID, selectedIdsArray: [],index:indexPath.row,selectedIndex:[])
            
            
            break
        default:
            break
        }
        doneButton.isSelected = true
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let selectedRows: [Any]? = vehicleModelTV.indexPathsForSelectedRows
        if type == 4 || type == 5{
            if selectedRows?.count == nil {
                doneButton.isSelected = false
            }
        }else{
            doneButton.isSelected = false
        }
    }
}



