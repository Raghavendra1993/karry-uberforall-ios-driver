//
//  MainModel.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 18/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class Home {
    
    let pickAddress :String!
    let dropAddress:String!
    let bookingDateNtime:String!
    let timeLeftToPick:String!
    let bookingAmt:String!
    let bookingId:String!
    let statCode:Int!
    let pickPhone:String!
    let dropPhone:String!
    let dropCustName:String!
    let pickCustName:String!
    let pickLatlog:String!
    let dropLatlog:String!
     let goodType:String!
     let quantity:String!
     let notes:String!
    var shipImage:[String]
    let custChn:String!
    
    init(pickAdd:String,dropAdd:String,bookTime:String,timeLeft:String,bookingFee:String,bid:String,statCode:Int,pkPhone:String,dpPhone:String,dCName:String,pCName:String,pLatLog:String,dLatLog:String,good:String,quant:String,exNotes:String,shipmentImage:[String],channel:String) {
        self.pickAddress       = pickAdd
        self.dropAddress       = dropAdd
        self.bookingDateNtime  = bookTime
        self.timeLeftToPick    = timeLeft
        self.bookingAmt        = bookingFee
        self.bookingId         = bid
        self.statCode = statCode
        self.pickPhone = pkPhone
        self.dropPhone = dpPhone
        self.dropCustName = dCName
        self.pickCustName = pCName
        self.pickLatlog = pLatLog
        self.dropLatlog = dLatLog
        self.goodType = good
        self.quantity = quant
        self.notes = exNotes
        self.shipImage = shipmentImage
        self.custChn = channel
    }
}

class HomeDetails {
    
    let pickAddress :String!
    let dropAddress:String!
    let bookingDateNtime:String!
    let timeLeftToPick:String!
    let bookingAmt:String!
    let bookingId:String!
    let statCode:Int!
    let pickPhone:String!
    let dropPhone:String!
    let dropCustName:String!
    let pickCustName:String!
    let pickLatlog:String!
    let dropLatlog:String!
    let signature:String!
    
    init(pickAdd:String,dropAdd:String,bookTime:String,timeLeft:String,bookingFee:String,bid:String,statCode:Int,pkPhone:String,dpPhone:String,dCName:String,pCName:String,pLatLog:String,dLatLog:String,sign:String) {
        self.pickAddress = pickAdd
        self.dropAddress = dropAdd
        self.bookingDateNtime = bookTime
        self.timeLeftToPick = timeLeft
        self.bookingAmt = bookingFee
        self.bookingId = bid
        self.statCode = statCode
        self.pickPhone = pkPhone
        self.dropPhone = dpPhone
        self.dropCustName = dCName
        self.pickCustName = pCName
        self.pickLatlog = pLatLog
        self.dropLatlog = dLatLog
         self.signature = sign
    }
}
