//
//  LeftSecondTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 14/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class LeftSecondTableViewCell: UITableViewCell {

    @IBOutlet var leftMenuImage: UIImageView!
    @IBOutlet var leftMenuName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
