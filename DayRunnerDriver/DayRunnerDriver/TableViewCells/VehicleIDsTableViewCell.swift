//
//  VehicleIDsTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 15/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class VehicleIDsTableViewCell: UITableViewCell {
    
    @IBOutlet var backGroundView: UIView!
    @IBOutlet var topView: UIView!
    @IBOutlet var selectedButton: UIButton!
    @IBOutlet var vehicleModel: UILabel!
    @IBOutlet var vehicleType: UILabel!
    @IBOutlet var plateNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
              Helper.shadowView(sender: topView, width:UIScreen.main.bounds.size.width-37, height:topView.frame.size.height)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
