//
//  HistoryTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 17/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet var bookingTime: UILabel!
    @IBOutlet var dropAddress: UILabel!
    @IBOutlet var pickAddress: UILabel!
    @IBOutlet var amount: UILabel!
    @IBOutlet var bid: UILabel!
    @IBOutlet var topView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
           Helper.shadowView(sender: topView, width:UIScreen.main.bounds.size.width-40, height:topView.frame.size.height)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
