//
//  UpcomingTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 14/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class UpcomingTableViewCell: UITableViewCell {

    @IBOutlet var topView: UIView!
    @IBOutlet var bookingDateNTime: UILabel!
    @IBOutlet var dropAddress: UILabel!
    @IBOutlet var pickUpAddress: UILabel!
    @IBOutlet var timeLeftToPickBooking: UILabel!
    @IBOutlet var bookingAmt: UILabel!
    @IBOutlet var bookingID: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
      Helper.shadowView(sender: topView, width:UIScreen.main.bounds.size.width-30, height:topView.frame.size.height)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
