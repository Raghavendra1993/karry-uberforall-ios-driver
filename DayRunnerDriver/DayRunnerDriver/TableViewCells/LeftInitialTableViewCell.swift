//
//  LeftInitialTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 14/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class LeftInitialTableViewCell: UITableViewCell {
    @IBOutlet var profileImage: UIImageView!

    @IBOutlet var driverName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
