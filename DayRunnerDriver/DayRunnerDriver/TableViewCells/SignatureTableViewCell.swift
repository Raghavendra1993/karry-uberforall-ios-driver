//
//  SignatureTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 17/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SignatureTableViewCell: UITableViewCell {

    @IBOutlet var amountField: UITextField!
    @IBOutlet var chargeField: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        amountField.layer.borderWidth = 1;
        amountField.layer.borderColor = Helper.UIColorFromRGB(rgbValue: 0xb2b2b2).cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
