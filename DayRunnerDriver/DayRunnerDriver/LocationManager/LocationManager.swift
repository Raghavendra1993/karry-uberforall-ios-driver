//
//  LocationManager.swift
//
//
//  Created by Jimmy Jose on 14/08/14.
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


import UIKit
import CoreLocation
import MapKit




// Todo: Keep completion handler differerent for all services, otherwise only one will work
enum GeoCodingType{
    
    case geocoding
    case reverseGeocoding
}

class LocationManager: NSObject,CLLocationManagerDelegate {
    
    /* Private variables */
    var bookingInfo = [Any]()
    var timer       = Timer()
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    fileprivate var locationStatus : NSString = "Calibrating"// to pass in handler
    fileprivate var locationManager: CLLocationManager!
    fileprivate var verboseMessage = "Calibrating"
    
    fileprivate let verboseMessageDictionary = [CLAuthorizationStatus.notDetermined:"You have not yet made a choice with regards to this application.",
                                                CLAuthorizationStatus.restricted:"This application is not authorized to use location services. Due to active restrictions on location services, the user cannot change this status, and may not have personally denied authorization.",
                                                CLAuthorizationStatus.denied:"You have explicitly denied authorization for this application, or location services are disabled in Settings.",
                                                CLAuthorizationStatus.authorizedAlways:"App is Authorized to use location services.",
                                                CLAuthorizationStatus.authorizedWhenInUse:"You have granted authorization to use your location only when the app is visible to you."]
    
    
    var delegate:LocationManagerDelegate? = nil
    
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    
    var latitudeAsString:String = ""
    var longitudeAsString:String = ""
    
    
    var lastKnownLatitude:Double = 0.0
    var lastKnownLongitude:Double = 0.0
    
    var lastKnownLatitudeAsString:String = ""
    var lastKnownLongitudeAsString:String = ""
    
    
    var keepLastKnownLocation:Bool = true
    var hasLastKnownLocation:Bool = true
    
    var autoUpdate:Bool = false
    
    var showVerboseMessage = false
    
    var locationdataArray = [Any]()
    var bgTask = BackgroundTaskManager.sharedBackgroundTaskManager()
    
    
    
    class var sharedInstance : LocationManager {
        struct Static {
            static let instance : LocationManager = LocationManager()
        }
        return Static.instance
    }
    
    
    fileprivate override init(){
        
        super.init()
        if(!autoUpdate){
            autoUpdate = !CLLocationManager.significantLocationChangeMonitoringAvailable()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.applicationEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
    }
    
    func applicationEnterBackground(){
        bgTask?.beginNewBackgroundTask()
        self.startUpdatingLocation()
    }
    
    fileprivate func resetLatLon(){
        
        latitude = 0.0
        longitude = 0.0
        
        latitudeAsString = ""
        longitudeAsString = ""
        
    }
    
    fileprivate func resetLastKnownLatLon(){
        
        hasLastKnownLocation = false
        
        lastKnownLatitude = 0.0
        lastKnownLongitude = 0.0
        
        lastKnownLatitudeAsString = ""
        lastKnownLongitudeAsString = ""
        
    }
    
    
    func startUpdatingLocation(){
        
        initLocationManager()
    }
    
    func stopUpdatingLocation(){
        
        //    if(autoUpdate){
        locationManager.stopUpdatingLocation()
        //        }else{
        //
        //            locationManager.stopMonitoringSignificantLocationChanges()
        //        }
        
        
        resetLatLon()
        if(!keepLastKnownLocation){
            resetLastKnownLatLon()
        }
    }
    
    fileprivate func initLocationManager() {
        
        // App might be unreliable if someone changes autoupdate status in between and stops it
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        // locationManager.locationServicesEnabled
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingHeading()
        
        let Device = UIDevice.current
        
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        let iOS8 = iosVersion >= 8
        
        if iOS8{
            locationManager.requestAlwaysAuthorization() // add in plist NSLocationAlwaysUsageDescription
            // locationManager.requestWhenInUseAuthorization() // add in plist NSLocationWhenInUseUsageDescription
            locationManager.allowsBackgroundLocationUpdates = true
        }
        startLocationManger()
    }
    
    fileprivate func startLocationManger(){
        
        //     if(autoUpdate){
        
        locationManager.startUpdatingLocation()
        
        //        }else{
        //
        //            locationManager.startMonitoringSignificantLocationChanges()
        //        }
    }
    
    fileprivate func stopLocationManger(){
        
        //    if(autoUpdate){
        
        locationManager.stopUpdatingLocation()
        //        }else{
        //
        //            locationManager.stopMonitoringSignificantLocationChanges()
        //        }
    }
    
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        stopLocationManger()
        
        resetLatLon()
        
        if(!keepLastKnownLocation){
            
            resetLastKnownLatLon()
        }
        
        if ((delegate != nil) && (delegate?.responds(to: #selector(LocationManagerDelegate.locationManagerReceivedError(_:))))!){
            delegate?.locationManagerReceivedError!(error.localizedDescription as NSString)
        }
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
       // newHeading.trueHeading
        print("new header ",  newHeading.magneticHeading)
    }
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let arrayOfLocation = locations as NSArray
        let location = arrayOfLocation.lastObject as! CLLocation
        let coordLatLon = location.coordinate
        
        latitude  = coordLatLon.latitude
        longitude = coordLatLon.longitude
        
        latitudeAsString  = coordLatLon.latitude.description
        longitudeAsString = coordLatLon.longitude.description
        
        
        lastKnownLatitude = coordLatLon.latitude
        lastKnownLongitude = coordLatLon.longitude
        
        lastKnownLatitudeAsString = coordLatLon.latitude.description
        lastKnownLongitudeAsString = coordLatLon.longitude.description
        
        hasLastKnownLocation = true
        
        let latLongs : [String : Any] =
            ["latitude"   : latitude,
             "longitude" :longitude]
        
        
        locationdataArray.append(latLongs)
        
        if (delegate != nil){
            if((delegate?.responds(to: #selector(LocationManagerDelegate.locationFoundGetAsString(_:longitude:))))!){
                delegate?.locationFoundGetAsString!(latitudeAsString as NSString,longitude:longitudeAsString as NSString)
            }
            if((delegate?.responds(to: #selector(LocationManagerDelegate.locationFound(_:longitude:))))!){
                delegate?.locationFound(latitude,longitude:longitude)
            }
        }
    }
    
    func locationUpdatesToCustomerChannel(val:String) {
        
        if (locationdataArray.count) != 0{
            
            let dict = self.locationdataArray[locationdataArray.count-1] as? [String:Any]
            let lat = dict?["latitude"] as! Double
            let log = dict?["longitude"] as! Double
            self.updateLocationdetailsToPubnub(lat: lat, long: log)
            locationdataArray = [Any]()
            
        }
    }
    
    
    func updateLocationdetailsToPubnub(lat:Double, long: Double)  {
        let ud = UserDefaults.standard
        let _: VNHPubNubWrapper = VNHPubNubWrapper.sharedInstance() as! VNHPubNubWrapper

        
        let message : [String : Any] =  ["lt"            : lat,
                                         "lg"            : long,
                                         "vt"            : Utility.vehicleTypeID,
                                         "token"         :Utility.sessionToken,
                                         "battery_Per"   :Utility.batteryLevel,
                                         "app_version"   :Utility.appVersion,
                                         "device_type"   :1,
                                         "location_check":1,
                                         "pubnubStr" : Utility.onGoingBid,
                                         "location_Heading":"323.232332"
                                         //                                         "bid"           :Utility.onGoingBid,
            //                                         "Bstatus"       :Utility.bookStat,
            //                                         "CustChn"       :Utility.onGoingCustChn
        ]
        
        if ud.integer(forKey: "DriverArrived") > 6{
            bookingInfo = [Any]()
            
            let latlongs: [String: Any] = ["latitude": lat,
                                           "longitude": long]
            if let array = ud.object(forKey: "pathArrived") as? [Any]{
                if (!array.isEmpty){
                    bookingInfo = array
                    ud.removeObject(forKey: "pathArrived")
                    ud.synchronize()
                }
            }
            bookingInfo.append(latlongs)
            ud.set(bookingInfo, forKey: "pathArrived")
            ud.synchronize()
            
        }else if ud.bool(forKey: "noBookings"){
            bookingInfo = [Any]()
            
            let latlongs: [String: Any] = ["latitude": lat,
                                           "longitude": long]
            if let array = ud.object(forKey: "beforeReach") as? [Any]{
                if (!array.isEmpty){
                    bookingInfo = array
                    ud.removeObject(forKey: "beforeReach")
                    ud.synchronize()
                }
            }
            bookingInfo.append(latlongs)
            ud.set(bookingInfo, forKey: "beforeReach")
            ud.synchronize()
        }
        
        //  pub.publish(withParameter: message, toChannel: Utility.serverChannel)
        //  pub.channel(message)
        self.updateLocationToServer(dict:message)
    }
    
    
    func updateLocationToServer(dict:[String : Any]) {
        
        NetworkHelper.requestPUT(serviceName:API.METHOD.LOCATION,
                                 params: dict,
                                 success: { (response : [String : Any]) in
                                    print("UpdateDriverStatus response : ",response)
                                    if response.isEmpty == false {
                                        if (response["statusCode"] != nil){
                                            let statCode:Int = response["statusCode"] as! Int
                                            if statCode == 401 {
                                                Helper.hidePI()
                                                self.sessionExpried()
                                            }
                                        }else{
                                            // self.appDelegate.handlingThePubnubResponse(pubDict: response)
                                        }
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            Helper.hidePI()
        }
    }
    
    func sessionExpried(){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let signIN: SplashViewController? = storyboard.instantiateViewController(withIdentifier: storyBoardIDs.splashVC!) as? SplashViewController
        
        let window = UIApplication.shared.keyWindow
        window?.rootViewController = signIN
        _ = Utility.deleteSavedData()
    }
    
    
    
    internal func locationManager(_ manager: CLLocationManager,
                                  didChangeAuthorization status: CLAuthorizationStatus) {
        var hasAuthorised = false
        let verboseKey = status
        switch status {
        case CLAuthorizationStatus.restricted:
            let view = LocationEnableView.shared
            view.show()
            locationStatus = "Restricted Access"
        case CLAuthorizationStatus.denied:
            let view = LocationEnableView.shared
            view.show()
            
            locationStatus = "Denied access"
        case CLAuthorizationStatus.notDetermined:
            locationStatus = "Not determined"
        default:
            let view = LocationEnableView.shared
            view.hide()
            locationStatus = "Allowed access"
            hasAuthorised = true
        }
        
        verboseMessage = verboseMessageDictionary[verboseKey]!
        
        if (hasAuthorised == true) {
            startLocationManger()
        }else{
            
            resetLatLon()
            if (!locationStatus.isEqual(to: "Denied access")){
                
                var verbose = ""
                if showVerboseMessage {
                    
                    verbose = verboseMessage
                    
                    if ((delegate != nil) && (delegate?.responds(to: #selector(LocationManagerDelegate.locationManagerVerboseMessage(_:))))!){
                        
                        delegate?.locationManagerVerboseMessage!(verbose as NSString)
                        
                    }
                }
            }
            if ((delegate != nil) && (delegate?.responds(to: #selector(LocationManagerDelegate.locationManagerStatus(_:))))!){
                delegate?.locationManagerStatus!(locationStatus)
            }
        }
        
    }
    
}


@objc protocol LocationManagerDelegate : NSObjectProtocol
{
    func locationFound(_ latitude:Double, longitude:Double)
    @objc optional func locationFoundGetAsString(_ latitude:NSString, longitude:NSString)
    @objc optional func locationManagerStatus(_ status:NSString)
    @objc optional func locationManagerReceivedError(_ error:NSString)
    @objc optional func locationManagerVerboseMessage(_ message:NSString)
}


