//
//  NewBookingViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 17/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class NewBookingViewController: UIViewController {
    
    
    @IBOutlet var bookingID: UILabel!
    @IBOutlet var progressText: UILabel!
    @IBOutlet var progressView: CircleProgressView!
    @IBOutlet var deliveryFees: UILabel!
    @IBOutlet var dropTime: UILabel!
    @IBOutlet var dropAddress: UILabel!
    @IBOutlet var pickupTime: UILabel!
    @IBOutlet var pickAddress: UILabel!
    var bookingDict = [String:Any]()
    var window: UIWindow?
    
    var countDown:Int = 30
    var progressVal:Double = 0.0
    var countVal:Float = 0.1
    var proVal:Float = 1.0
    
    var timerProgress = Timer()
    
    let nf = NumberFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
        nf.numberStyle = NumberFormatter.Style.none
        nf.maximumFractionDigits = 2
        self.progressText.text = "30"
        timerProgress =  Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerTick), userInfo: nil, repeats: true)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        pickAddress.text  = bookingDict["adr1"] as? String
        dropAddress.text  = bookingDict["drop1"] as? String
        // deliveryFees.text  = bookingDict["amount"] as? String
        dropTime.text  = bookingDict["dropDt"] as? String
        pickupTime.text  = bookingDict["dt"] as? String
        bookingID.text = String(describing:bookingDict["bid"] as? NSNumber)
    }
    
    func timerTick(){
        progressVal = progressVal + 1.0
        
        let val:Double = 1/30
        
        print(val)
        
        self.progressView.progress = ((1.0) - (val * progressVal))
        
        //   if (Float(Double(1/300)) * Float(progressVal)) == countVal  {
        //       if countVal * 10 == proVal  {
        //           proVal = proVal + 1
        countDown = countDown - 1;
        self.progressText.text = "" + nf.string(from: NSNumber(value: self.countDown))!
        //         }
        //          countVal = countVal + 0.1;
        //      }
        
        if countDown <= 0 {
            self.window?.rootViewController? = self
            self.window?.rootViewController?.dismiss(animated: true, completion: nil)
            timerProgress.invalidate()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func acceptTheBooking(_ sender: Any) {
        self.respondingToAppointment(status: "2")
        timerProgress.invalidate()
    }
    
    func respondingToAppointment(status:String) {
        Helper.showPI(message:"Loading..")
        
        let params : [String : Any] =  ["token"     : Utility.sessionToken,
                                        "ent_status": status,
                                        "ent_booking_id" :bookingDict["bid"]! as Any ]
        
        print(params)
        
        NetworkHelper.requestPOST(serviceName:API.METHOD.RESPONDAPPT,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("Verification Response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        self.window?.rootViewController? = self
//                                        self.window?.rootViewController?.removeFromParentViewController()
                                        
                                        self.moveTohomeVC()
                                        
                                        // self.window?.rootViewController?.dismiss(animated: true, completion: nil)
                                    } else {
                                    }
                                    
        }) { (Error) in
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    
    func moveTohomeVC() {
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: homeVC)
        
        //  UINavigationBar.appearance().tintColor = UIColor(hex: "689F38")
        
        leftViewController.homeVC = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = homeVC
        // self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }
    
    
    
}
