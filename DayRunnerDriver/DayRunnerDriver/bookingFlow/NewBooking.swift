//
//  NewBooking.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//
import UIKit

class NewBooking: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        perform(#selector(showController), with: nil, afterDelay: 0.01)
    }
    
    
    func showController(){
        let controller = NewBookingViewController()
        present(controller, animated: true, completion:{
            
        })
    }
}
