//
//  SignatureViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 17/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import FloatRatingView


class SignatureViewController: UIViewController,UIImagePickerControllerDelegate ,UINavigationControllerDelegate,FloatRatingViewDelegate {
    @IBOutlet var heightOFTableView: NSLayoutConstraint!
    
    @IBOutlet var signatureView: YPDrawSignatureView!
    @IBOutlet var ratingView: FloatRatingView!
    var noOfServices: Int = 0
    var myImages = [UIImage]()
    
    @IBOutlet var docLabel: UILabel!
    @IBOutlet var contentScrollView: UIView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var timeFar4e: UILabel!
    
    @IBOutlet var custDiscount: UILabel!
    @IBOutlet var distanceFare: UILabel!
    
    @IBOutlet var subtotal: UILabel!
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var signatureTableView: UITableView!
    @IBOutlet var custPhone: UILabel!
    @IBOutlet var custName: UILabel!
    @IBOutlet var grandTotal: UILabel!
    
    var services:[String] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpRatingView()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0x23C590)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.heightOFTableView.constant = self.signatureTableView.contentSize.height
        
    }
    
    
    func setUpRatingView(){
        
        self.ratingView.delegate = self
        ratingView.fullImage = UIImage.init(named: "invoice_green_star_icon")
        ratingView.emptyImage = UIImage.init(named: "invoice_grey_star_icon")
        self.ratingView.maxRating = 5
        self.ratingView.minRating = 1
        self.ratingView.rating = 4
        self.ratingView.editable = true
        self.ratingView.halfRatings = true
        self.ratingView.floatRatings = true
        
    }
    
    @IBAction func completeTheJob(_ sender: Any) {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        signatureTableView.reloadData()
        self.heightOFTableView.constant =  self.signatureTableView.contentSize.height
    }
    
    @IBAction func addServices(_ sender: Any) {
        noOfServices = noOfServices + 1;
        signatureTableView.reloadData()
        self.heightOFTableView.constant =  self.signatureTableView.contentSize.height
    }
    
    @IBAction func hideKeyboard(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func takeDocPic(_ sender: Any) {
        self.selectImage()
        
    }
    @IBAction func retakeSignature(_ sender: Any) {
        self.signatureView.clear()
    }
    
    @IBAction func backToRootVC(_ sender: Any) {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func anyHelp(_ sender: Any) {
        
    }
    
    /*****************************************************************/
    //MARK: - UIImage Picker
    /*****************************************************************/
    
    func selectImage() {
        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: "Please Select Image",
                                                                         message: "",
                                                                         preferredStyle: .actionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel",
                                                              style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery",
                                                            style: .default) { action -> Void in
                                                                self.chooseFromPhotoGallery()
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera",
                                                              style: .default) { action -> Void in
                                                                self.chooseFromCamera()
        }
        actionSheetController.addAction(deleteActionButton)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    func chooseFromPhotoGallery()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    func chooseFromCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerControllerSourceType.camera;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    /*****************************************************************/
    //MARK: - UIImage Picker Delegate Method
    /*****************************************************************/
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        myImages.append(image)
        collectionView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating:Float) {
        //        self.liveLabel.text = NSString(format: "%.2f", self.floatRatingView.rating) as String
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Float) {
        //        self.updatedLabel.text = NSString(format: "%.2f", self.floatRatingView.rating) as String
    }
}

extension SignatureViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return noOfServices
        }else{
            return 1;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier:"signatureTableCell") as! SignatureTableViewCell!
       
            return cell!
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier:"addService") as! AddNewServiceTableViewCell!
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 {
            return 55;
        }else{
            return 36;
        }
    }
}

extension SignatureViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension SignatureViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 1{
            
        }
    }
}

extension SignatureViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "invoiceDocDetails", for: indexPath as IndexPath) as! SignatureCollectionViewCell
        cell.documentsPhoto.image = myImages[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if myImages.count > 0
        {
            docLabel.isHidden = true
            return myImages.count
        }else{
            docLabel.isHidden = false
            return 0
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}


