//
//  OnBookingViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 15/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import JaneSliderControl

class OnBookingViewController: UIViewController {
    let locationManager = CLLocationManager()
    let location = CLLocation()
    
    @IBOutlet var googleMaps: UIButton!
    
    @IBOutlet var getCurrentLocation: UIButton!
    @IBOutlet var wazeMaps: UIButton!
    @IBOutlet var bookingAddress: UILabel!
    @IBOutlet var bookingID: UILabel!
    @IBOutlet var customerName: UILabel!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet weak var leftSlider: SliderControl!
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var timeLeft: UILabel!
    @IBOutlet var distanceLabel: UILabel!
    
    
    
    var status:Int = 0
    
    var SelectedIndex:Int = 0
    
    var disc = String()
    var distFare = String()
    var timeFare = String()
    var grandTotal = String()
    var waitFee = String()
    
    
    var bookingDict  = [Home]()
    var didFindMyLocation = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initiateMap()
        
        self.mapView.bringSubview(toFront: self.googleMaps)
        self.mapView.bringSubview(toFront: self.wazeMaps)
        self.mapView.bringSubview(toFront: self.getCurrentLocation)
        leftSlider.sliderText = "ARRIVED TO PICKUP"
        titleLabel.text = "On the way to pickup"
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0x23C590)
        
        status = bookingDict[SelectedIndex].statCode
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateCustomerInfoStatus()
        mapView.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        mapView.removeObserver(self, forKeyPath:"myLocation" , context: nil)
    }
    
    fileprivate func sliderName(_ slider:SliderControl) -> String {
        switch (slider) {
        case self.leftSlider: return "Middle Left Slider"
        default: return "Unknown Slider"
        }
    }
    
    @IBAction func backToHomeVC(_ sender: Any) {
        
        _ = navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func callToCustomer(_ sender: Any) {
        if status == 7 {
            let dropPhone = "tel://" + bookingDict[SelectedIndex].dropPhone
            if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(url as URL)
            }
        }else{
            let dropPhone = "tel://" + bookingDict[SelectedIndex].pickPhone
            if let url = NSURL(string: dropPhone), UIApplication.shared.canOpenURL(url as URL) {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    
    @IBAction func cancelBooking(_ sender: Any) {
        performSegue(withIdentifier: "toCancelBooking", sender: nil)
    }
    
    @IBAction func getCurrentLocation(_ sender: Any) {
        guard let lat = self.mapView.myLocation?.coordinate.latitude,
            let lng = self.mapView.myLocation?.coordinate.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 16)
        self.mapView.animate(to: camera)
    }
    
    @IBAction func wayToGoogleMaps(_ sender: Any) {
        if status == 7{
            
            let latLongs = bookingDict[SelectedIndex].pickLatlog
            
            let name = latLongs?.components(separatedBy: ",")
            
            let latit = Double(name![0])
            let logit = Double(name![1])
            
            if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                
                UIApplication.shared.openURL(URL(string:
                    "comgooglemaps://?saddr=&daddr=\(String(describing: latit)),\(String(describing: logit))&directionsmode=driving")!)
                
            } else {
                let link = "https://itunes.apple.com/us/app/id585027354?mt=8"
                UIApplication.shared.openURL(NSURL(string: link)! as URL)
                UIApplication.shared.isIdleTimerDisabled = true
            }
            
        }else
        {
            let latLongs = bookingDict[SelectedIndex].dropLatlog
            
            let name = latLongs?.components(separatedBy: ",")
            
            let lat: String = name![0]
            let log: String = name![1]
            
            if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                UIApplication.shared.openURL(URL(string:
                    "comgooglemaps://?saddr=&daddr=\(String(describing: Float(lat))),\(String(describing: Float(log)))&directionsmode=driving")!)
                
            } else {
                let link = "https://itunes.apple.com/us/app/id585027354?mt=8"
                UIApplication.shared.openURL(NSURL(string: link)! as URL)
                UIApplication.shared.isIdleTimerDisabled = true
            }
            
        }
    }
    
    @IBAction func wayToWazeMaps(_ sender: Any) {
        viewWaze(location : location)
    }
    
    @IBAction func shipmentDetails(_ sender: Any) {
        performSegue(withIdentifier: "shipmentDetails", sender: SelectedIndex)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func sliderChanged(_ sender: SliderControl) {
        print("Changed")
    }
    
    ///MARK:- Slider text data
    @IBAction func sliderFinished(_ sender: SliderControl) {
        self.updateBookingStatus(status:status)
    }
    
    func updateCustomerInfoStatus(){
        bookingID.text = bookingDict[SelectedIndex].bookingId
        let ud = UserDefaults.standard
        
        ud.set(String(describing:bookingDict[SelectedIndex].bookingId!) + "|" + bookingDict[SelectedIndex].custChn + "|" + String(describing:status), forKey: USER_INFO.SELBID)
        ud.set(String(describing:status), forKey: USER_INFO.BOOKSTATUS)
        ud.set(bookingDict[SelectedIndex].custChn, forKey: USER_INFO.SELCHN)
        ud.synchronize()

        switch status {
        case 6:
            resetSlider()
            bookingAddress.text = bookingDict[SelectedIndex].pickAddress
            customerName.text = bookingDict[SelectedIndex].pickCustName
            titleLabel.text = "ARRIVED AT PICKUP"
            leftSlider.sliderText = "ARRIVED AT PICKUP"
            status = 7
            break
        case 7:
            ud.set(7, forKey: "DriverArrived")
            ud.synchronize()
            resetSlider()
            bookingAddress.text = bookingDict[SelectedIndex].dropAddress
            customerName.text = bookingDict[SelectedIndex].dropCustName
            titleLabel.text = "TRIP START"
            leftSlider.sliderText = "START"
            status = 8
            break
        case 8:
            resetSlider()
            customerName.text = bookingDict[SelectedIndex].dropCustName
            bookingAddress.text = bookingDict[SelectedIndex].pickAddress
            titleLabel.text = "UNLOADED"
            leftSlider.sliderText = "UNLOAD"
            status = 9
            break
        default:
            break
        }
    }
    
    @IBAction func slideCanceled(_ sender: SliderControl) {
        print("Cancelled")
    }
    
    func updateBookingStatus(status:Int) {
        Helper.showPI(message:"Loading..")
        let ud = UserDefaults.standard
        var  arrivedData = String()
        var  jsonData1   = Data()
               let params : [String : Any]
        if status == 7 {
            if ((ud.object(forKey: "beforeReach")) != nil) {
                jsonData1 = try! JSONSerialization.data(withJSONObject:(ud.object(forKey: "beforeReach")) as Any, options: JSONSerialization.WritingOptions.prettyPrinted)
                arrivedData = String(data: jsonData1, encoding: String.Encoding.utf8)!
                
            }else{
                arrivedData = ""
            }
            
            params  =  ["token"     : Utility.sessionToken,
                        "ent_status": status,
                        "ent_booking_id":bookingDict[SelectedIndex].bookingId as Any,
                        "travel_path_arrived":arrivedData]
            
        }else{
            params  =  ["token"     : Utility.sessionToken,
                        "ent_status": status,
                        "ent_booking_id":bookingDict[SelectedIndex].bookingId as Any]
            
        }
        
        NetworkHelper.requestPUT(serviceName:API.METHOD.UPDATEBOOKINGSTATUS,
                                 params: params,
                                 success: { (response : [String : Any]) in
                                    print("UpdateDriverStatus response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        if self.status == 9{
                                            self.resetSlider()
                                            
                                            let dict          = response["data"]     as! [String:Any]
                                            let invoice       = dict["invoice"]      as! [String:Any]
                                            let distFare      = invoice["distFare"]  as? String
                                            let timeFare      = invoice["timeFare"]  as? String
                                            let waitFare      = invoice["watingFee"] as? String
                                            let grandTot      = invoice["total"]     as? String
                                            let discount      = invoice["Discount"]  as? String
                                            
                                            self.disc       = String(describing: discount!)
                                            self.timeFare   = String(describing: distFare!)
                                            self.distFare   = String(describing: timeFare!)
                                            self.grandTotal = String(describing: grandTot!)
                                            self.waitFee    = String(describing: waitFare!)
                                            self.performSegue(withIdentifier: "toSignatureVC", sender: self.SelectedIndex)
                                        }
                                        self.updateCustomerInfoStatus()
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            Helper.hidePI()
             self.resetSlider()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toSignatureVC" {
            let nextScene = segue.destination as? InvoiceViewController
            nextScene?.bookingDict = bookingDict
            nextScene?.SelectedIndex = sender as! Int
            nextScene?.distFare = self.distFare
            nextScene?.timeFare = self.timeFare
            nextScene?.grandTotal = self.grandTotal
            nextScene?.disc = self.disc
            nextScene?.waitFee = self.waitFee
            
        }else if segue.identifier == "shipmentDetails"{
            let nav =  segue.destination as! UINavigationController
            let nextScene = nav.viewControllers.first as! ShipmentDetailsVC?
            nextScene?.bookingDict = bookingDict
            nextScene?.SelectedIndex = sender as! Int
        }else if segue.identifier == "toCancelBooking"{
            let nav =  segue.destination as! UINavigationController
            let nextScene = nav.viewControllers.first as! CancelViewController?
            nextScene?.bookingID = bookingDict[SelectedIndex].bookingId
            nextScene?.delegate = self
        }
    }
    
    
    func initiateMap() {
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        locationManager.startUpdatingLocation()
    }
    
    func resetSlider()
    {
        self.leftSlider.reset()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if !didFindMyLocation {
            let myLocation: CLLocation = change![NSKeyValueChangeKey.newKey] as! CLLocation
            mapView.camera = GMSCameraPosition.camera(withTarget: myLocation.coordinate, zoom: 16.0)
            
            let marker = GMSMarker(position: myLocation.coordinate)
            marker.icon = UIImage.init(named: "live_tracking_van_icon-ipad")
            marker.map = mapView
            didFindMyLocation = true
            
            
                        var lat = Double()
                        var lon = Double()
                        if status == 7{
                            let latLongs = bookingDict[SelectedIndex].pickLatlog
                            let name = latLongs?.components(separatedBy: ",")
                            lat = Double(name![0])!
                            lon = Double(name![1])!
                        }else{
                            let latLongs = bookingDict[SelectedIndex].dropLatlog
                            let name = latLongs?.components(separatedBy: ",")
                            lat = Double(name![0])!
                            lon = Double(name![1])!
                        }
            self.updateTheDistanceNDEta(start: myLocation.coordinate.latitude, slog: myLocation.coordinate.longitude, end: lat, eLog: lon)
                        let currentLatlog = CLLocation(latitude: myLocation.coordinate.latitude, longitude: myLocation.coordinate.longitude)
                        let custDist = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lon))
                        let itemDist: Float = Float(Int(currentLatlog.distance(from: custDist))/1000)
                        distanceLabel.text = String(describing:itemDist) + "Kms"

        }
    }
    
    
    func updateTheDistanceNDEta(start slat:Double, slog:Double ,end elat:Double, eLog:Double){
        let param: ETAParams = ETAParams()
        
        param.source = ETACoordinates(Double(slat), Double(slog))
        param.destination = ETACoordinates(elat, eLog)
        
        // Get Calculated ETA
        ETAModel.getETA(params: param, completion: { (success, response) in
            if success {
                print("\n\nDuration :\(response.durationText)\nDistance :\(response.distanceText)\n\n")
                self.timeLeft.text = response.durationText
                self.distanceLabel.text = response.distanceText
                
            }
        })
    }
    
    func viewWaze(location : CLLocation) {
        let lat: Double
        let log: Double
        
        if status == 6{
            let latLongs = bookingDict[SelectedIndex].dropLatlog
            let name = latLongs?.components(separatedBy: ",")
            lat = Double(name![0])!
            log = Double(name![1])!
        }else{
            let latLongs = bookingDict[SelectedIndex].pickLatlog
            let name = latLongs?.components(separatedBy: ",")
            lat = Double(name![0])!
            log = Double(name![1])!
        }
        
        //        let latitude:Double = location.coordinate.latitude;
        //        let longitude:Double = location.coordinate.longitude;
        
        var link:String = "waze://"
        let url:NSURL = NSURL(string: link)!
        
        if UIApplication.shared.canOpenURL(url as URL) {
            
            let urlStr:NSString = NSString(format: "waze://?ll=%f,%f&navigate=yes",lat, log)
            
            UIApplication.shared.openURL(NSURL(string: urlStr as String)! as URL)
            UIApplication.shared.isIdleTimerDisabled = true
            
            
        } else {
            link = "http://itunes.apple.com/us/app/id323229106"
            UIApplication.shared.openURL(NSURL(string: link)! as URL)
            UIApplication.shared.isIdleTimerDisabled = true
        }
        
    }
}


extension OnBookingViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse
        {
            locationManager.startUpdatingLocation()
            mapView.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        print(coord.longitude)
        print(coord.latitude)
        didFindMyLocation = false
        if let location = locations.first
        {
            
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 16, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
}

extension OnBookingViewController: GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool)
    {
        print("willMove gesture")
    }
}

extension OnBookingViewController:cancelBookingDelegate{
    func cancelReason() {
        _ = navigationController?.popToRootViewController(animated: true)
    }
}
