//
//  HistoryViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Charts





class HistoryViewController: UIViewController {
    

    @IBOutlet var historyTableView: UITableView!
    @IBOutlet var menuButton: UIButton!
    
    @IBOutlet var datePickerView: UIDatePicker!
    @IBOutlet var bottomDatePicker: NSLayoutConstraint!
 
    @IBOutlet var weeksCollectionView: UICollectionView!
    
    @IBOutlet var thisWeekLabel: UILabel!
    @IBOutlet var amountEarnedinWeek: UILabel!
    @IBOutlet var barGraph: BarChartView!
    var bookingArray = [HomeDetails]()
    var dateSelected:Int = 0
    var selectedIndex:Int = 0
    var stardDate = String()
    var bookingID = String()
    var endDateSelected = String()
    var searchBar = UISearchBar()
    
    var arrayOfDates = [String]()
    var arrayOfWeeks = [[String]]()
    
    var months: [String]!
    var mount:[Double]!
    var selectedWeek:Int = 0
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0x23C590)
        
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.minimal
        
           getWeekCount()
        
         if UserDefaults.standard.bool(forKey: "onHome"){
            menuButton.setImage(#imageLiteral(resourceName: "signup_cross_icon_off"), for: .normal)
         }else{
            menuButton.setImage(#imageLiteral(resourceName: "whiteMenuIcon"), for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
       // self.AssignedTrips()
        
        months = ["SUN","MON","TUE","WED","THU","FRI","SAT"]
        mount = [30.0, 40.0, 20.0, 50.0, 10.0, 30.0,20.0]
        setChart(dataPoints: months, values: mount)
        barGraph.animate(yAxisDuration: 1.5, easingOption: .easeInOutQuart)
    
    }
    

    
    // this function use it to set up the chart
    func setChart(dataPoints:[String], values:[Double])  {
        barGraph.noDataText = "You need to provide data for the chart."
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x:Double(i), yValues: [Double(values[i])])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Earnings data")
        chartDataSet.colors = [Helper.UIColorFromRGB(rgbValue: 0x23C590)]
        let chartData = BarChartData(dataSets: [chartDataSet]) //BarChartData()
        
        chartData.barWidth = 0.9
        barGraph.data = chartData
        barGraph.xAxis.valueFormatter = IndexAxisValueFormatter(values:dataPoints)
        barGraph.xAxis.granularityEnabled = true
        barGraph.xAxis.drawGridLinesEnabled = false
        barGraph.xAxis.labelPosition = .bottom
        barGraph.xAxis.granularity = 2
        barGraph.leftAxis.enabled = true
        
        barGraph.data?.setDrawValues(false)
        barGraph.pinchZoomEnabled = true
        barGraph.scaleYEnabled = true
        barGraph.scaleXEnabled = true
        barGraph.highlighter = nil
        barGraph.doubleTapToZoomEnabled = true
        barGraph.chartDescription?.text = ""
        barGraph.rightAxis.enabled = false
        
        barGraph.leftAxis.axisMinimum = 0.0
        barGraph.leftAxis.axisMaximum = 50.0
        barGraph.xAxis.axisMaximum = 7.0
        barGraph.xAxis.axisMinimum = -1.0
        
    } // end of setchart function
    

    
    func AssignedTrips(){
        bookingArray = [HomeDetails]()
        Helper.showPI(message:"Loading..")
        let weekStartEnd = arrayOfWeeks[selectedWeek]
        
        let params : [String : Any] =  ["token": Utility.sessionToken,
                                        "ent_page_index":"0",
                                        "ent_start_date":weekStartEnd.first! + " 00:00:00",
                                        "ent_end_date":weekStartEnd.last! + " 23:59:59",
                                        "ent_booking_id":bookingID]
        
        print("params :",params)
        NetworkHelper.requestPOST(serviceName:API.METHOD.HISTORYSERVICE,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    let screenSize: CGRect = UIScreen.main.bounds
                                    self.historyTableView.frame = CGRect(x: 0, y: 280, width: screenSize.width, height: screenSize.height - 280)
                                    if response.isEmpty == false {
                                        let data =  response["data"] as? [String:Any]
                                        self.parsingTheServiceResponse(responseData: (data?["appointments"] as! [[String : Any]]))
                                    } else {
                                        
                                    }
                                    
        })
        { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    
    
    func parsingTheServiceResponse(responseData:[[String: Any]]){
        
        for dict in responseData {
            
            let custAddress = dict["addrLine1"] as? String
            let dropAddress = dict["dropLine1"] as? String
            let PickTime    = dict["apntDate"] as? String
            let dropTime    = dict["drop_dt"] as? String
            let custName    = dict["customerName"] as? String
            let custPhone   = dict["customerPhone"] as? String
            let pickLat     = dict["pickup_ltg"] as? String
            let dropLat     = dict["drop_ltg"] as? String
            let bid         = dict["bid"] as? NSNumber
            let status      = dict["statusCode"] as? NSNumber
            
            for shipments in (dict["shipemntDetails"] as? [[String: Any]])!  {
                
                let fare      = shipments["Fare"] as? String
                let dropPhone = shipments["mobile"] as? String
                let dropName  = shipments["name"] as? String
                let signature  = shipments["signatureUrl"] as? String
                
                bookingArray.append(HomeDetails.init(pickAdd: custAddress!, dropAdd: dropAddress!, bookTime: PickTime!, timeLeft: dropTime!, bookingFee: fare!, bid: String(describing:bid!), statCode: status as! Int, pkPhone: custPhone!, dpPhone: dropPhone!, dCName: dropName!, pCName: custName!,pLatLog:pickLat!,dLatLog:dropLat!, sign: signature!))
            }
        }
        historyTableView.reloadData()
        Helper.hidePI()
    }
    
    
    @IBAction func searchOfParticularBooking(_ sender: Any) {
        
        //        if startDate.titleLabel?.text == "START DATE" ||  endDate.titleLabel?.text == "END DATE"{
        //            self.present(Helper.alertVC(title:"Alert", message:"Please Select Both Dates"), animated: true, completion: nil)
        //        }else{
        //            self.AssignedTrips()
        //        }
        //  showSearchBar()
    }
    
    func showSearchBar() {
        searchBar.alpha = 0
        searchBar.placeholder = "Search for booking id"
        searchBar.showsCancelButton = true
        navigationItem.titleView = searchBar
        navigationItem.setLeftBarButton(nil, animated: true)
        UIView.animate(withDuration: 0.5, animations: {
            self.searchBar.alpha = 1
        }, completion: { finished in
            self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        searchBar.isHidden =  true
      //   navigationItem.setLeftBarButtonItem(searchBarButtonItem, animated: true)
        UIView.animate(withDuration: 0.3, animations: {
             self.navigationItem.title = "Invite"
        }, completion: { finished in
            
        })
    }
    
    
    @IBAction func endDate(_ sender: Any) {
        dateSelected = 2
        startTheAnimation()
    }
    
    @IBAction func startDate(_ sender: Any) {
        dateSelected = 1
        startTheAnimation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectDate(_ sender: Any) {
        datePickerView.datePickerMode = UIDatePickerMode.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, EEE"
        let somedateString = dateFormatter.string(from: datePickerView.date)
        
        let innerdateFormatter = DateFormatter()
        innerdateFormatter.dateFormat = "yyyy-MM-dd"
        if dateSelected == 1 {
            stardDate = innerdateFormatter.string(from: datePickerView.date) + "00:00:00"
        //    startDate.setTitle(somedateString,for: .normal)
        }else{
            endDateSelected =  innerdateFormatter.string(from: datePickerView.date) + "23:59:59"
         //   endDate.setTitle(somedateString,for: .normal)
        }
        endTheAnimation()
    }
    
    @IBAction func cancelDatePicker(_ sender: Any) {
        endTheAnimation()
    }
    
    @IBAction func menuAction(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: "onHome"){ //Userdefaults handled in homeVC
            dismiss(animated: true, completion: nil)
        }else{
            slideMenuController()?.toggleLeft()
       }
    }
    
    @IBAction func showTheWeekTransaction(_ sender: Any) {
    }
    // MARK:- start moving the bottonview to up
    func startTheAnimation(){
        UIView.animate(withDuration: 0.4,
                       delay: 0.2,
                       options: .curveEaseOut,
                       animations: {
                        self.bottomDatePicker.constant = 0;
                        self.view.layoutIfNeeded()
                        
        },
                       completion: { finished in
                        print("startAnimation")
        })
    }
    
    // MARK:- start moving the bottonview to Down
    func endTheAnimation(){
        UIView.animate(withDuration: 0.4,
                       delay: 0.2,
                       options: .curveEaseOut,
                       animations: {
                        self.bottomDatePicker.constant = -225;
                        self.view.layoutIfNeeded()
                        
        },
                       completion: { finished in
                        print("EndAnimation")
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toHistoryDetails"{
            let nav =  segue.destination as! UINavigationController
            let nextScene = nav.viewControllers.first as! PastBookingController?
            nextScene?.bookingArray = bookingArray
            nextScene?.selectedIndex = selectedIndex
        }
    }
    
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
        let weekDay = myCalendar.component(.weekday, from: todayDate)
        return weekDay
      
    }
    
    func getWeekCount(){
        let yearStarts = ["2017-01-01","2017-01-02","2017-01-03","2017-01-04","2017-01-05","2017-01-06","2017-01-07"]
        for initialDate in yearStarts {
            let startWeek = self.getDayOfWeek(initialDate)
            if startWeek != 1{
                
            }else{
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let result = formatter.string(from: date)
                arrayOfDates = Dates.printDatesBetweenInterval(Dates.dateFromString(initialDate), Dates.dateFromString(result))
                
                print(arrayOfDates.count)
                var weekDict = [String]()
                for index in 0...arrayOfDates.count-1 {
                    if ((index % 7) == 0) && ((index * 1) != 0) {
                        arrayOfWeeks.append(weekDict)
                        weekDict = [String]()
                        // if index != arrayOfDates.count {
                        weekDict.append(arrayOfDates[index])
                        // }
                    }else
                    {
                        weekDict.append(arrayOfDates[index])
                        if index == arrayOfDates.count - 1{
                            arrayOfWeeks.append(weekDict)
                        }
                    }
                }
            }
            
        }
        selectedWeek = arrayOfWeeks.count-1
        let indexPath = IndexPath(item: selectedWeek, section: 0)
        self.weeksCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .right)
        
//        if let cell: WeeksCollectionViewCell = weeksCollectionView.cellForItem(at: indexPath) as? WeeksCollectionViewCell {
//            cell.selected
//        }
        weeksCollectionView.reloadData()
        
        weeksCollectionView.scrollToItem(at: IndexPath(row: arrayOfWeeks.count-1, section: 0), at: UICollectionViewScrollPosition.right, animated: false)
        self.AssignedTrips()
    }
    
    func selectWeek(_ sender: UIButton) {
        if selectedWeek == sender.tag % 1000{
            selectedWeek = sender.tag % 1000
        }else{
            selectedWeek = sender.tag % 1000
        }
         weeksCollectionView.scrollToItem(at: IndexPath(row: selectedWeek, section: 0), at: UICollectionViewScrollPosition.right, animated: false)
        //setChart(dataPoints: months, values: mount)
        weeksCollectionView.reloadData()
        self.AssignedTrips()
    }
}

///****** tableview datasource*************//
extension HistoryViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return bookingArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"history") as! HistoryTableViewCell!
        
        cell?.bid.text = bookingArray[indexPath.row].bookingId
        cell?.pickAddress.text = bookingArray[indexPath.row].pickAddress
        cell?.dropAddress.text = bookingArray[indexPath.row].dropAddress
        cell?.amount.text = Utility.currencySymbol + bookingArray[indexPath.row].bookingAmt
        cell?.bookingTime.text = Helper.changeDateFormatForHome(bookingArray[indexPath.row].bookingDateNtime)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100;
    }
}

///***********tableview delegate methods**************//
extension HistoryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tableView.deselectRow(at: indexPath, animated: false)
        performSegue(withIdentifier: "toHistoryDetails", sender: nil)///*** moves to booking history***//
        
    }
}

extension HistoryViewController:UISearchBarDelegate
{
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        hideSearchBar()
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("searching is done")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searching is done")
    }
}

extension Date {
    var startOfWeek: Date {
        let date = Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
        let dslTimeOffset = NSTimeZone.local.daylightSavingTimeOffset(for: date)
        return date.addingTimeInterval(dslTimeOffset)
    }
    
    var endOfWeek: Date {
        return Calendar.current.date(byAdding: .second, value: 604799, to: self.startOfWeek)!
    }
}

extension HistoryViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "weeks", for: indexPath as IndexPath) as! WeeksCollectionViewCell
        
        if selectedWeek == indexPath.row {
            cell.weeksButton.isSelected = true
        }else{
            cell.weeksButton.isSelected = false
        }
        let startDate = Helper.changeDateFormatWithMonth(arrayOfWeeks[indexPath.row][0])
        let endDate = Helper.changeDateFormatWithMonth(arrayOfWeeks[indexPath.row][arrayOfWeeks[indexPath.row].count - 1])
        
        cell.weeksButton.setTitle(startDate + "-" + endDate, for: .normal)
        
        //        cell.weeksButton.tag = indexPath.row + 1000
        //        cell.weeksButton.addTarget(self, action: #selector(self.selectWeek), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayOfWeeks.count
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let screenSize: CGRect = UIScreen.main.bounds
        return CGSize(width: screenSize.width/3, height: 60)
    }
}

// MARK: - CollectionView delegate method
extension HistoryViewController:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell: WeeksCollectionViewCell = collectionView.cellForItem(at: indexPath) as! WeeksCollectionViewCell
        cell.weeksButton.isSelected = true
        selectedWeek = indexPath.row
        self.AssignedTrips()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if let cell: WeeksCollectionViewCell = collectionView.cellForItem(at: indexPath) as? WeeksCollectionViewCell {
            cell.weeksButton.isSelected = false
        }
    }
}
