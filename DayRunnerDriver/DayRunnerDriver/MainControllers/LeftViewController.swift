//
//  LeftViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher


class LeftViewController: UIViewController {
    
    @IBOutlet var leftVCTableView: UITableView!
    var homeVC: UIViewController!
    var ProfileVC: UIViewController!
    var HistoryVC: UIViewController!
    var SupportVC: UIViewController!
    var InviteVC: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setTableViewBackgroundGradient(sender: leftVCTableView, Helper .UIColorFromRGB(rgbValue: 0x3EBA74), Helper .UIColorFromRGB(rgbValue: 0x17CA9C))
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let ud = UserDefaults.standard
        ud.set(false, forKey: "cameraOpened")
        
        if USER_INFO.USER_NAME == Utility.userName {
          
            self.profileDetails()
        }else{
            self.leftVCTableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///**********Adding gradiant to tableView**************//
    func setTableViewBackgroundGradient(sender: UITableView, _ topColor:UIColor, _ bottomColor:UIColor) {
        
        let gradientBackgroundColors = [topColor.cgColor, bottomColor.cgColor]
        let gradientLocations = [0.0,1.0]
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = gradientBackgroundColors
        gradientLayer.locations = gradientLocations as [NSNumber]?
        gradientLayer.frame = self.view.bounds
        let backgroundView = UIView(frame: self.view.bounds)
        backgroundView.layer.insertSublayer(gradientLayer, at: 0)
        leftVCTableView.backgroundView = backgroundView
    }
    
    func profileDetails(){
        
        NetworkHelper.requestGETURL(method: API.METHOD.PROFILEDATA + Utility.sessionToken,
                                    success: { (response) in
                                        Helper.hidePI()
                                        if response.isEmpty == false {
                                            if (response["statusCode"] != nil){
                                                let statCode:Int = response["statusCode"] as! Int
                                                if statCode == 401 {
                                                    Helper.hidePI()
                                                    self.sessionExpried()
                                                    self.present(Helper.alertVC(title: "Message", message:"Your Session has beed expired"), animated: true, completion: nil)
                                                    
                                                }else
                                                {
                                                    Helper.hidePI()
                                                    self.present(Helper.alertVC(title: "Message", message:"bad request or  internal server error"), animated: true, completion: nil)
                                                }
                                            }else{
                                                let data =  response["data"] as? [String:Any]
                                                let image = data?["pPic"] as? String!
                                                let name = data?["Name"] as? String!
                                                let ud = UserDefaults.standard
                                                ud.set(image, forKey:  USER_INFO.USERIMAGE)
                                                ud.set(name, forKey:  USER_INFO.USER_NAME)
                                                ud.synchronize()
                                                self.leftVCTableView.reloadData()
                                            }
                                        } else {
                                            
                                        }
                                        
        })
        { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    
    func sessionExpried(){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let signIN: SplashViewController? = storyboard.instantiateViewController(withIdentifier: storyBoardIDs.splashVC!) as? SplashViewController
        
        let window = UIApplication.shared.keyWindow
        window?.rootViewController = signIN
        _ = Utility.deleteSavedData()
    }

    
    func logoutFromApp() {
        Helper.showPI(message:"Logging Out..")
        
        
        let params : [String : Any] =  ["token"   : Utility.sessionToken,
                                        "userType"     : "2"]
        
        NetworkHelper.requestPUT(serviceName:API.METHOD.LOGOUT,
                                 params: params,
                                 success: { (response : [String : Any]) in
                                    print("Verification Response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        
                                        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                                        let signIN: SplashViewController? = storyboard.instantiateViewController(withIdentifier: storyBoardIDs.splashVC!) as? SplashViewController
                                        
                                        let window = UIApplication.shared.keyWindow
                                        window?.rootViewController = signIN
                                        _ = Utility.deleteSavedData()
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
                 Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
}

//*****************Tableview datasource**********//
extension LeftViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1;
        }else{
            return 5;
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier:"first") as! LeftInitialTableViewCell!
           // cell?.profileImage.defa
            
            let imageURL = Utility.userImage
            cell?.profileImage.kf.setImage(with: URL(string: imageURL),
                                           placeholder:UIImage.init(named: "menuslider_defaultimag"),
                                           options: [.transition(ImageTransition.fade(1))],
                                           progressBlock: { receivedSize, totalSize in
            },
                                           completionHandler: { image, error, cacheType, imageURL in
            })
            
            
            cell?.driverName.text = Utility.userName
            
            return cell!
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier:"second") as! LeftSecondTableViewCell!
            switch indexPath.row {
            case 0:
                cell?.leftMenuImage.image = UIImage(named: "menu_home_icon")
                cell?.leftMenuName.text = "Home"
                break;
            case 1:
                cell?.leftMenuImage.image = UIImage(named: "menu_history_icon")
                cell?.leftMenuName.text = "History"
                break;
            case 2:
                cell?.leftMenuImage.image = UIImage(named: "menu_support_icon")
                cell?.leftMenuName.text = "Support"
                break;
            case 3:
                cell?.leftMenuImage.image = UIImage(named: "inviteicon")
                cell?.leftMenuName.text = "Invite"
                break;
            default:
                cell?.leftMenuImage.image = UIImage(named: "menu_logout_icon")
                cell?.leftMenuName.text = "Logout"
                break;
            }
            return cell!
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 {
            return 160;
        }else{
            return 50.0;
        }
    }
}
//*****************Tableview delegate**********//
extension LeftViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.section == 0 {
            let profileController = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.profileVC!) as! ProfileViewController
            self.ProfileVC = UINavigationController(rootViewController: profileController)
            self.slideMenuController()?.changeMainViewController(self.ProfileVC, close: true)
        }else{
            switch indexPath.row {
            case 0:
                let homeController = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.homeVC!) as! HomeViewController
                self.homeVC = UINavigationController(rootViewController: homeController)
                self.slideMenuController()?.changeMainViewController(self.homeVC, close: true)
                
                break
            case 1:
                let historyController = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.historyVC!) as! HistoryViewController
                self.HistoryVC = UINavigationController(rootViewController: historyController)
                self.slideMenuController()?.changeMainViewController(self.HistoryVC, close: true)
                break
            case 2:
                let supportController = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.supportVC!) as! SupportViewController
                self.SupportVC = UINavigationController(rootViewController: supportController)
                self.slideMenuController()?.changeMainViewController(self.SupportVC, close: true)
                break
            case 3:
                let inviteController = storyboard?.instantiateViewController(withIdentifier:storyBoardIDs.inviteVC!) as! InviteViewController
                self.InviteVC = UINavigationController(rootViewController: inviteController)
                self.slideMenuController()?.changeMainViewController(self.InviteVC, close: true)
                break
            case 4:
                logoutFromApp()
            default:
                break
            }
        }
    }
}

