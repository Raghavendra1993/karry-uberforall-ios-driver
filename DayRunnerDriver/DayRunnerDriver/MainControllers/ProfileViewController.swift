//
//  ProfileViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileViewController: UIViewController ,UINavigationControllerDelegate {
    var profilePicUrl = String()
    
    var activeTextField = UITextField()

    @IBOutlet var passwordTF: UITextField!
    @IBOutlet var contentSrollView: UIView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var menuButton: UIButton!
    
    @IBOutlet var vehicleNumber: UITextField!
    @IBOutlet var vehicleType: UITextField!
    @IBOutlet var mobileNumber: UITextField!
    @IBOutlet var firstName: UITextField!
    @IBOutlet var topView: UIView!
    
    var email = String()
    
    @IBOutlet var profileImage: UIImageView!
     let amazonWrapper:AmazonWrapper = AmazonWrapper.sharedInstance()
    var profileUrl = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topView = Helper.setViewBackgroundGradient(sender: topView, Helper.UIColorFromRGB(rgbValue: 0x3EBA74), Helper.UIColorFromRGB(rgbValue: 0x17CA9C))
        self.topView.bringSubview(toFront: self.menuButton)
        profilePicUrl = ""
        makeTextfieldsDisable()
      //  navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
      //  navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0x23C590)
        
       
        amazonWrapper.delegate = self
        self.profileDetails()
    }
    
    
    
    func makeTextfieldsDisable(){
        firstName.isUserInteractionEnabled = false
        mobileNumber.isUserInteractionEnabled = false
        vehicleType.isUserInteractionEnabled = false
        vehicleNumber.isUserInteractionEnabled = false
         passwordTF.isUserInteractionEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    
    @IBAction func tapGestureAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    func dismissView(){
        self.view.endEditing(true)
    }
    
    @IBAction func changeName(_ sender: Any) {
        
       performSegue(withIdentifier: "changeName", sender: nil)
    }
    
    @IBAction func changePhoneNumber(_ sender: Any) {
        
        performSegue(withIdentifier: "changePhone", sender: nil)
    }
    
    @IBAction func changePassword(_ sender: Any) {
        CheckOldPassword.instance.show()
        CheckOldPassword.instance.delegate = self
        
    }
    
    
    
   
    @IBAction func changeProfilePic(_ sender: Any) {
        selectImage()
    }
    
    @IBAction func menuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         passwordTF.text = Utility.checkOldPassword
        //self.navigationController?.isNavigationBarHidden = true
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func profileDetails(){
        Helper.showPI(message:"Loading..")
        let ud = UserDefaults.standard
        if ud.bool(forKey: "cameraOpened") == false {
            
            ud.set(true, forKey: "cameraOpened")
            NetworkHelper.requestGETURL(method: API.METHOD.PROFILEDATA + Utility.sessionToken,
                                        success: { (response) in
                                            Helper.hidePI()
                                            if response.isEmpty == false {
                                                let data =  response["data"] as? [String:Any]
                                                self.firstName.text = data?["Name"] as? String!
                                                self.mobileNumber.text = data?["phone"] as? String!
                                                self.vehicleType.text = data?["typeName"] as? String!
                                                self.vehicleNumber.text = data?["platNo"] as? String!
                                                self.email = (data?["email"] as? String!)!
                                                let imageURL = (data?["pPic"] as? String!)!
                                                self.profileImage.kf.setImage(with: URL(string: imageURL!),
                                                                              placeholder:UIImage.init(named: "profile_defaultimage"),
                                                                              options: [.transition(ImageTransition.fade(1))],
                                                                              progressBlock: { receivedSize, totalSize in
                                                },
                                                                              completionHandler: { image, error, cacheType, imageURL in
                                                })
                                                
                                                let image = data?["pPic"] as? String!
                                                let name = data?["Name"] as? String!
                                                let ud = UserDefaults.standard
                                                ud.set(image, forKey:  USER_INFO.USERIMAGE)
                                                ud.set(name, forKey:  USER_INFO.USER_NAME)
                                                ud.synchronize()
                                                
                                            } else {
                                                
                                            }
                                            
            })
            { (Error) in
                Helper.hidePI()
                self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
            }
        }
    }
    
    
    
    func uploadProfileimgToAmazon(){
        let ud = UserDefaults.standard
        ud.set(true, forKey: "cameraOpened")
        var url = String()
        url = AMAZONUPLOAD.PROFILEIMAGE + Helper.saveProfileImg + "_" + Utility.userId + ".png"
        amazonWrapper.uploadImageToAmazon(withImage: profileImage.image!, imgPath: url)
    }
    
    
    
    // MARK: - Selet profile Image
    func selectImage() {
        
        if profilePicUrl == ""{
            //Create the AlertController and add Its action like button in Actionsheet
            let actionSheetController: UIAlertController = UIAlertController(title: "Please Select Image",
                                                                             message: "",
                                                                             preferredStyle: .actionSheet)
            
            let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel",
                                                                  style: .cancel) { action -> Void in
            }
            actionSheetController.addAction(cancelActionButton)
            
            let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery",
                                                                style: .default) { action -> Void in
                                                                    self.chooseFromPhotoGallery()
            }
            actionSheetController.addAction(saveActionButton)
            
            let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera",
                                                                  style: .default) { action -> Void in
                                                                    self.chooseFromCamera()
            }
            actionSheetController.addAction(deleteActionButton)
            self.present(actionSheetController, animated: true, completion: nil)
        }else{
            let actionSheetController: UIAlertController = UIAlertController(title: "Please Select Image",
                                                                             message: "",
                                                                             preferredStyle: .actionSheet)
            
            let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel",
                                                                  style: .cancel) { action -> Void in
            }
            actionSheetController.addAction(cancelActionButton)
            
            let saveActionButton: UIAlertAction = UIAlertAction(title: "Gallery",
                                                                style: .default) { action -> Void in
                                                                    self.chooseFromPhotoGallery()
            }
            actionSheetController.addAction(saveActionButton)
            
            let deleteActionButton: UIAlertAction = UIAlertAction(title: "Camera",
                                                                  style: .default) { action -> Void in
                                                                    self.chooseFromCamera()
            }
            actionSheetController.addAction(deleteActionButton)
            let removeActionButton: UIAlertAction = UIAlertAction(title: "Remove image",
                                                                  style: .default) { action -> Void in
                                                                    self.removeProfilePic()
            }
            actionSheetController.addAction(removeActionButton)
            self.present(actionSheetController, animated: true, completion: nil)
            
        }
    }
    
    //MARK: - Remove profile pic
    func removeProfilePic(){
        profileImage.image = UIImage.init(named: "profile_defaultimage")
        profilePicUrl = ""
    }
    
    //MARK: - open Photo Gallary
    func chooseFromPhotoGallery()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
         
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
    }
    
    //MARK: - Open Camera
    func chooseFromCamera()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let imagePickerObj = UIImagePickerController()
            imagePickerObj.delegate = self
            imagePickerObj.sourceType = UIImagePickerControllerSourceType.camera;
            imagePickerObj.allowsEditing = false
            self.present(imagePickerObj, animated: true, completion: nil)
        }
        else
        {
            
        }
    }
    
    func updateimage(){
        
        let params : [String : Any] =  ["token"       : Utility.sessionToken,
                                        "ent_profile" : profileUrl,
                                        ]
        
        NetworkHelper.requestPUT(serviceName:API.METHOD.UPDATEPROFILE,
                                 params: params,
                                 success: { (response : [String : Any]) in
                                    print("UpdateDriverStatus response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        //  self.profileDetails()
                                    } else {
                                        
                                    }
                                    
        }) { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
}

//MARK: - Imagepicker delegate
extension ProfileViewController:UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        profileImage.image = image
        self.uploadProfileimgToAmazon()
        profilePicUrl = "profile_profile_default_image"
        //.resizeImage(size: CGSize(width: 100, height: 100))
        let ud = UserDefaults.standard
        ud.set(false, forKey: "cameraOpened")
        self.dismiss(animated: true, completion: nil)
    }
}

extension ProfileViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        dismissView()
        return true
    }
}

extension ProfileViewController:CheckPasswordDelegate{
    func AuthorisedToChangeThePassword() {
           performSegue(withIdentifier: "toChangePassword", sender: nil)
       
    }
}

extension ProfileViewController:AmazonWrapperDelegate{
    func didImageUploadedSuccessfully(withDetails imageURL: String) {
        Helper.showPI(message:"Loading..")
        profileUrl = imageURL
        self.updateimage()
        let ud = UserDefaults.standard
        ud.set(profileUrl, forKey:  USER_INFO.USERIMAGE)
        ud.synchronize()
    }
    
    
    func didImageFailtoUpload(_ error: Error?) {
        
    }
}
