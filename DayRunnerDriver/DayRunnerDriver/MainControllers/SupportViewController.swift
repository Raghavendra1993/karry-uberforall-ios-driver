//
//  SupportViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class SupportViewController: UIViewController {
    
    @IBOutlet var menuButton: UIButton!
    @IBOutlet var supportTableView: UITableView!
    var supportArray = [Support]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        supportTableView.estimatedRowHeight = 10
        supportTableView.rowHeight = UITableViewAutomaticDimension
        self.navigationController?.navigationBar.tintColor = Helper.UIColorFromRGB(rgbValue: 0xffffff)
        self.navigationController?.navigationBar.barTintColor = Helper.UIColorFromRGB(rgbValue: 0x23C590)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getTheSupportData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    ///**********opens the leftmenu********//
    @IBAction func menuAction(_ sender: Any) {
        slideMenuController()?.toggleLeft()
    }
    
    func getTheSupportData(){
        Helper.showPI(message:"Loading..")
        
        NetworkHelper.requestGETURL(method: API.METHOD.SUPPORT  + "0",
                                    success: { (response) in
                                        Helper.hidePI()
                                        if response.isEmpty == false {
                                            
                                            self.parsingTheServiceResponse(responseData: (response["data"] as? [[String: Any]])!)
                                            
                                        } else {
                                            
                                        }
        })
        { (Error) in
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    func parsingTheServiceResponse(responseData:[[String: Any]]){
        
        for items in responseData{
            let dropLat    = items["Name"] as? String
            supportArray.append(Support.init(supName: dropLat!, supArray: items["subcat"]! as! [[String : Any]]))
        }
        self.supportTableView.reloadData()
    }
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
         if segue.identifier == "tosubSupport" {
            let nextScene            = segue.destination as? SupSupportViewController
            nextScene?.supportDict   = supportArray
            nextScene?.selectedIndex = sender as! Int
        }
    }

}


///*********** tableview datasource************///
extension SupportViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return supportArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SupportDetailListCell = tableView.dequeueReusableCell(withIdentifier: "SupportDetailListCell") as! SupportDetailListCell!
        cell.suportLabel.text = supportArray[indexPath.row].supportName
        
        return cell
    }
}

///*********** tableview Delegate************///
extension SupportViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "tosubSupport", sender: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
