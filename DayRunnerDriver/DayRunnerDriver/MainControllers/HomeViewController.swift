//
//  HomeViewController.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 13/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class HomeViewController: UIViewController {
    
    @IBOutlet var menuButton: UIButton!
    @IBOutlet var historyDetails: UIButton!
    @IBOutlet var offlineOnlineButton: UIButton!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var upcomingButton: UIButton!
    @IBOutlet var currentButton: UIButton!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var contentScrollView: UIView!
    @IBOutlet var upcomingTableView: UITableView!
    @IBOutlet var currentTableView: UITableView!
    
    @IBOutlet var currentLocationButton: UIButton!
    @IBOutlet var leadConstraint: NSLayoutConstraint!
    var didFindMyLocation = false
    
    let locationManager = CLLocationManager()
    var timer       = Timer()
    var bookingArray = [Home]()
    var pickMarkers: GMSMarker?
    var pickMarkView: UIImageView?
    
    var tableReloadTime    = Timer()
    
    var bookings = [[String:Any]]()
    
    let pub = VNHPubNubWrapper.sharedInstance() as! VNHPubNubWrapper
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let location = LocationManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.currentButton.isSelected = true
        initiateMap()
        UserDefaults.standard.set(true, forKey: "onHome")
        UserDefaults.standard.set(false, forKey: "noBookings")
    }
    
    func initiateThePubnub() {
        appDelegate.updateDriverState()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.mapView.bringSubview(toFront: self.offlineOnlineButton)
        self.mapView.bringSubview(toFront: self.historyDetails)
        self.mapView.bringSubview(toFront: self.menuButton)
        self.mapView.bringSubview(toFront: self.currentLocationButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.AssignedTrips()
        self.initiateThePubnub()
        self.navigationController?.isNavigationBarHidden = true
        super.viewWillAppear(animated)
        
        // Define identifier
        let notificationName = Notification.Name("gotNewBooking")
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(AssignedTrips), name: notificationName, object: nil)
        
    }
    
    func reloadTableView() {
        self.tableReloadTime.invalidate()
        self.tableReloadTime = Timer.scheduledTimer(timeInterval:60,
                                          target: self,
                                          selector: #selector(reloadTableEveryMinutes),
                                          userInfo: nil,
                                          repeats: true)
        
    }
    
    func reloadTableEveryMinutes(){
        if bookingArray.count != 0 {
            self.currentTableView.reloadData()
        }
    }
    
    //************ open the left menu**************//
    @IBAction func menuAction(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "onHome")
        slideMenuController()?.toggleLeft()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        mapView.clear()
        // Stop listening notification
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "gotNewBooking"), object: nil);
        self.tableReloadTime.invalidate()
    }
    
    @IBAction func goOfflineOnlineAction(_ sender: Any) {
        if offlineOnlineButton.isSelected {
            self.updateMasterStatus(status: "3")
                  Helper.showPI(message:"Go Online..")
        }else{
            self.updateMasterStatus(status: "4")
            Helper.showPI(message:"Go Offline..")
        }
    }
    
    
    func updateMasterStatus(status:String) {
        
        let params : [String : Any] =  ["token"     : Utility.sessionToken,
                                        "ent_status": status]
        
        NetworkHelper.requestPUT(serviceName:API.METHOD.UPDATEDRIVERSTATUS,
                                  params: params,
                                  success: { (response : [String : Any]) in
                                    print("UpdateDriverStatus response : ",response)
                                    Helper.hidePI()
                                    if response.isEmpty == false {
                                        if (response["statusCode"] != nil){
                                            let statCode:Int = response["statusCode"] as! Int
                                            if statCode == 401 {
                                                Helper.hidePI()
                                                self.sessionExpried()
                                                self.present(Helper.alertVC(title: "Message", message:"Your Session has beed expired"), animated: true, completion: nil)
                                                
                                            }else
                                            {
                                                Helper.hidePI()
                                                self.present(Helper.alertVC(title: "Message", message:"bad request or  internal server error"), animated: true, completion: nil)
                                            }
                                            
                                        }else{
                                            
                                            if status == "3"
                                            {
                                                self.offlineOnlineButton.isSelected = false
                                                self.handleTimer()
                                                
                                            }else{
                                                self.stopLocationUpdates()
                                                self.offlineOnlineButton.isSelected = true
                                            }
                                        }
                                    } else {
                                        if status == "4"
                                        {
                                            self.offlineOnlineButton.isSelected = false
                                            self.handleTimer()
                                            
                                        }else{
                                            self.stopLocationUpdates()
                                            self.offlineOnlineButton.isSelected = true
                                        }
                                    }
                                    
        }) { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
            if status == "4"
            {
                self.offlineOnlineButton.isSelected = false
                self.handleTimer()
                
            }else{
               self.stopLocationUpdates()
                self.offlineOnlineButton.isSelected = true
            }
        }
    }
    
       
    func AssignedTrips(){
           bookingArray = [Home]()
        Helper.showPI(message:"Loading..")
        let params : [String : Any] =  ["": ""] //Get APi
        
        print("params :",params)
        
        NetworkHelper.requestGETURL(method: API.METHOD.ASSIGNEDTRIPS + Utility.sessionToken,
                                    success: { (response) in
                                        
                                        if response.isEmpty == false {
                                            
                                            if (response["statusCode"] != nil){
                                                let statCode:Int = response["statusCode"] as! Int
                                                if statCode == 401 {
                                                    Helper.hidePI()
                                                    self.sessionExpried()
                                                    self.present(Helper.alertVC(title: "Message", message:"Your Session has beed expired"), animated: true, completion: nil)
                                                    
                                                }else
                                                {
                                                    Helper.hidePI()
                                                    self.present(Helper.alertVC(title: "Message", message:"bad request or  internal server error"), animated: true, completion: nil)
                                                }
                                                
                                            }else{
                                                
                                                let data =  response["data"] as? [String:Any]
                                                
                                                self.bookings = data?["appointments"] as! [[String : Any]]
                                                
                                                let flag:Int = data!["MasterStatus"] as! Int
                                                
                                                if flag == 3
                                                {
                                                    self.offlineOnlineButton.isSelected = false
                                                    self.handleTimer()
                                                    
                                                }else{
                                                    self.stopLocationUpdates()
                                                    self.offlineOnlineButton.isSelected = true
                                                }
                                                self.parsingTheServiceResponse(responseData: (data?["appointments"] as! [[String : Any]]))
                                            }
                                        } else {
                                            
                                        }
                                        
        })
        { (Error) in
            Helper.hidePI()
            self.present(Helper.alertVC(title: "Message", message:Error.localizedDescription as NSString), animated: true, completion: nil)
        }
    }
    
    func parsingTheServiceResponse(responseData:[[String: Any]]){
        
        for dict in responseData {
            
            let custAddress = dict["addrLine1"] as? String
            let dropAddress = dict["dropLine1"] as? String
            let PickTime    = dict["apntDate"] as? String
            let dropTime    = dict["drop_dt"] as? String
            let custName    = dict["customerName"] as? String
            let custPhone    = dict["customerPhone"] as? String
            let pickLat    = dict["pickup_ltg"] as? String
            let dropLat    = dict["drop_ltg"] as? String
            let bid         = dict["bid"] as? NSNumber
            let status      = dict["statusCode"] as? NSNumber
            let notes    = dict["extraNotes"] as? String
            let custChn    = dict["customerChn"] as? String
            
            
            for shipments in (dict["shipemntDetails"] as? [[String: Any]])!  {
                
                let fare = shipments["Fare"] as? String
                let dropPhone = shipments["mobile"] as? String
                let dropName = shipments["name"] as? String
                let goods = shipments["goodType"] as? String
                let quant = shipments["quantity"] as? String
                let shipImage = shipments["photo"] as? [String]
                
                bookingArray.append(Home.init(pickAdd: custAddress!, dropAdd: dropAddress!, bookTime: PickTime!, timeLeft: dropTime!, bookingFee: fare!, bid: String(describing:bid!), statCode: status as! Int, pkPhone: custPhone!, dpPhone: dropPhone!, dCName: dropName!, pCName: custName!,pLatLog:pickLat!,dLatLog:dropLat!, good: goods!, quant: quant!, exNotes: notes!, shipmentImage: shipImage!, channel: custChn!))
            }
            
        }
        currentTableView.reloadData()
        Helper.hidePI()
        self.placeTheMarkersOnMap()
        self.reloadTableView()
        self.handlingThePublishData()
    }
    
    func handlingThePublishData(){
        
        var bidSum = String()
        var chnSum = String()
        var statCodeSum = String()
        
        for dict  in bookingArray {
            if bidSum.isEmpty {
                
                bidSum = String(describing:dict.bookingId!) + "|" + dict.custChn! + "|" + String(describing:dict.statCode!)
                chnSum = dict.custChn!
                statCodeSum = String(describing:dict.statCode!)
            }
            else{
                bidSum = bidSum + "," + String(describing:dict.bookingId!) + "|" + dict.custChn! + "|" + String(describing:dict.statCode!)
                chnSum = "," + dict.custChn!
                statCodeSum = "," + String(describing:dict.statCode!)
            }
        }
        let ud = UserDefaults.standard
        ud.set(bidSum, forKey: USER_INFO.SELBID)
        ud.set(statCodeSum, forKey: USER_INFO.BOOKSTATUS)
        ud.set(chnSum, forKey: USER_INFO.SELCHN)
        ud.synchronize()
        
    }
    
    
    func sessionExpried(){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let signIN: SplashViewController? = storyboard.instantiateViewController(withIdentifier: storyBoardIDs.splashVC!) as? SplashViewController
        
        let window = UIApplication.shared.keyWindow
        window?.rootViewController = signIN
        _ = Utility.deleteSavedData()
    }
    
    func placeTheMarkersOnMap(){
        if bookingArray.count != 0 {
            for val in 0...bookingArray.count-1{
                let latLongs = bookingArray[val].pickLatlog
                let name = latLongs?.components(separatedBy: ",")
                let latit = Double(name![0])
                let logit = Double(name![1])
                let position = CLLocationCoordinate2D(latitude: latit!, longitude: logit!)
                let house = UIImage(named: "home_map_pin_icon")!.withRenderingMode(.alwaysTemplate)
                let markerView = UIImageView(image: house)
                markerView.tintColor = .red
                pickMarkView = markerView
                var marker = GMSMarker()
                marker = GMSMarker(position: position)
                marker.icon = #imageLiteral(resourceName: "home_map_pin_icon")
                marker.map = mapView
            }
        }
    }
    
    func stopLocationUpdates()  {
        self.timer.invalidate()
        self.pub.unsubscribeFromPresenceChannel()
        self.pub.unsubscribeFromMyChannel()
       // self.location.stopUpdatingLocation()
    }
    
    func updateLcocationToServerChannel()  {
        if Utility.sessionToken == USER_INFO.SESSION_TOKEN {
            self.timer.invalidate()
            self.pub.unsubscribeFromPresenceChannel()
               self.pub.unsubscribeFromMyChannel()
        }else{
            self.location.locationUpdatesToCustomerChannel(val: "")
        }
    }
    
    func handleTimer() {
        self.timer.invalidate()
        pub.subscribeToMyChannel()
        self.pub.subscribeToPresenceMyChannel()
        self.location.startUpdatingLocation()
        self.timer = Timer.scheduledTimer(timeInterval:12,
                                          target: self,
                                          selector: #selector(updateLcocationToServerChannel),
                                          userInfo: nil,
                                          repeats: true)
        
    }
    
    //************ opens the bookingVC**************//
    @IBAction func historyDetailsAction(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "onHome")
        performSegue(withIdentifier: "toBookingHistory", sender: nil)
    }
    
    //*********** upcoming the action to view upcoming bookings**************//
    @IBAction func upcomingAction(_ sender: Any) {
        var frame = mainScrollView.bounds
        frame.origin.x = 1 * frame.size.width
        mainScrollView.scrollRectToVisible(frame, animated: true)
    }
    
    ///*********** upcoming the action to view current bookings**************//
    @IBAction func currentBookingsAction(_ sender: Any) {
        var frame = mainScrollView.bounds
        frame.origin.x = 0 * frame.size.width
        mainScrollView.scrollRectToVisible(frame, animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    ////MARK: - Initiate map
    func initiateMap() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func getCurrentLocation(_ sender: Any) {
        guard let lat = self.mapView.myLocation?.coordinate.latitude,
            let lng = self.mapView.myLocation?.coordinate.longitude else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 16)
        self.mapView.animate(to: camera)
        
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if !didFindMyLocation {
            let myLocation: CLLocation = change![NSKeyValueChangeKey.newKey] as! CLLocation
            mapView.camera = GMSCameraPosition.camera(withTarget: myLocation.coordinate, zoom: 16.0)
            didFindMyLocation = true
        }
    }
    
    ///************ segue identifier method****************//
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "toBookingVC" {
            let nextScene            = segue.destination as? OnBookingViewController
            nextScene?.bookingDict   = bookingArray
            nextScene?.SelectedIndex = sender as! Int
        }else  if segue.identifier == "homeToInvoice" {
            let nextScene            = segue.destination as? InvoiceViewController
            nextScene?.bookingDict   = bookingArray
            nextScene?.SelectedIndex = sender as! Int
            
            let dict          = self.bookings[sender as! Int]
            let invoice       = dict["invoice"]      as! [String:Any]
            let distFare      = invoice["distFare"]  as? String
            let timeFare      = invoice["timeFare"]  as? String
            let waitFare      = invoice["watingFee"] as? String
            let grandTot      = invoice["total"]     as? String
            let discount      = invoice["Discount"]  as? String
            
            nextScene?.distFare   = String(describing: distFare!)
            nextScene?.timeFare   = String(describing: timeFare!)
            nextScene?.grandTotal = String(describing: grandTot!)
            nextScene?.disc       = String(describing: discount!)
            nextScene?.waitFee    = String(describing: waitFare!)
        }
    }
    
    func ifnoBookings(){
        let ud = UserDefaults.standard
        ud.removeObject(forKey: USER_INFO.SELBID)
        ud.removeObject(forKey: USER_INFO.BOOKSTATUS)
        ud.removeObject(forKey:  USER_INFO.SELCHN)
        ud.set(false, forKey: "noBookings")
        ud.synchronize()
    }
}

////MARK: - Slidemenu delegate
extension HomeViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}

// //MARK: - Cllocation delegate method
extension HomeViewController: CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse
        {
            locationManager.startUpdatingLocation()
            mapView.isMyLocationEnabled = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        print(coord.longitude)
        print(coord.latitude)
        didFindMyLocation = false
        if let location = locations.first
        {
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 16, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
}

// //MARK: - GMSMapView Delegate
extension HomeViewController: GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition)
    {
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool)
    {
        print("willMove gesture")
    }
}


// //MARK: - TableView Datasource method
extension HomeViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let ud = UserDefaults.standard
        if bookingArray.count == 0{
            self.ifnoBookings()
        }else{
            ud.set(true, forKey: "noBookings")
        }
        ud.synchronize()
        return bookingArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 99{
            let cell = tableView.dequeueReusableCell(withIdentifier:"current") as! CurrentTableViewCell!
            if bookingArray.count > 0{
                let array = bookingArray[indexPath.row]
                
                if let bookTime = array.bookingDateNtime{
                    cell?.bookingDateNTime.text = Helper.changeDateFormatForHome(bookTime)
                }
                if let amount = array.bookingAmt{
                    cell?.bookingAmt.text = Utility.currencySymbol +  amount
                }
                if let pickAdd = array.pickAddress{
                    cell?.pickUpAddress.text = pickAdd
                }
                if let dropAdd = array.dropAddress{
                    cell?.dropAddress.text = dropAdd
                }
                
                if let bid = array.bookingId{
                    cell?.bookingID.text = bid
                }
                
                if array.statCode == 6 {
                    cell?.timeLeftLabel.text = "Time left for pickup"
                    cell?.timeLeftToPickBooking.text = Helper.getTheTimeDifference(array.bookingDateNtime)
                }else{
                      cell?.timeLeftLabel.text = "Time left for drop"
                    cell?.timeLeftToPickBooking.text = Helper.getTheTimeDifference(array.timeLeftToPick)
                }
            }
            
            return cell!
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier:"upcoming") as! UpcomingTableViewCell!
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120;
    }
}

//MARK: - TableView Delegate method
extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == upcomingTableView{
            if bookingArray[indexPath.row].statCode == 9 {
                performSegue(withIdentifier: "homeToInvoice", sender: indexPath.row)
                tableView.deselectRow(at: indexPath, animated: false)
                
            }else{
                performSegue(withIdentifier: "toBookingVC", sender: indexPath.row)
                tableView.deselectRow(at: indexPath, animated: false)
            }
        }else{
            if bookingArray[indexPath.row].statCode == 9 {
                performSegue(withIdentifier: "homeToInvoice", sender: indexPath.row)
                tableView.deselectRow(at: indexPath, animated: false)
                
            }else{
                performSegue(withIdentifier: "toBookingVC", sender: indexPath.row)
                tableView.deselectRow(at: indexPath, animated: false)
            }
        }
    }
}



//NewBookingViewController
//MARK: - scrollview delegate method
extension HomeViewController : UIScrollViewDelegate
{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.mainScrollView {
            let scrollviewContentObject = scrollView.contentOffset.x
            leadConstraint.constant = scrollviewContentObject/2
            if scrollviewContentObject < scrollView.bounds.size.width/2  {
                self.currentButton.isSelected = true
                self.upcomingButton.isSelected = false
            }
            else
            {
                self.currentButton.isSelected = false
                self.upcomingButton.isSelected = true
            }
        }
    }
}


