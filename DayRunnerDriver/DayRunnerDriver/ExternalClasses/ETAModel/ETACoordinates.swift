//
//  ETACoordinates.swift
//  DayRunner
//
//  Created by Vasant Hugar on 26/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class ETACoordinates: NSObject {
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    init(_ latitude: Double, _ longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    override init() {
        super.init()
    }
}
