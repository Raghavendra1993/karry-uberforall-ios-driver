//
//  ETAModel.swift
//  DayRunner
//
//  Created by Vasant Hugar on 26/05/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class ETAModel: NSObject {
    
    static let API_KEY: String = "AIzaSyB7weLGLZ5OVP-AW9GBmASs2eKkl-akeak" // Get the Key/Server from Google Developer Console
    
    class func getETA(params: ETAParams, completion: @escaping((_ sucess: Bool, _ response: ETAResponse) -> Void)) {
        
        let source = "\(String(describing: params.source.latitude)),\(String(describing: params.source.longitude))"
        let destination = "\(String(describing: params.destination.latitude)),\(String(describing: params.destination.longitude))"
        let key = API_KEY
        
        let etaURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(source)&destination=\(destination)&key=\(key)"
        
        //print("\n\nETA Request: \(etaURL)")
        
        NetworkHelper.request(urlString: etaURL,
                              success: { (response) in
                                
                                //print("ETA Response: \(response)")
                                
                                if (response["error_message"] == nil) {
                                    // Success
                                    if let routes: [AnyObject] = (response["routes"] as? [AnyObject]?)! {
                                        if routes.isEmpty == false {
                                            
                                            if let legs: [AnyObject] = (routes.first! as! [String : AnyObject])["legs"] as? [AnyObject] {
                                                
                                                let resp: ETAResponse = ETAResponse()
                                                if let distance: [String: AnyObject] = ((legs.first as! [String : AnyObject])["distance"] as? [String : AnyObject]?)! {
                                                    
                                                    //print("\n\nETA Distance: \(distance)")

                                                    resp.distanceText = distance["text"] as! String
                                                    resp.distance = Int(distance["value"]! as! NSNumber)
                                                }
                                                
                                                if let duration: [String: AnyObject] = ((legs.first as! [String : AnyObject])["duration"] as? [String : AnyObject]?)! {

                                                    //print("\n\nETA Duration: \(duration)")
                                                    
                                                    resp.durationText = duration["text"] as! String
                                                    resp.duration = (duration["value"]?.description)!
                                                }
                                                completion(true, resp)
                                            }
                                        }
                                        else {
                                            completion(false, ETAResponse())
                                        }
                                    }
                                    else {
                                        completion(false, ETAResponse())
                                    }
                                }
                                else {
                                    // Error
                                     print("ETA Response: \(response)")
                                    completion(false, ETAResponse())
                                    
                                }
        },
                              failure: { (error) in
                                 print("ETA Response: \(error)")
                                completion(false, ETAResponse())
        })
    }
}
