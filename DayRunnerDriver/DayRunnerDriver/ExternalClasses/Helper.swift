//
//  Helper.swift
//  Dayrunner
//
//  Created by Rahul Sharma on 29/11/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//


//let activityData = ActivityData(size: CGSize(width: 30,height: 30),
//                                message: "Loading...",
//                                type: NVActivityIndicatorType.ballRotateChase,
//                                color: UIColor.white,
//                                padding: nil,
//                                displayTimeThreshold: nil,
//                                minimumDisplayTime: nil)
//NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)

import UIKit

class Helper: NSObject {
    
    
    /********************************************************/
    //MARK: - METHODS
    /********************************************************/
    
    /// Method to get Country Code
    ///
    /// - Returns: Country code
    class func getCountyCode() -> String {
        let currentLocale:Locale = (Locale.current as NSLocale) as Locale
        
        return ("" as? String!)!
      //  return (currentLocale.object(forKey: .countryCode) as? String)!
    }
    
    /// Method to get Country Name
    ///
    /// - Returns: Returns Country Name
    class func getCountryName() -> String {
        
        if let name = (Locale.current as NSLocale).displayName(forKey: .
            countryCode, value: getCountyCode()) {
            // Country name was found
            return name
        }
        else {
            // Country name cannot be found
            return getCountyCode()
        }
    }
    
    /// Get Fonts Family
    class func fonts() {
        
        for family: String in UIFont.familyNames {
            print("\n==== : Font family : \(family)")
            
            for name: String in UIFont.fontNames(forFamilyName: family) {
                print("************ : \(name)")
            }
        }
    }
    
    /// Array for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: [AnyObject]
    class func arrayForObj(object: Any?) -> [AnyObject] {
        
        switch object {
            
        case is [AnyObject]: // Array of Type [AnyObject]
            
            return object as! [AnyObject]
            
        case is [Any]: // Array of Type [Any]
            
            return object as! [AnyObject]
            
        default:
            
            return []
        }
    }
    
    // alertVC
    class func alertVC(title:NSString, message:NSString)->UIAlertController {
        let alertController = UIAlertController(title: title as String, message: message as String, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
//        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
//            UIAlertAction in
//            NSLog("Cancel Pressed")
//        }
        
        // Add the actions
        alertController.addAction(okAction)
       // alertController.addAction(cancelAction)
        
        return alertController;
    }
    
    
    /********************************************************/
    //MARK: - Variables
    /********************************************************/
    
    /// Current Date and Time
    static var currentDateTime: String {
        
        let now = Date()
        let dateFormatter = DateFormatter()
        let indianLocale = Locale(identifier: "en_US")
        dateFormatter.locale = indianLocale as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: now)
    }
    
    /// Current Date and Time
    static var saveProfileImg: String {
        
        let now = Date()
        let dateFormatter = DateFormatter()
        let indianLocale = Locale(identifier: "en_US")
        dateFormatter.locale = indianLocale as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        return dateFormatter.string(from: now)
    }

    
    /// Current time stamp
    static var currentTimeStamp: String {
        return String(format: "%0.0f", Date().timeIntervalSince1970 * 1000)
    }
    
    /// Current time Stamp in Int64 format
    static var currentTimeStampInt64: Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    /// Get getCurrentGMTDateTime
    static var currentGMTDateTime: String {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateTimeInIsoFormatForZuluTimeZone: String = dateFormatter.string(from: now)
        return dateTimeInIsoFormatForZuluTimeZone
    }
    
    /// Change the Date format
    ///
    /// - Parameter dateString: Old formate
    /// - Returns: "yyyy-MM-dd HH:mm:ss"
    class func change(_ date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-YYYY hh:mm a"
        let oldDate = dateFormatter.date(from: date)
        
        let indianLocale = Locale(identifier: "en_US")
        dateFormatter.locale = indianLocale as Locale!
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: oldDate!)
    }
    
    class func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    /********************************************************/
    //MARK: - Time Format
    /********************************************************/
    
    /// Change the Format to 07-Dec-2016
    ///
    /// - Parameter date: Date type
    /// - Returns: Returns 07-Dec-2016 as String
    class func changeDate(_ date: Date) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy" //22-Nov-2012
        let formattedDateString = dateFormatter.string(from: date)
        return formattedDateString
    }
    
    /// Get Date From String
    ///
    /// - Parameter dateString: String of Date
    /// - Returns: Date
    class func dateFromString(_ dateString: String) -> Date {
        
        let dateFormatter = DateFormatter()
        let date: Date? = dateFormatter.date(from: dateString)
        return date!
    }
    
    /// Change Date format from "yyyy-MM-dd HH:mm:ss" to "dd-MMM-yyyy"
    ///
    /// - Parameter dateString: String of Date
    /// - Returns: "10-DEC-1991"
    class func changeDateFormat(_ dateString: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date: Date? = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let formattedDateString: String = dateFormatter.string(from: date!)
        return formattedDateString
    }
    
    class func changeDateFormatWithMonth(_ dateString: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date: Date? = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "MMM dd"
        let formattedDateString: String = dateFormatter.string(from: date!)
        return formattedDateString
    }
    
    class func changeDateFormatForHome(_ dateString: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date: Date? = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "MMM dd, hh:MM a"
        let formattedDateString: String = dateFormatter.string(from: date!)
        return formattedDateString
    }


    
    /********************************************************/
    //MARK: - Current Symbol
    /********************************************************/
    class func getCurrency(_ amount: Float) -> String {
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.locale = Locale.current
        currencyFormatter.maximumFractionDigits = 2
        currencyFormatter.minimumFractionDigits = 2
        currencyFormatter.alwaysShowsDecimalSeparator = true
        currencyFormatter.numberStyle = .currency
        let someAmount: NSNumber = NSNumber(value: amount)
        let currencystring: String? = currencyFormatter.string(from: someAmount)
        
        return currencystring!
    }
    

    //MARK: - Progress Ideicator
    // Show with Default Message
    class func showPI(message: String) {
        
        
        let activityData = ActivityData(size: CGSize(width: 30,height: 30),
                                        message: "" ,
                                        type: NVActivityIndicatorType.ballRotateChase,
                                        color: Helper.UIColorFromRGB(rgbValue: 0x484848), // UIColor.white,
                                        padding: nil,
                                        displayTimeThreshold: nil,
                                        minimumDisplayTime: nil)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
       // Hide
    class func hidePI() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    class func addBorderShadow(view : UIView) -> UIView {
        
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize.zero
        view.layer.shadowRadius = 5
        view.layer.shadowPath = UIBezierPath(rect: view.bounds).cgPath
        view.layer.shouldRasterize = true
        return view
    }
    
    class func setViewBackgroundGradient(sender: UIView, _ topColor:UIColor, _ bottomColor:UIColor)->UIView {
        
        let gradient = CAGradientLayer()
        
        gradient.frame = sender.bounds
        gradient.colors = [topColor, bottomColor]
        
        sender.layer.insertSublayer(gradient, at: 0)
        return sender
    }
    
    class func shadowView(sender: UIView, width:CGFloat, height:CGFloat ) {
        
        sender.layer.cornerRadius = 4
        // Drop Shadow
        let squarePath = UIBezierPath(rect:CGRect.init(x: 0, y:0 , width: width, height: height)).cgPath
            
        sender.layer.shadowPath = squarePath;
        //sender.layer.shadowRadius = 4
        sender.layer.shadowOffset = CGSize(width: CGFloat(0.5), height: CGFloat(0.5))
        sender.layer.shadowColor = UIColor.lightGray.cgColor
        sender.layer.shadowOpacity = 1.0
        sender.layer.masksToBounds = false
    }
    
    class func getTheTimeDifferenceDrop(_ stringDate: String) -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date1: Date? = df.date(from: stringDate)
        let date2 = Date()
        let interval: TimeInterval = date2.timeIntervalSince(date1!)
        var hours = Int(interval) / 3600
        // integer division to get the hours part
        
        
        var minutes: Int = (Int(interval) - (hours * 3600)) / 60
        // interval minus hours part (in seconds) divided by 60 yields minutes
        var timeDiff: String
        if minutes < 0 || hours <= 0 {
            if hours < 0 {
                hours = -(hours)
            }
            minutes = -(minutes)
            timeDiff = "-" + String(describing: hours) + ":" + String(describing: minutes) + " Hrs"
            //  timeDiff = "-\(hours):%02d"
        }
        else {
            timeDiff =  String(describing: hours) + ":" + String(describing: minutes) + " Hrs"
            //  timeDiff = "\(hours):%02d"
        }
        return timeDiff
    }


    class func getTheTimeDifference(_ stringDate: String) -> String {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date1: Date? = df.date(from: stringDate)
        let date2 = Date()
        let interval: TimeInterval? = date1?.timeIntervalSince(date2)
        var hours = Int(interval!) / 3600
        // integer division to get the hours part
        var minutes: Int = (Int(interval!) - (hours * 3600)) / 60
        // interval minus hours part (in seconds) divided by 60 yields minutes
        var timeDiff: String
        if minutes < 0 || hours <= 0 {
            if hours < 0 {
                hours = -(hours)
            }
            minutes = -(minutes)
            timeDiff = "-" + String(describing: hours) + ":" + String(describing: minutes) + " Hrs"
        }
        else {
             timeDiff =  String(describing: hours) + ":" + String(describing: minutes) + " Hrs"
        }
        return timeDiff
    }

    class  func toastView(messsage : String, view: UIView ){
        let toastLabel = UILabel(frame: CGRect(x: 0, y: 100, width: view.frame.size.width,  height : 35))
        toastLabel.backgroundColor = Helper.UIColorFromRGB(rgbValue: 0x484848)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = NSTextAlignment.center;
        view.addSubview(toastLabel)
        toastLabel.text = messsage
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIView.animate(withDuration: 2.0, delay: 0.3, options: UIViewAnimationOptions.curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        })
    }
    
}
