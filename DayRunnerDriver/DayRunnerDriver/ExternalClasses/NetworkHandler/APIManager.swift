//
//  APIManager.swift
//  Trustpals
//
//  Created by apple on 10/02/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit


/// Response Flag
///
/// - Default: -1 For Default
/// - Success: 0 For Success
/// - Failure: 1 For Failure
enum APIResponseFlag: Int {
    
    case Default = -1
    case Success = 0
    case Failure = 1
}

/// Staus Class
class APIStatus: NSObject {
    
    /// Check if Success
    var Success: Bool = false
    
    // Check for Error to Show Message
    var Error: NSError? = nil
    
    
    /// Initialize
    ///
    /// - Parameters:
    ///   - success: set true if Success else False
    ///   - error: Set NSError
    init(success: Bool, error: NSError?) {
        self.Success = success
        self.Error = error
    }
}

/// Class For API Keys
class APIKeys: NSObject {
    
    
    /// Error Number
    var num: Int = 0
    
    
    /// Error Flag By default -1
    var flag: APIResponseFlag = .Default
    
    
    /// Error Message
    var msg: String = ""
    
    
    /// Initilize
    ///
    /// - Parameters:
    ///   - flag: errFlag
    ///   - msg: errMsg
    ///   - num: errNum
    init(flag: APIResponseFlag, msg: String, num: Int) {
        self.flag = flag
        self.msg = msg
        self.num = num
    }
}

/// Response Class
class APIResponse: NSObject {
    
    /// Status
    var status: APIStatus? = nil
    
    /// Error Keys errFlag, errMsg, errNum
    var errKeys: APIKeys? = nil
    
    /// Data in DIctionary format
    var data: [String: AnyObject] = [:]
}


/// Request Class
class APIRequest: NSObject {
    
    
    /// Method to append with Base url
    var method: String
    
    // Params to Be sent in request in Dictionary format
    var params: [String: Any]
    
    
    /// Initialize
    ///
    /// - Parameters:
    ///   - method: Method to append with Base url
    ///   - params: Params to Be sent in request in Dictionary format
    init(method: String, params: [String: Any]) {
        self.method = method
        self.params = params
    }
}

class APIManager: NSObject {
    
    
    /// Show Message There is Error
    ///
    /// - Parameter response: Response Obj
    class func showErrorMessage(response: APIResponse) {
        
        let flag: APIResponseFlag = (response.errKeys?.flag)!
        
        switch (flag) {
            
        case .Success:
            
            break
            
        case .Failure:
            
            break
            
        default:
            break
        }
    }
    
//    class func getVerificationCode(params: [String: Any], completion: @escaping (APIResponse) -> Void) {
//        
//        NetworkHelper.requestPOST1(method: API.METHOD.GET_VERIFICATION_CODE,
//                                   params: params,
//                                   completion: { (response) in
//                                    
//                                    if (response.status?.Success)! {
//                                        showErrorMessage(response: response)
//                                    }
//                                    completion(response)
//        })
//    }
    
//    class func verifyCode(params: [String: Any], completion: @escaping (APIResponse) -> Void) {
//        
//        NetworkHelper.requestPOST1(method: API.METHOD.VERIFY_CODE,
//                                   params: params,
//                                   completion: { (response) in
//                                    
//                                    showErrorMessage(response: response)
//                                    completion(response)
//        })
//        
//    }
}
