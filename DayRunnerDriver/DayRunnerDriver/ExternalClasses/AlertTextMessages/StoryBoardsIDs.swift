//
//  StoryBoardsIDs.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 21/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit


struct storyBoardIDs {
    static let profileVC        = "ProfileViewController" as String!
    static let homeVC           = "HomeViewController" as String!
    static let historyVC        = "HistoryViewController" as String!
    static let supportVC        = "SupportViewController" as String!
    static let splashVC         = "SplashViewController" as String!
    static let helpVC           = "HelpViewController"
    static let newBookingVC     = "NewBookingViewController" as String
    static let signIN           = "SignInViewController" as String
    static let StoryBoard       = "Main" as String
    
    static let inviteVC         = "InviteViewController" as String!
}
