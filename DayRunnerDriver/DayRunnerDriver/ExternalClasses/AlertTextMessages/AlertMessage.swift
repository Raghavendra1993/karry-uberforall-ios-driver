//
//  AlertMessage.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 21/04/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

struct signup {
    static let Message          = "Message" as String!
    static let InValidEmail     = "Enter Valid email id" as String!
    static let selectOperator   = "Please select operator" as String!
    static let enterFirstName   = "Please enter first name" as String!
    static let enterLastName    = "Please enter Last name" as String!
    static let enterPhoneNumber = "Please enter Phone number" as String!
    static let enterEmailId     = "Please enter Email Address" as String!
    static let enterPassword    = "Please enter password" as String!
    static let selectImage      = "Please Select Image" as String!
    static let cancel           = "Cancel" as String!
    static let gallery          = "Gallery" as String!
    static let camera           = "Camera" as String!
    static let USER_PIC         = "user_pic"
 }

struct vehicleDetails {
    static let Message          = "Message" as String!
    static let vehicleNumber    = "Please enter Plate number" as String!
    static let vehicleColor     = "Please enter vehicle Specialities" as String!
    static let vehicleType      = "Please enter vehicle type" as String!
    static let vehicleMake      = "Please enter vehicle make" as String!
    static let vehicleModel     = "Please select vehcile model" as String!
    static let selectImage      = "Please Select Image" as String!
    static let cancel           = "Cancel" as String!
    static let gallery          = "Gallery" as String!
    static let camera           = "Camera" as String!
    static let USER_PIC         = "user_pic" as String!
}




