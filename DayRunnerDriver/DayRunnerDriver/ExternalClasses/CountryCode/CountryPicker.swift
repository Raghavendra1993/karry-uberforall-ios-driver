//
//  VNHCountryPicker.swift
//  Trustpals
//
//  Created by Rahul Sharma on 02/12/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import UIKit

protocol CountryPickerDelegate:class
{
    func didPickedCountry(country: Country)
}

/// Country Class
class Country: NSObject {

    let dialCode: String
    let code: String
    let name: String
    let flag: UIImage
    
    init(dialCode: String, code: String, name: String, flag: UIImage) {
        self.dialCode = dialCode
        self.code = code
        self.name = name
        self.flag = flag
    }
    
    override init()
    {
        self.dialCode = ""
        self.code = ""
        self.name = ""
        self.flag = UIImage()
    }
}

class CountryPicker: UIViewController {

    open weak var delegate: CountryPickerDelegate?
    @IBOutlet weak var CPSearchBar: UISearchBar!
    @IBOutlet weak var CPTableView: UITableView!
    var arrayOfCountry: [Country] = []
    var arrayOfSearch = [Country]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        arrayOfCountry = getCountry()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /// Get County Code
    ///
    /// - Returns: Returns Array
    fileprivate func getCountry() -> [Country] {
        
        let path = Bundle.main.path(forResource: "CallingCodes", ofType: "plist")
        let array = NSArray(contentsOfFile: path!) as! [[String: String]]
        
        let temp: NSMutableArray = []
        for dict in array {
            temp.add(Country(dialCode: dict["dial_code"]!,
                               code: dict["code"]!,
                               name: dict["name"]!,
                               flag: countryImage(code: dict["code"]!)))
        }
        return temp as Array as! [Country]
    }
    
    /// Country Flag Based on country code
    ///
    /// - Parameter code: country code
    /// - Returns: Flag Image
    fileprivate func countryImage(code: String) -> UIImage {
        let bundle = "assets.bundle/"
        return UIImage(named: bundle + code.lowercased() + ".png") ?? UIImage()
    }
    
    
    /// Get Counrty DialCode Based on Country code
    ///
    /// - Parameter code: Country Code
    /// - Returns: Dial Code Ex +91, +1
    class func dialCode(code: String) -> Country {
    
        let path = Bundle.main.path(forResource: "CallingCodes", ofType: "plist")
        let array = NSArray(contentsOfFile: path!) as! [[String: String]]
        
        // Populate the results
        for dict: [String: String] in array {
            
            // Check for Dial Code
            if code == dict["code"] {
                let bundle = "assets.bundle/"
                let flag = UIImage(named: bundle + code.lowercased() + ".png")!
                
                // Return All the object
                return Country(dialCode: dict["dial_code"]!,
                                 code: dict["code"]!,
                                 name: dict["name"]!,
                                 flag: flag)
            }
        }
        return Country()
    }
}

extension CountryPicker: UITableViewDelegate, UITableViewDataSource {
    
    //MARK: - UItableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Check Array has Search result
        if arrayOfSearch.count != 0 {
            return arrayOfSearch.count
        }
        return arrayOfCountry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell: CountryPickerCell = tableView.dequeueReusableCell(withIdentifier: "CountryPickerCell", for: indexPath) as! CountryPickerCell
        
        // If Array has Search result
        // Load cell with Search results
        var country: Country? = nil
        if arrayOfSearch.count != 0 {
            country = arrayOfSearch[indexPath.row]
        }
        else {
            // Else all the Countries
            country = arrayOfCountry[indexPath.row]
        }
        // Update UI
        cell.countryImage.image = country?.flag
        cell.countryName.text = String(format: "%@ (%@)",(country?.name)!,(country?.dialCode)!)
        
        return cell
    }
    
    //MARK: - UItableView Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
        }
        
        // If Array has Search result
        // Get selected data from Search results
        var country: Country? = nil
        if arrayOfSearch.count != 0 {
            country = arrayOfSearch[indexPath.row]
        }
        else {
            // Else Get selected data from All Countries results
            country = arrayOfCountry[indexPath.row]
        }
        delegate?.didPickedCountry(country: country!)
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .none
        }
    }
}

extension CountryPicker: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContent(searchBar.text!)
    }
    
    
    /// Filter Content
    ///
    /// - Parameter searchText: Search String
    fileprivate func filterContent(_ searchText: String) {
        
        arrayOfSearch.removeAll()
        // Populate the results
        for country: Country in arrayOfCountry {
            
            let countryName = country.name.lowercased()
            if (countryName.range(of: searchText.lowercased()) != nil) {
                arrayOfSearch.append(country)
            }
        }
        print("Filtered Country : \(arrayOfSearch)")
        self.CPTableView.reloadData()
    }
}
