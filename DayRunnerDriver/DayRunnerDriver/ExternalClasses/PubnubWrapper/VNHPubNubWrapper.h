//
//  PubNubWrapper.h
//  Homappy
//
//  Created by Rahul Sharma on 29/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PubNub/PubNub.h>

/**
 *  Publish Key and Subscription Key are
 *  Unique for Each APP
 *  Need to change
 */
#define PUBLISH_KEY   @"pub-c-9becfb52-7b95-4eff-8e74-958a871ed1c7"
#define SUBSCRIBE_KEY @"sub-c-0dc5d148-bddc-11e5-bcee-0619f8945a4f"

@protocol VNHPubNubWrapperDelegate <NSObject>

@optional
/**
 *  Received Message from PubNub
 *
 *  @param message messageDict
 *  @param channel Channel that we published to
 */
- (void)receivedMessage:(NSDictionary *)message andChannel:(NSString *)channel;

@end

@interface VNHPubNubWrapper : NSObject <PNObjectEventListener>

// PubNUb Client
@property (nonatomic) PubNub *client;

// My Channel that i get on eiher Login or SignUp
@property (nonatomic) NSString *my_channel;

// Array of Channels that subscribed with
@property (nonatomic) NSArray *subscribed_channels;

// PubNub Wrapper Delegate
@property (nonatomic, weak) id<VNHPubNubWrapperDelegate>delegate;


// To Add Timer for every 5 sec
@property (nonatomic, strong) NSDictionary *params;
@property (nonatomic, strong) NSString *toChannel;
@property (nonatomic, strong) NSTimer *timerObj;

/**
 *  Shared Instance
 *
 *  @return returns Object of its own Class
 */
+ (id)sharedInstance;
/**
 *  Delete Instance
 *
 *  @return returns Object of its own Class
 */
+ (void)deleteInstance;

/**
 *  Initiation of PubNub Method
 *  Call this method from AppDelagate, with Calling sharedInstance
 */
- (void)initiatePubNub;

/**
 *  Subscribe to MyChannels
 *  To the channel that I get on login or Signup
 */
- (void)subscribeToMyChannel;
/**
 *  Subscribe with Channels
 *
 *  @param channels array of Channels
 */
- (void)subscribeToChannels:(NSArray *)channels;

/**
 *  Publish with Parameter to Channel
 *
 *  @param parameter Parameter Dict
 *  @param channel   toChannel
 */
- (void)publishWithParameter:(NSDictionary *)parameter
                   toChannel:(NSString *)channel;
/**
 *  Publish with complition block
 *
 *  @param parameter       Parameter Dict
 *  @param channel         channel Publish to
 *  @param completionBlock complition block
 */
- (void)publishWithParameter:(NSDictionary *)parameter
                   toChannel:(NSString *)channel
              withCompletion:(void(^)(bool success))completionBlock;
/**
 *  Publush with Parameter To Channel and Start timer
 *
 *  @param parameter  Parameter Dict
 *  @param channel    ChannelTo
 *  @param startTimer Start Timer YES if You want, else NO
 */
- (void)publishWithParameter:(NSDictionary *)parameter
                   toChannel:(NSString *)channel
                  startTimer:(BOOL)startTimer;

/**
 *  Stop timer
 */
- (void)stopTimer;

/**************************************** Unsubscribe from Channels ***************************************/

//Unsubscription process results arrive to listener
//which should adopt to PNObjectEventListener protocol and registered using:

/**
 *  Unsubscribe from My Channels
 */
- (void)unsubscribeFromMyChannel;
/**
 *  Unsubscribe from All Channels
 */
- (void)unsubscribeFromAllChannel;
/**
 *  Unsubscribe from Custom Channels
 *
 *  @param channels Channels Array
 */
- (void)unsubscribeFromChannels:(NSArray *)channels;
-(void)channel:(NSDictionary *)dict;
- (void)subscribeToPresenceMyChannel;
- (void)unsubscribeFromPresenceChannel;
@end
