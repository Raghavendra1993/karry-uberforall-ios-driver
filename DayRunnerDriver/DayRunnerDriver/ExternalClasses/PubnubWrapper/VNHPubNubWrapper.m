//
//  PubNubWrapper.m
//  Homappy
//
//  Created by Rahul Sharma on 29/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "VNHPubNubWrapper.h"
#import "DayRunnerDriver-Swift.h"

@implementation VNHPubNubWrapper

static VNHPubNubWrapper *share;

/**********************************PubNub User Defined Methods*******************************************/
/**
 *  Shared Instance
 *
 *  @return returns Object of its own Class
 */
+ (id)sharedInstance {
    if (!share) {
        share = [[self alloc] init];
    }
    return share;
}
+ (void)deleteInstance {
    if (share) {
        share = nil;
    }
}

- (instancetype)init{
    
    if (self == [super init]) {
        
        PNConfiguration *configuration = [PNConfiguration configurationWithPublishKey:
                                          Utility.publishKey
                                                                         subscribeKey:
                                          Utility.subscribeKey];
        
        configuration.uuid= [NSString stringWithFormat:@"m_%@",Utility.userId];
        configuration.presenceHeartbeatValue = 10;
        configuration.presenceHeartbeatInterval = 5;
        configuration.authKey = nil;
        self.client = [PubNub clientWithConfiguration:configuration];
        
    }
    return self;
}

/**
 *  Initiation of PubNub Method
 *  Call this method from AppDelagate, with Calling sharedInstance
 */
- (void)initiatePubNub {
    
//    self.my_channel = [[NSUserDefaults standardUserDefaults] objectForKey:@""];
//    [self.client subscribeToChannels: @[self.my_channel] withPresence:YES];
}

/*--------------------------------*/
#pragma marks - Subcription Methods
/*--------------------------------*/
/**
 *  Subscribe to MyChannels
 *  To the channel that I get on login or Signup
 */
- (void)subscribeToMyChannel {
    
    self.my_channel = Utility.driverChannel;
    [self.client addListener:self];
    [self.client subscribeToChannels: @[self.my_channel] withPresence:YES];
}

/**
 *  Subscribe with Channels
 *
 *  @param Channels array of Channels
 */
- (void)subscribeToChannels:(NSArray *)channels {
    
    [self.client addListener:self];
    [self.client subscribeToChannels:channels withPresence:YES];
    self.subscribed_channels = [channels mutableCopy];
}

/**
 *  Subscribe to presence
 *  To the channel that I get on login or Signup
 */
- (void)subscribeToPresenceMyChannel {
    [self.client addListener:self];
    [self.client subscribeToChannels: @[Utility.presenceChannel] withPresence:YES];
      NSLog(@"presenceChannel subscribed");
}
- (void)unsubscribeFromPresenceChannel{
  [self.client unsubscribeFromChannels:@[Utility.presenceChannel]
                            withPresence:NO];
    NSLog(@"presenceChannel Unsubscribed");
}

/*--------------------------------*/
#pragma marks - Publishing Methods
/*--------------------------------*/

/**
 *  Publish with Parameter to Channel
 *
 *  @param parameter Parameter Dict
 *  @param channel   toChannel
 */
- (void)publishWithParameter:(NSDictionary *)parameter
                   toChannel:(NSString *)channel {
    
    [self.client publish:parameter
               toChannel:channel
          storeInHistory:YES
          withCompletion:^(PNPublishStatus *status) {
              
              // Check whether request successfully completed or not.
              if (!status.isError) {
                  // Message successfully published to specified channel.
               //   NSLog(@"\n\nRequest successfully completed. Message successfully published to specified channel.\n\n");
                  
                NSLog(@"\n\nPublish with Parameter : %@ \nTo Channel : %@\n\n",parameter,channel);
              }
              // Request processing failed.
              else {
                  
                  // Handle message publish error. Check 'category' property to find out possible issue
                  // because of which request did fail.
                  //
                  // Request can be resent using: [status retry];
                  NSLog(@"\n\nPublish error : %@\n\n",[status stringifiedCategory]);
              }
          }];
}
/**
 *  Publish with complition block
 *
 *  @param parameter       Parameter Dict
 *  @param channel         channel Publish to
 *  @param completionBlock complition block
 */
- (void)publishWithParameter:(NSDictionary *)parameter
                   toChannel:(NSString *)channel
              withCompletion:(void(^)(bool success))completionBlock{
    
    [self.client publish:parameter
               toChannel:channel
          storeInHistory:YES
          withCompletion:^(PNPublishStatus *status) {
              
              // Check whether request successfully completed or not.
              if (!status.isError) {
                  // Message successfully published to specified channel.
                  NSLog(@"\n\nRequest successfully completed. Message successfully published to specified channel.\n\n");
                  completionBlock(YES);
              }
              // Request processing failed.
              else {
                  
                  // Handle message publish error. Check 'category' property to find out possible issue
                  // because of which request did fail.
                  //
                  // Request can be resent using: [status retry];
                  NSLog(@"\n\nPublish error : %@\n\n",[status stringifiedCategory]);
                  completionBlock(NO);
              }
          }];
}


/**
 *  Publush with Parameter To Channel and Start timer
 *
 *  @param parameter  Parameter Dict
 *  @param channel    ChannelTo
 *  @param startTimer Start Timer YES if You want, else NO
 */
- (void)publishWithParameter:(NSDictionary *)parameter
                   toChannel:(NSString *)channel
                  startTimer:(BOOL)startTimer {
    
    [self.client publish:parameter
               toChannel:channel
          storeInHistory:YES
          withCompletion:^(PNPublishStatus *status) {
              
              // Check whether request successfully completed or not.
              if (!status.isError) {
                  // Message successfully published to specified channel.
              //    NSLog(@"\n\nRequest successfully completed. Message successfully published to specified channel.\n\n");
                  
               //   NSLog(@"\n\nPublish with Parameter : %@ \nTo Channel : %@\n\n",parameter,channel);
              }
              // Request processing failed.
              else {
                  
                  // Handle message publish error. Check 'category' property to find out possible issue
                  // because of which request did fail.
                  //
                  // Request can be resent using: [status retry];
                  NSLog(@"\n\nPublish error : %@\n\n",[status stringifiedCategory]);
              }
          }];
    
    self.params = [parameter mutableCopy];
    self.toChannel = channel;
    
    if (startTimer && !self.timerObj) {
        
        self.timerObj = [NSTimer scheduledTimerWithTimeInterval:5
                                                         target:self
                                                       selector:@selector(startTimer)
                                                       userInfo:nil
                                                        repeats:YES];
        NSLog(@"Timer started");
    }
}
/**
 *  Start timer
 */
- (void)startTimer {
    [self publishWithParameter:self.params
                     toChannel:self.toChannel
                    startTimer:YES];
}
/**
 *  Stop timer
 */
- (void)stopTimer {
    if (self.timerObj) {
        [self.timerObj invalidate];
        self.timerObj = nil;
    }
    NSLog(@"Tmer stopped");
}

-(void)channel:(NSDictionary *)dict{
    
        [self.client hereNowForChannel:Utility.driverPublishChn withVerbosity:PNHereNowUUID
                            completion:^(PNPresenceChannelHereNowResult *result,
                                         PNErrorStatus *status) {
                                // Check whether request successfully completed or not.
                                if (!status.isError) {
                                    
                                    // Handle downloaded presence information using:
                                    //   result.data.uuids - list of uuids.
                                    NSInteger count = [[NSString stringWithFormat:@"%@",result.data.occupancy] integerValue];
                                    if (count > 0)
                                    {
                                        [self publishWithParameter:dict toChannel:Utility.driverPublishChn];
                                    }else{
                                        NSLog(@"Customer not in live tracking");
                                    }
                                }
                                else
                                {
                                    
                                    // Handle presence audit error. Check 'category' property to find
                                    // out possible issue because of which request did fail.
                                    //
                                    // Request can be resent using: [status retry];
                                }
                                
                            }];
}

/**********************************PubNub SDK Methods*******************************************/

/**
 @brief  Notify listener about new message which arrived from one of remote data object's live feed
 on which client subscribed at this moment.
 
 @param client  Reference on \b PubNub client which triggered this callback method call.
 @param message Reference on \b PNResult instance which store message information in \c data
 property.
 
 @since 4.0
 */
- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message {
    
    // Handle new message stored in message.data.message
    if (message.data.channel) {
        
        // Message has been received on channel group stored in
        // message.data.subscribedChannel
    }
    else {
        
        // Message has been received on channel stored in
        // message.data.subscribedChannel
    }
    NSLog(@"\n\nReceived message: %@ \non channel %@ \nat %@\n\n", message.data.message,
          message.data.subscription, message.data.timetoken); //subscribedChannel
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(receivedMessage:andChannel:)]) {
        
        [self.delegate receivedMessage:message.data.message
                            andChannel:message.data.subscription];
    }
}
/**
 @brief      Notify listener about subscription state changes.
 @discussion This callback can fire when client tried to subscribe on channels for which it doesn't
 have access rights or when network went down and client unexpectedly disconnected.
 
 @param client Reference on \b PubNub client which triggered this callback method call.
 @param status  Reference on \b PNStatus instance which store subscriber state information.
 
 @since 4.0
 */
- (void)client:(PubNub *)client didReceiveStatus:(PNSubscribeStatus *)status {
    
    if (status.category == PNUnexpectedDisconnectCategory) {
        // This event happens when radio / connectivity is lost
    }
    else if (status.category == PNConnectedCategory) {
        
        // Connect event. You can do stuff like publish, and know you'll get it.
        // Or just use the connected event to confirm you are subscribed for
        // UI / internal notifications, etc
        NSLog(@"\n\nPubnub Connected. \nSubscribed Channels : %@\n\n",[status subscribedChannels]);
        
    }
    else if (status.category == PNReconnectedCategory) {
        
        // Happens as part of our regular operation. This event happens when
        // radio / connectivity is lost, then regained.
    }
    else if (status.category == PNDecryptionErrorCategory) {
        
        // Handle messsage decryption error. Probably client configured to
        // encrypt messages and on live data feed it received plain text.
    }
}

/**************************************** Unsubscribe from Channels ***************************************/

//Unsubscription process results arrive to listener
//which should adopt to PNObjectEventListener protocol and registered using:


/**
 *  Unsubscribe from My Channels
 */
- (void)unsubscribeFromMyChannel {
    self.my_channel = Utility.driverChannel;
    [self.client unsubscribeFromChannels:@[self.my_channel]
                            withPresence:NO];
}
/**
 *  Unsubscribe from All Channels
 */
- (void)unsubscribeFromAllChannel {
    
    [self.client unsubscribeFromChannels:self.subscribed_channels
                            withPresence:NO];
}
/**
 *  Unsubscribe from Custom Channels
 *
 *  @param channels Channels Array
 */
- (void)unsubscribeFromChannels:(NSArray *)channels {
    
    [self.client unsubscribeFromChannels:channels
                            withPresence:NO];
}

@end
