//
//  Utility.swift
//  Dayrunner
//
//  Created by Rahul Sharma on 28/09/16.
//  Copyright © 2016 3Embed. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    /*****************************************************/
    //MARK: -            (Getter Methods)
    /*****************************************************/
    
    static var deviceId: String {
        
        if let ID: String = UIDevice.current.identifierForVendor?.uuidString {
            return ID
        }
        else {
            return "iPhone_Simulator_ID"
        }
    }
    
   static var batteryLevel: Float {
        return UIDevice.current.batteryLevel
    }
    
    
    static var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
    
    static var appName: String {
        return (Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String)!
    }
    
    static var appVersion: String {
        return (Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String)!
    }
    
    static var deviceName: String {
        return UIDevice.current.systemName as String
    }
    
    static var deviceVersion: String {
        return UIDevice.current.systemVersion as String
    }
    
    // Get Session Token
    static var sessionToken: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.SESSION_TOKEN) as! String? {
            return token
        }
        return USER_INFO.SESSION_TOKEN
    }
    
    // Get Driver Channel
    static var driverChannel: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.DRIVERCHANNEL) as! String? {
            return token
        }
        return USER_INFO.DRIVERCHANNEL
    }

    
    // Get presence channel
    static var presenceChannel: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.PRESENCECHANNEL) as! String? {
            return token
        }
        return USER_INFO.PRESENCECHANNEL
    }


    // Get publish key
    static var publishKey: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.PUBLISHKEY) as! String? {
            return token
        }
        return USER_INFO.PUBLISHKEY
    }

    // Get Subsribe key
    static var subscribeKey: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.SUBSCRIBEKEY) as! String? {
            return token
        }
        return USER_INFO.SUBSCRIBEKEY
    }
    
    // Get Server channel
    static var serverChannel: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.SERVERCHANNEL) as! String? {
            return token
        }
        return USER_INFO.SERVERCHANNEL
    }
    
    // Get VehicleType ID
    static var vehicleTypeID: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.VEHTYPEID) as! String? {
            return token
        }
        return USER_INFO.VEHTYPEID
    }


    
    // Get UserID
    static var userId: String {
        if let ID: String = UserDefaults.standard.value(forKey: USER_INFO.USER_ID) as! String? {
            return ID
        }
        return USER_INFO.USER_ID
    }
    
    
    
    // Get referralCode
    static var referralCode: String {
        if let ID: String = UserDefaults.standard.value(forKey: USER_INFO.REFERRAL_CODE) as! String? {
            return ID
        }
        return USER_INFO.REFERRAL_CODE
    }
    
    
    // Get checkOldPassword
    static var checkOldPassword: String {
        if let ID: String = UserDefaults.standard.value(forKey: USER_INFO.OLDPASSWORD) as! String? {
            return ID
        }
        return USER_INFO.OLDPASSWORD
    }
    
    // Get User Name
    static var userName: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.USER_NAME) as! String? {
            return name
        }
        return USER_INFO.USER_NAME
    }
    
    // Get User Name
    static var userImage: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.USERIMAGE) as! String? {
            return name
        }
        return USER_INFO.USERIMAGE
    }
    
    // Get Push Token
    static var pushToken: String {
        if let token: String = UserDefaults.standard.value(forKey: USER_INFO.PUSH_TOKEN) as! String? {
            return token
        }
        return  USER_INFO.PUSH_TOKEN
    }
    
    // Get currencySymbol
    static var currencySymbol: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.CURRENCYSYMBOL) as! String? {
            return name
        }
        return USER_INFO.CURRENCYSYMBOL
    }

    // Get currencySymbol
    static var WeightUnits: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.WEIGHT) as! String? {
            return name
        }
        return USER_INFO.WEIGHT
    }

    // Get DistanceUdits
    static var DistanceUnits: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.DISTANCE) as! String? {
            return name
        }
        return USER_INFO.DISTANCE
    }

    // Get DistanceUdits
    static var driverPublishChn: String {
        if let name: String = UserDefaults.standard.value(forKey: USER_INFO.DRIVERPUBCHANNEL) as! String? {
            return name
        }
        return USER_INFO.DRIVERPUBCHANNEL
    }

    
    
    // Get User Phone
    static var savedID: String {
        if let id: String = UserDefaults.standard.value(forKey: USER_INFO.SAVEDID) as! String? {
            return id
        }
        return ""
    }
    
    // Get User Phone
    static var savedPassword: String {
        if let password: String = UserDefaults.standard.value(forKey: USER_INFO.SAVEDPASSWORD) as! String? {
            return password
        }
        return ""
    }

    
       // Get User Email
    static var userEmail: String {
        if let email: String = UserDefaults.standard.value(forKey: USER_INFO.USER_EMAIL) as! String? {
            return email
        }
        return USER_INFO.USER_EMAIL
    }
    
    // Get User Dial Code
    static var userDialCode: String {
        if let dialcode: String = UserDefaults.standard.value(forKey: USER_INFO.USER_DIALCODE) as! String? {
            return dialcode
        }
        return USER_INFO.USER_DIALCODE
    }
    
    // Get User Phone
    static var userPhone: String {
        if let phone: String = UserDefaults.standard.value(forKey: USER_INFO.USER_PHONE) as! String? {
            return phone
        }
        return USER_INFO.USER_PHONE
    }
   
    // Get User Phone
    static var onGoingBid: String {
        if let bid: String = UserDefaults.standard.value(forKey: USER_INFO.SELBID) as! String? {
            return bid
        }
        return USER_INFO.SELBID
    }
    

    // Get User Phone
    static var onGoingCustChn: String {
        if let chn: String = UserDefaults.standard.value(forKey: USER_INFO.SELCHN) as! String? {
            return chn
        }
        return USER_INFO.SELCHN
    }
    
    // Get User Phone
    static var bookStat: String {
        if let status: String = UserDefaults.standard.value(forKey: USER_INFO.BOOKSTATUS) as! String? {
            return status
        }
        return USER_INFO.BOOKSTATUS
    }

    

    
    // Get Amazon Phone
    static var amazonUrl: String {
        return "https://s3-us-west-2.amazonaws.com/dayrunner/"
    }
    

    
    // Get User Type
    static var userType: Int = 1
    
    class func deleteSavedData() -> Bool {
        let pub = VNHPubNubWrapper.sharedInstance() as! VNHPubNubWrapper
        pub.unsubscribeFromMyChannel()
        pub.unsubscribeFromPresenceChannel()
        
        let ud = UserDefaults.standard
        ud.removeObject(forKey: USER_INFO.SESSION_TOKEN)
        ud.removeObject(forKey: USER_INFO.DEVICE_ID)
        ud.removeObject(forKey: USER_INFO.DID_AGREE_TC)
        ud.removeObject(forKey: USER_INFO.COUPON)
       // ud.removeObject(forKey: USER_INFO.PUSH_TOKEN)
        ud.removeObject(forKey: USER_INFO.STRIPE_KEY)
        ud.removeObject(forKey: USER_INFO.USER_ID)
        ud.removeObject(forKey: USER_INFO.USER_NAME)
        ud.removeObject(forKey: USER_INFO.USER_EMAIL)
        ud.removeObject(forKey: USER_INFO.USER_PHONE)
        ud.removeObject(forKey: USER_INFO.USERIMAGE)
        ud.removeObject(forKey: USER_INFO.USER_DIALCODE)
        ud.removeObject(forKey: USER_INFO.VEHTYPEID)
        VNHPubNubWrapper.deleteInstance()
        ud.synchronize()
        
        return true
    }
    
    /***************************** Generic Utility ********************************/
    
    /// Int for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: Int Value
    class func intForObj(object: Any?) -> Int {
        
        switch object {
            
        case is Int, is Int8, is Int16, is Int32, is Int64:
            
            return object as! Int
            
        case is String:
            
            if (object as! String) == "" {
                return 0
            }
            return Int(object as! String)!
            
        default:
            
            return 0
        }
    }
    
    /// Float for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: Float Value
    class func floatForObj(object: Any?) -> Float {
        
        switch object {
            
        case is Float, is Int:
            
            return object as! Float
            
        case is String:
            
            if (object as! String) == "" {
                return 0.0
            }
            return Float(object as! String)!
            
        default:
            return 0.0
        }
    }
    
    /// String for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: String
    class func strForObj(object: Any?) -> String {
        
        switch object {
            
        case is String: // String
            
            switch object as! String {
                
            case "(null)", "(Null)", "<null>", "<Null>":
                
                return ""
                
            default:
                
                return object as! String
            }
            
        case is Int, is Float:
            
            return String(describing: object)
            
        default:
            
            return ""
        }
    }
    /// Dictionary for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: [String: AnyObject]
    class func dictionaryForObj(object: Any?) -> [String: AnyObject] {
        
        switch object {
            
        case is [String: AnyObject]: // Dictionary of Type [String: AnyObject]
            
            return object as! [String : AnyObject]
            
        case is [String: Any]: // Dictionary of Type [String: Any]
            
            return object as! [String : AnyObject]
            
        default:
            
            return [:]
        }
    }
    
    /// Array for Object
    ///
    /// - Parameter object: Any Object
    /// - Returns: [AnyObject]
    class func arrayForObj(object: Any?) -> [AnyObject] {
        
        switch object {
            
        case is [AnyObject]: // Array of Type [AnyObject]
            
            return object as! [AnyObject]
            
        case is [Any]: // Array of Type [Any]
            
            return object as! [AnyObject]
            
        default:
            
            return []
        }
    }
}
