//
//  DateExtension.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 23/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import Foundation

class Dates {

    
    
    static func printDatesBetweenInterval(_ startDate: Date, _ endDate: Date) ->Array<String>
    {
        var startDate = startDate
        let calendar = Calendar.current
        var dateArray = [String]()
        
        let fmt = DateFormatter()
        fmt.dateFormat = "yyyy-MM-dd"
        
        while startDate <= endDate {
         //   print(fmt.string(from: startDate))
            
            dateArray.append(fmt.string(from: startDate))
            
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
        }
        return dateArray
    }
    
    static func dateFromString(_ dateString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        return dateFormatter.date(from: dateString)!
    }
}
