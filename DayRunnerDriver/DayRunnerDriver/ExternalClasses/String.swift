//
//  Strings.swift
//  iDeliver
//
//  Created by 3Embed on 09/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import Foundation

extension String {
    
    /// Calculate Length Of String
    var length: Int {
        return self.characters.count
    }
    /// Check if It is Number
    var isNumber : Bool {
        get{
            return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
        }
    }
    
    /// Validate Email address
    var isValidEmail: Bool {
        
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        }
        catch {
            return false
        }
    }
    
    //validate PhoneNumber
//    var isValidPhoneNumber: Bool {
//        
//        let charcter  = NSCharacterSet(charactersIn: "+0123456789").inverted
//        var filtered: String!
//        let inputString: NSArray = self.components(separatedBy: charcter) as NSArray
//        filtered = inputString.componentsJoined(by: "") as String!
//        return self == filtered
//    }
    
    /// Encode to Base64
    ///
    /// - Returns: Returns Encoded String
//    func encodeToBase64() -> String {
//        
//        var res:String? = ""
//        let utf = self.data(using: String.Encoding.utf8)
//        res = utf?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue:0))
//        return res!
//    }
//    
//    //: ### Base64 encoding a string
//    func base64Encoded() -> String? {
//        if let data = self.data(using: .utf8) {
//            return data.base64EncodedString()
//        }
//        return nil
//    }
//    
//    //: ### Base64 decoding a string
//    func base64Decoded() -> String? {
//        if let data = Data(base64Encoded: self) {
//            return String(data: data, encoding: .utf8)
//        }
//        return nil
//    }
    
//    func removeWhiteSpace() -> String {
//        
//        let nsurl = URL(string: self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)
//        var string = String(describing: nsurl?.absoluteString)
//        string = string.replacingOccurrences(of: " ", with: "")
//        return string
//    }
//    
//    static func className(_ aClass: AnyClass) -> String {
//        return NSStringFromClass(aClass).components(separatedBy: ".").last!
//    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.characters.index(self.startIndex, offsetBy: from))
    }
}
