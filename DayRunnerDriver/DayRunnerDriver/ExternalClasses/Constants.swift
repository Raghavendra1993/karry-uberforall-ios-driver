//
//  Constants.swift
//  Dayrunner Driver
//
//  Created by Rahul Sharma on 24/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

import Foundation
import UIKit

struct API
{
    private static let BASE_IP              = "http://192.241.153.106"
    static let BASE_URL                     = BASE_IP + ":7002/"
    
    struct SOCKET {
        
        static let URL = BASE_IP
        static let PORT_NUMBER              = "9999"
        
        struct CHANNEL {
            static let CUSTOMER_STATUS      = "CustomerStatus"
            static let MESSAGE              = "Message"
        }
    }
    
    struct AOTH {
        
        // Authentication Header
        static let USERNAME = ""
        static let PASSWORD = ""
    }
    
    struct METHOD {
        
        static let Master                   = "master/"
        
        //Master Login
        static let LOGIN                    = Master + "signin"
        
        //Master signup
        static let SIGNUP                   = Master + "signup"
        
        static let VEHICLEDEFAULT           = Master  + "vechicleDefault"
        
        static let EMAILPHONEVALIDATE       = Master  + "EmailPhoneValidate"
        
        static let UPDATEDRIVERSTATUS       = Master  + "status"
        
        static let RESPONDAPPT              = Master + "respondToRequest"
        
        static let ASSIGNEDTRIPS            = Master + "assignedtrips/"
        
        static let PROFILEDATA              = Master + "profile/"
        
        static let ACKNOWLEDGEBK            = Master + "ackbooking"
        
        static let UPDATEBOOKINGSTATUS      = Master + "bookingStatus"
        
        static let HISTORYSERVICE           = Master  + "trips"
        
        static let getZones                 = Master + "zone"
        
        static let UPDATEPROFILE            = Master + "profile"
        
        static let CANCELBOOKING            = Master + "cancelBooking"
        
        static let LOCATION                 = Master + "location"
        
      
        
        static let Apps                     = "app/"
        
        //Master GetOTP
        static let SignupGetOTP             = Apps + "signupOtp"
        
        //Master GetOTP
        static let getOperators             = Apps + "operators"
        
        static let makeModel                = Apps + "makeModel"
        
        static let vehicleTypes             = Apps + "vehicleType"
        
        static let verifyOTP                = Apps + "verifyiOtp"
        
        static let LOGOUT                   = Apps + "LogOut"
        
        static let FORGOTPASSWORD           = Apps + "forgotpassword"
        
        static let UPDATEPASSWORD           = Apps + "updatePassword"
        
        static let CANCELREASONS            = Apps + "cancelReasons/2/"
        
        static let CONFIG                   = Apps + "config/"
        
          static let SUPPORT                 = Apps + "support/"
        
        static let Campaign                 = "campaign/"
        
        static let REFERRALCODE             = Campaign + "referral/"
        
        
        
    }
}

struct SERVER_CONSTANTS{
     static let googleMapsApiKey = "AIzaSyCidTHwrjz2luQS2QuKqGxAVIl_y_ojY2g"
}

struct AMAZONUPLOAD {
    static let APPNAME                   = "Drivers/"
    
    static let VEHICLE                   = "Vehicles/"
    
    static let PROFILEIMAGE    = APPNAME + "ProfilePics/"
    
    static let VEHICLEIMAGE    = APPNAME + "vehicleImage/"
    
    static let DRIVERLICENCE   = APPNAME + "DriverLincence/"
    
    
    static let SIGNATURE       = APPNAME + "signature/"
    
    
    static let DOCUMENTS      = APPNAME + "completionDoc/"
    
    static let INSURANCE       = VEHICLE + "VehicleDocuments/"
    
}


struct USER_INFO {
   
    

    
    static let DID_AGREE_TC = "did_agree_terms&Conditions"
    static let STRIPE_KEY = "stripe_key"
    static let COUPON = "coupon"
    
 
    static let USER_EMAIL = "user_email"
    static let USER_DIALCODE = "user_dialcode"
    static let USER_PHONE = "user_phone"
    
    static let SOCKET_SERVER_CHANNEL = "server_channel"
    static let SOCKET_MY_CHANNEL = "my_channel"
    
    
    //***********
    static let SESSION_TOKEN   = "session_token"
    static let PRESENCECHANNEL = "presence_chn"
    static let PUBLISHKEY      = "pub_key"
    static let SUBSCRIBEKEY    = "sub_key"
    static let SERVERCHANNEL   = "server_chn"
    static let DRIVERCHANNEL   = "driver_channel"
    static let DRIVERPUBCHANNEL = "driver_Pub_channel"
    static let VEHTYPEID       = "typeId"
    static let USER_ID         = "user_id"
    static let REFERRAL_CODE   = "Referal_Code"
    
    static let OLDPASSWORD     = "Oldpassword"
    static let USER_NAME       = "user_name"
    static let USERIMAGE       = "pPic"
    
    static let CURRENCYSYMBOL     = "$"
    static let DISTANCE        = "kms"
    static let WEIGHT          = "Kgs"
    
    static let DEVICE_ID       = "device_id"
    static let PUSH_TOKEN      = "default_push_token"
    
    static let SAVEDID         = "savedID"
    static let SAVEDPASSWORD   = "password"
    
    static let SELBID          = "undefined"
    static let SELCHN          = "chn"
    static let BOOKSTATUS      = "0"
    
}

struct COLOR {
    
    static let NAVIGATION_BAR  = UIColor (cgColor: 0xB88955 as! CGColor)
    static let NAVIGATION_TITLE  = WHITE
    static let BACK_BUTTON = WHITE
    
    static let WHITE = UIColor.white
    static let BLACK = UIColor.black
    static let GRAY = UIColor.gray
    static let DARK_GRAY = UIColor.darkGray
    static let LIGHT_GRAY = UIColor.lightGray
    static let EBEBEB = UIColor (cgColor: 0xEBEBEB as! CGColor)
    static let F8F8F8 = UIColor (cgColor: 0xF8F8F8 as! CGColor)
    
}

enum HelveticaNeue: String {
    
    case HelveticaNeue    = "HelveticaNeue"
    case Italic           = "HelveticaNeue-Italic"
    case Bold             = "HelveticaNeue-Bold"
    case UltraLight       = "HelveticaNeue-UltraLight"
    case CondensedBlack   = "HelveticaNeue-CondensedBlack"
    case BoldItalic       = "HelveticaNeue-BoldItalic"
    case CondensedBold    = "HelveticaNeue-CondensedBold"
    case Medium           = "HelveticaNeue-Medium"
    case Light            = "HelveticaNeue-Light"
    case Thin             = "HelveticaNeue-Thin"
    case ThinItalic       = "HelveticaNeue-ThinItalic"
    case LightItalic      = "HelveticaNeue-LightItalic"
    case UltraLightItalic = "HelveticaNeue-UltraLightItalic"
}

struct FONT {
    
    static let NAVIGATION_TITLE = UIFont(name: HelveticaNeue.Medium.rawValue, size: 15)
    static let BACK_BUTTON = UIFont(name: HelveticaNeue.Medium.rawValue, size: 12)
}

struct iPHONE {
    
    //  Device IPHONE
    static let IS_iPHONE_4s: Bool =  (UIScreen.main.bounds.size.height == 480)
    static let IS_iPHONE_5: Bool =  (UIScreen.main.bounds.size.height == 568)
    static let IS_iPHONE_6: Bool =  (UIScreen.main.bounds.size.height == 667)
    static let IS_iPHONE_6_Plus: Bool =  (UIScreen.main.bounds.size.height == 736)
}


struct API_CONSTANT {
    
    static let GOOGLE_MAP_KEY = "AIzaSyBRUzLnkRtfZzYD0rGzYBYD-tc6HRRYKTU"
    static let GOOGLE_SERVER_KEY = "AIzaSyDCIvaWZlH-r8TqUCXJ7rPEg5vdWiU1pOk"
    
    static let STRIPE_KEY = "pk_test_IBYk0hnidox7CDA3doY6KQGi"
}

struct NOTIFICATION_NAME {
    
    static let ADD_POST_JOB_BUTTON = "add_post_job_button"
    static let REMOVE_POST_JOB_BUTTON = "remove_post_job_button"
    static let POST_JOB_BUTTON_ACTION = "post_job_button_action"
}

struct UIMessages {

    static let Alert = "Alert"
    static let OK = "OK"
    static let YES = "YES"
    static let NO = "NO"
    static let Message = "Message"
    static let Error = "Error"
    static let Cancel = "Cancel"
    
    struct SPLASH {

        static let WELCOME = "Welcome"
        static let HI = "Hi"
        static let Hi_MESSAGE = "Welcome"
    }
    
    struct Warnings {
        // Contact Sync Message
        static let ContactAccessTitle = "Can't access contact"
        static let ContactAccessMessage = "Please go to Settings -> Trustpals to enable contact permission"
        
        static let CameraSupportMessage = "Your device doesn't support Camera. Please choose other option."
    }
    
}
