//
//  GrandTotalTableViewCell.swift
//  DayRunnerDriver
//
//  Created by Rahul Sharma on 18/05/17.
//  Copyright © 2017 3Embed. All rights reserved.
//

import UIKit

class GrandTotalTableViewCell: UITableViewCell {

    @IBOutlet var grandTotal: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
