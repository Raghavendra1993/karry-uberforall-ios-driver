//
//  PBBillDetailsCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 11/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit



class PBBillDetailsCell: UITableViewCell {
    
    @IBOutlet var subTotal: UILabel!
    @IBOutlet var discount: UILabel!
    @IBOutlet var timeFare: UILabel!
    @IBOutlet var distanceFee: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

  
