//
//  PBDriverDetailsCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 11/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class PBDriverDetailsCell: UITableViewCell {
    @IBOutlet var distance: UILabel!

    @IBOutlet var dropAddress: UILabel!
    @IBOutlet var pickAddress: UILabel!
    @IBOutlet var timeDuration: UILabel!
    @IBOutlet var rating: UILabel!
    @IBOutlet var totalAmount: UILabel!
    @IBOutlet var senderPhone: UILabel!
    @IBOutlet var senderName: UILabel!
    @IBOutlet var senderImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
