//
//  PastBookingController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 11/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import Kingfisher

enum PBSectionType : Int {
    case PBDriverDetails = 0
    case PBBillDetails = 1
    case PBServies = 2
    case PBGrandTot = 3
    case PBPaymentType = 4
    
    case PBReceiversDetails = 5
    case PBDocuments = 6
    
}

enum PBRowType : Int {
    case PBRowSeperator = 0
    case PBRowDefault = 1
    case PBRowBottomSeperator = 2
}


class PastBookingController: UIViewController {
    
    @IBOutlet var bookingID: UILabel!
    @IBOutlet var dateOfBooking: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var bookingArray = [HomeDetails]()
    var selectedIndex:Int = 0
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        bookingID.text = bookingArray[selectedIndex].bookingId
        dateOfBooking.text = Helper.changeDateFormatForHome(bookingArray[selectedIndex].bookingDateNtime)
        
        tableView.estimatedRowHeight = 10
        tableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: AnyObject) {
       dismiss(animated: true, completion: nil)
    }
}

extension PastBookingController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionType : PBSectionType = PBSectionType(rawValue: section)!
        
        switch sectionType {
            
        case .PBDriverDetails:
            return 2
            
        case .PBBillDetails :
            return 1
        
        case .PBServies:
            return 1
            
        case .PBGrandTot:
            return 1
            
        case .PBPaymentType :
            return 2
            
        case .PBReceiversDetails :
            return 2
            
        case .PBDocuments :
            return 2
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sectionType : PBSectionType = PBSectionType(rawValue: indexPath.section)!
        let sectionCell : PBSectionCell = tableView.dequeueReusableCell(withIdentifier: "PBSectionCell") as! PBSectionCell
        
        let rowType : PBRowType = PBRowType(rawValue: indexPath.row)!
        
        switch sectionType {
        case .PBDriverDetails:
            
            switch rowType {
                
            case .PBRowDefault:
                return sectionCell
            default:
                let cell: PBDriverDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PBDriverDetailsCell") as! PBDriverDetailsCell
                
                cell.distance.text = "20kms"
                
                cell.dropAddress.text = bookingArray[selectedIndex].dropAddress
                cell.pickAddress.text = bookingArray[selectedIndex].pickAddress
                cell.timeDuration.text = "20mins"
                cell.rating.text = "4"
                cell.totalAmount.text = bookingArray[selectedIndex].bookingAmt
                cell.senderPhone.text = bookingArray[selectedIndex].pickPhone
                cell.senderName.text = bookingArray[selectedIndex].pickCustName
                return cell
            }
            
        case .PBBillDetails:
            
            let cell :PBBillDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PBBillDetailsCell") as! PBBillDetailsCell
            
            cell.subTotal.text = Utility.currencySymbol + bookingArray[selectedIndex].bookingAmt
            cell.discount.text = Utility.currencySymbol + "0.00"
            cell.timeFare.text = Utility.currencySymbol + "0.00"
            cell.distanceFee.text = Utility.currencySymbol + "0.00"
            return cell
            
            
        case .PBServies:
            
            let cell : ServicesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "service") as! ServicesTableViewCell
            return cell
            
        case .PBGrandTot:
            
            let cell : GrandTotalTableViewCell = tableView.dequeueReusableCell(withIdentifier: "grandtotal") as! GrandTotalTableViewCell
            cell.grandTotal.text = bookingArray[selectedIndex].bookingAmt
            return cell
            
        case .PBPaymentType:
            
            switch rowType {
                
            case .PBRowDefault:
                return sectionCell
                
            default:
                let cell :PBPaymentTypeCell = tableView.dequeueReusableCell(withIdentifier: "PBPaymentTypeCell") as! PBPaymentTypeCell
                return cell
            }
            
        case .PBReceiversDetails:
            
            switch rowType {
                
            case .PBRowDefault:
                return sectionCell
                
            default:
                let cell :PBReceiversDetailsCell = tableView.dequeueReusableCell(withIdentifier: "PBReceiversDetailsCell") as! PBReceiversDetailsCell
                
                cell.signatureImage.kf.setImage(with: URL(string: bookingArray[selectedIndex].signature),
                                                placeholder:UIImage.init(named: "profile"),
                                                options: [.transition(ImageTransition.fade(1))],
                                                progressBlock: { receivedSize, totalSize in
                },
                                                completionHandler: { image, error, cacheType, imageURL in
                })
                
                cell.receiverName.text  = bookingArray[selectedIndex].dropCustName
                cell.receiverNumber.text  = bookingArray[selectedIndex].dropPhone
                return cell
            }
            
        case .PBDocuments:
            
            switch rowType {
                
            case .PBRowDefault:
                return sectionCell
                
            default:
                let cell :PBDocumentsCell = tableView.dequeueReusableCell(withIdentifier: "PBDocumentsCell") as! PBDocumentsCell
                return cell
            }
        }
    }
}

extension PastBookingController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
